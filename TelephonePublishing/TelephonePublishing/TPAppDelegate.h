//
//  TPAppDelegate.h
//  TelephonePublishing
//
//  Created by Tpbeca tec on 16/09/14.
//  Copyright (c) 2014 Hearst Magazine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ContainerViewController.h"

@class SWRevealViewController; //prueba menu tipo facebook

@interface TPAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) NSMutableArray *sections;
@property (strong, nonatomic) NSMutableArray *arrayVC;
@property (strong, nonatomic) NSMutableArray *arrayNames;
@property (nonatomic, retain) ContainerViewController* horizontalViewController;
@property (nonatomic, retain) ContainerViewController* detailHorizontalViewController;

@property (strong, nonatomic) SWRevealViewController *viewController; //prueba menu tipo facebook

-(void)goHorizontal:(NSInteger *)index;
-(void)goDetail:(NSString *)type array:(NSArray *)array idx:(NSInteger)idx;

@end

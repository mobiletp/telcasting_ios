//
//  Connection.m
//  TAPStart
//
//  Created by Julio Rivas on 12/06/12.
//  Copyright (c) 2012 TAPTAPNetworks. All rights reserved.
//

#import "Connection.h"
#import "HMDataManager.h"

@implementation Connection

@synthesize delegate=_delegate;
@synthesize url=_url;

#pragma mark - Init

-(id)initConnection
{
    if([self init])
    {
    
    }
    return self;
}

#pragma mark - Connection

-(void)makeConnectionWithDictionary:(NSDictionary *)dict
{
    DLog(@"%@", dict);
    
    //Url encoding
    _url = [[NSString alloc] initWithString:[dict valueForKey:kDictUrl]];
    [dict setValue:[_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] forKey:kDictUrl];
    
    //Rquest
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[dict valueForKey:kDictUrl]] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:kDefaultTimeout];
    
    //Cookie
    if([dict valueForKey:kDictCookie])
    {
        [request setValue:[dict valueForKey:kDictCookie] forHTTPHeaderField:kDictCookie];   
    }
    
    //Connection type
    if([[dict valueForKey:kDictConnType] isEqualToString:kConnTypePost])
    {
        //POST
        [request setHTTPMethod:kConnTypePost];
        
        //Parámetros
        if([dict valueForKey:kDictConnParams])
        {
            [request setHTTPBody:[[dict valueForKey:kDictConnParams] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        //Web service
        if([dict valueForKey:kDictBody] && [dict valueForKey:kDictEnvelope])
        {
            [request setHTTPBody:[@"" dataUsingEncoding:NSUTF8StringEncoding]];
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];        
        }
    }
    else if([[dict valueForKey:kDictConnType] isEqualToString:kConnTypeGet])
    {
        //GET
        [request setHTTPMethod:kConnTypeGet];
    }    
    
    DLog(@"header: %@",[request allHTTPHeaderFields]);
    
    //Response
    _receivedData = [[NSMutableData alloc] initWithLength:0];
    
    //Conn
    _connection = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:YES];    

}

-(void)makeConnection:(NSString *)url
{
    //Diccionario
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:0];
    
    //Meto url
    [dict setValue:url forKey:kDictUrl];
    
    //Llamo a con    
    [self makeConnectionWithDictionary:dict];
}

-(void)stopConnection
{
    //Para la conexión si está activa
    if(_connection)[_connection cancel];
}

#pragma mark - NSURLConnection delegate

- (void) connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    /* This method is called when the server has determined that it has
	 enough information to create the NSURLResponse. It can be called
	 multiple times, for example in the case of a redirect, so each time
	 we reset the data. */
    
    //Header
    NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
    //[_responseHeader release];
    _responseHeader=[NSDictionary dictionaryWithDictionary:[httpResponse allHeaderFields]];

    DLog(@"Header %@",_responseHeader);
    
    //Compruebo si tengo cookie
    if([_responseHeader valueForKey:kDictCookie])
    {
        if(_delegate && [_delegate respondsToSelector:@selector(connection:didReceiveCookie:)])
        {
            DLog(@"Set new cookie");
            
            //Mando cookie al delegado
            [_delegate connection:self didReceiveCookie:[_responseHeader valueForKey:kDictCookie]];
        }
    }
    
    //Server status
	_responseStatusCode = [httpResponse statusCode];	
    
    //Inicio respuesta
    [_receivedData setLength:0];	
}


- (void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    DLog();
    
    /* Append the new data to the received data. */
    [_receivedData appendData:data];
}


- (void) connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    DLog();
    
    if (_delegate && [_delegate respondsToSelector:@selector(connection:didFailWithError:)]) 
    {
        [_delegate connection:self didFailWithError:error];
    }
}


- (NSCachedURLResponse *) connection:(NSURLConnection *)connection  willCacheResponse:(NSCachedURLResponse *)cachedResponse
{
    DLog();
    
	/* this application does not use a NSURLCache disk or memory cache */
    return nil;
}


- (void) connectionDidFinishLoading:(NSURLConnection *)connection
{
    DLog();
    
    if (_delegate && [_delegate respondsToSelector:@selector(connection:didFinishWithData:)])
    {
        [_delegate connection:self didFinishWithData:_receivedData];
    }
}


#pragma mark - HTTPS interface

+ (BOOL)allowsAnyHTTPSCertificateForHost:(NSString*)host 
{ 
    return YES; 
}

+ (void)setAllowsAnyHTTPSCertificate:(BOOL)allow forHost:(NSString*)host 
{ 	
}

#pragma mark - Memory managment

-(void)dealloc
{

}

@end

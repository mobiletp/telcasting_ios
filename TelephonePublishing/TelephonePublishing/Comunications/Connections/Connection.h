//
//  Connection.h
//  TAPStart
//
//  Created by Julio Rivas on 12/06/12.
//  Copyright (c) 2012 TAPTAPNetworks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSURLRequest (DummyInterface)
+ (BOOL)allowsAnyHTTPSCertificateForHost:(NSString*)host;
+ (void)setAllowsAnyHTTPSCertificate:(BOOL)allow forHost:(NSString*)host;
@end

@class Connection;

@protocol ConnectionDelegate <NSObject>
-(void)connection:(Connection *)connection didFinishWithData:(NSData *)data;
-(void)connection:(Connection *)connection didFailWithError:(NSError *)error;
@optional -(void)connection:(Connection *)connection didReceiveCookie:(NSString *)cookie;
@end

#define kDefaultTimeout 10.0

#define kDictUrl @"url"
#define kDictConnType @"type"
#define kDictConnParams @"params"
#define kDictCookie @"Set-Cookie"
#define kDictBody @"Body"
#define kDictEnvelope @"Envelope"

#define kConnTypePost @"POST"
#define kConnTypeGet @"GET"

@interface Connection : NSObject <NSURLConnectionDelegate>
{
    //Url
    NSString *_url;
    
    //objeto conexion
	NSURLConnection *_connection;
	
	//datos recibidos
	NSMutableData *_receivedData;
    
    //response status
    NSInteger _responseStatusCode;
    
    //header
    NSDictionary *_responseHeader;
    
    //Delegado
    @private id<ConnectionDelegate> __delegate;
}

-(id)initConnection;
-(void)makeConnection:(NSString *)url;
-(void)makeConnectionWithDictionary:(NSDictionary *)dict;
-(void)stopConnection;

@property (assign) id<ConnectionDelegate> delegate;
@property (nonatomic, retain) NSString *url;

@end

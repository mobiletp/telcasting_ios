//
//  XMLConstants.h
//  Elle
//
//  Created by Julio Rivas on 28/08/12.
//  Copyright (c) 2012 TAPTAP Networks. All rights reserved.
//

#ifndef Elle_XMLConstants_h
#define Elle_XMLConstants_h

enum  {
    XML_BLOCK_IPHONE = 0,
    XML_BLOCK_BLOGS,
    XML_BLOCK_CITAS,
    XML_BLOCK_RECETAS,
    XML_BLOCK_RECETASIN,
    XML_BLOCK_ANDORID,
    XML_BLOCK_POSTS,
    XML_BLOCK_COMMENTS,
    XML_BLOCK_EXPORTACION,
    XML_BLOCK_HOROSCOPOS,
    XML_BLOCK_SUEGNOS
    
};

enum  {
    XML_OBJECT_APP = 0,
    XML_OBJECT_BLOG,
    XML_OBJECT_CITAS,
    XML_OBJECT_RECETAS,
    XML_OBJECT_RECETASIN,
    XML_OBJECT_CANAL,
    XML_OBJECT_TOKEN,
    XML_OBJECT_POST,
    XML_OBJECT_COMMENT,
    XML_OBJECT_RESPONSE,
    XML_OBJECT_CONTENIDO,
    XML_OBJECT_IMAGEN,
    XML_OBJECT_PREDICCION,
    XML_OBJECT_SUEGNOS
};

#define XML_block_iphone @"iphone"
#define XML_block_blogs @"blogs"
#define XML_block_citas @"citas"
#define XML_block_recetas @"recetas"
#define XML_block_suegnos @"sueños"
#define XML_block_recetasin @"recetasin"
#define XML_block_android @"android"

#define XML_block_exportacion @"service"
#define XML_object_contenido @""

#define XML_block_horoscopos @"service"
#define XML_object_prediccion @"prediccion"
#define XML_object_suegnos @"prediccion"

#define XML_id @"id"
#define XML_Tipo @"Tipo"
#define XML_url @"url"
#define XML_Titulo @"Titulo"
#define XML_Entradilla @"Entradilla"
#define XML_Ingredientes @"Ingredientes"
#define XML_Autor @"Autor"
#define XML_ImagePrincipal @"ImagenPrincipal"
#define XML_ImagePrincipalFull @"FotoCuaA"
#define XML_ImagePrincipalThumbs @"FotoCuaB"
#define XML_object_Imagen @"Imagen"
#define XML_FotoMiniA @"FotoMiniA"
#define XML_FotoFullA @"FotoFullA"
#define XML_Texto @"Texto"
#define XML_Pasos @"Pasos"
#define XML_url_portadas @"UrlXml"

#define XML_object_app @"app"
#define XML_object_blog @"blog"
#define XML_object_citas @"categoria_cita"
#define XML_object_recetas @"prediccion" //Recetas magicas
#define XML_object_recetasin @"Contenido"
#define XML_object_item @"item"
#define XML_object_response @"response"

#define XML_imagen @"imagen"
#define XML_titulo @"titulo"
#define XML_texto @"texto"
#define XML_dia @"dia"
#define XML_signo @"signo"
#define XML_tienda @"tienda"
#define XML_blog_id @"blog_id"
#define XML_blog_name @"blog_name"
#define XML_blog_url @"blog_url"
#define XML_blog_img @"blog_img"
#define XML_citas_id @"categoria_cita_id"
#define XML_citas_name @"categoria_name"
#define XML_citas_url @"categoria_url"
#define XML_citas_img @"categoria_img"

#define XML_recetas_id @"id"
#define XML_recetas_titulo @"titulo"
#define XML_recetas_tipo @"tipo"
#define XML_recetas_description @"texto"

#define XML_canal @"canal"
#define XML_appToken @"appToken"
#define XML_blog_description @"blog_description"
#define XML_blog_category @"blog_category"
#define XML_blog_feed @"post_feed"
#define XML_citas_feed @"categoria_feed"
#define XML_recetas_feed @"categoria_feed"

#define XML_post_title @"title"
#define XML_post_link @"link"
#define XML_post_comments @"comments"
#define XML_post_pubDate @"pubDate"
#define XML_post_creator @"dc:creator"
#define XML_post_category @"category"
#define XML_post_description @"description"
#define XML_post_content @"content:encoded"

#define XML_post_comment_rss @"wfw:commentRss"
#define XML_post_comment_number @"slash:comments"
#define XML_post_id @"postID"
#define XML_post_image @"postIMG"
#define XML_formated_date @"formatedDate"

#define XML_code @"code"
#define XML_comment @"comment"

#endif

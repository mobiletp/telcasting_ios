//
//  XMLReader.m
//  
//
//  Created by Julio Rivas on 16/03/10.
//  Copyright 2010 . All rights reserved.
//

#import "XMLReader.h"
#import "XMLConstants.h"
#import "Connection.h"
#import "App.h"
#import "HMDataManager.h"
//#import "Response.h"
#import "Item.h"
#import "Image.h"
#import "HoroscopoMes.h"
#import "Recetas.h"
#import "Dreams.h"

@implementation XMLReader

@synthesize contentElement=_contentElement;
@synthesize delegate=_delegate;
@synthesize currentRecetas=_currentRecetas;
@synthesize currentApp=_currentApp;
@synthesize currentResponse=_currentResponse;
@synthesize currentItem=_currentItem;
@synthesize currentImage=_currentImage;
@synthesize currentAstro=_currentAstro;
@synthesize currentDreams=_currentDreams;

/**
 *	Función de lectura de un XML contenido en un NSData
 ****************************************************************/

- (void)parseXMLFileAtData:(NSData *)data
{
    _foundHoroscopo = NO;
    _foundRecetas = NO;
    _foundDreams = NO;
    
    [self parseXMLFileAtData:data :-1];
}

- (void)parseXMLFileAtData:(NSData *)data :(NSUInteger)sender
{	    
    types=[[NSMutableArray alloc] initWithCapacity:0];
	senderType = sender;
	
    _foundHoroscopo = NO;
    _foundRecetas = NO;
    _foundDreams = NO;
    
	//crea el parser
	NSXMLParser *parser = [[NSXMLParser alloc] initWithData:data];
	
    //asigna delegados
    [parser setDelegate:self];
	
    //inicializa opciones de parseo
    [parser setShouldProcessNamespaces:NO];
    [parser setShouldReportNamespacePrefixes:NO];
    [parser setShouldResolveExternalEntities:NO];
    
	//ejecuta el parser
    [parser parse];
    
}

/**
 *	Metodo que parsea atributos
 ******************************************************/

- (void)parseAttributes:(NSDictionary *)attributes
{
	
}

/**
 *	Función de comienzo de elemento
 ******************************************************/

- (void) startElement:(NSString *)elementName :(NSDictionary *)attributes
{
    //DLog(@"%@",elementName);
    //Attributes
    _attributes = [[NSDictionary alloc] initWithDictionary:attributes];
    
    //Element
    if([elementName isEqualToString:XML_block_exportacion] && [[attributes valueForKey:@"name"] isEqualToString:@"Contenido"])
    {
        //Bloque
        [types addObject:[NSNumber numberWithInt:XML_BLOCK_EXPORTACION]];
    }
    else if([elementName isEqualToString:XML_block_exportacion] && [[attributes valueForKey:@"name"] isEqualToString:@"Horoscopos"])
    {
        //Reset
        _foundAstro=NO;
        
        _foundHoroscopo = YES;
        
        //Block horoscopos
        [types addObject:[NSNumber numberWithInt:XML_BLOCK_HOROSCOPOS]];
    }
    else if([elementName isEqualToString:XML_block_exportacion] && [[attributes valueForKey:@"name"] isEqualToString:@"Sueños"])
    {
        //Bloque
        [types addObject:[NSNumber numberWithInt:XML_BLOCK_SUEGNOS]];
        _foundDreams = YES;
    }
    else if([elementName isEqualToString:XML_block_exportacion] && [[attributes valueForKey:@"name"] isEqualToString:@"Recetas"])
    {
        //Bloque
        [types addObject:[NSNumber numberWithInt:XML_BLOCK_RECETAS]];
        _foundRecetas = YES;
    }
    else if([elementName isEqualToString:XML_object_contenido])
    {
        //Object Contenido
        [types addObject:[NSNumber numberWithInt:XML_OBJECT_CONTENIDO]];
        
        //Item
        _currentItem = [[Item alloc] initItem];
    }
    else if([elementName isEqualToString:XML_ImagePrincipal])
    {
        _contentElement = nil;
        _contentElement = [NSMutableString string];
        
        //Image
        _currentImage = [[Image alloc] initImage];
    }
    else if([elementName isEqualToString:XML_ImagePrincipalFull])
    {
        _contentElement = nil;
        _contentElement = [NSMutableString string];
        
        //Image
        _currentImage = [[Image alloc] initImage];
    }
    else if([elementName isEqualToString:XML_ImagePrincipalThumbs])
    {
        _contentElement = nil;
        _contentElement = [NSMutableString string];
        
        //Image
        _currentImage = [[Image alloc] initImage];
    }
    else if([elementName isEqualToString:XML_object_Imagen])
    {
        //Object Contenido
        [types addObject:[NSNumber numberWithInt:XML_OBJECT_IMAGEN]];
        
        //Image
        _currentImage = [[Image alloc] initImage];
    }
	else if([elementName isEqualToString:XML_block_iphone])
	{
        //Bloque iphone
		[types addObject:[NSNumber numberWithInt:XML_BLOCK_IPHONE]];
	}
    else if([elementName isEqualToString:XML_block_android])
    {
        //Bloque adroid
		[types addObject:[NSNumber numberWithInt:XML_BLOCK_ANDORID]];
    }        
	else if([elementName isEqualToString:XML_object_app] && [[types lastObject] intValue]==XML_BLOCK_IPHONE)
	{
		//Objeto App
        [types addObject:[NSNumber numberWithInt:XML_OBJECT_APP]];
		
        //Init App
		_currentApp = [[App alloc] initApp];
	}
    else if([elementName isEqualToString:XML_block_blogs])
	{
        //Bloque blogs
		[types addObject:[NSNumber numberWithInt:XML_BLOCK_BLOGS]];
	}
    else if([elementName isEqualToString:XML_block_citas])
    {
        //Bloque Citas
        [types addObject:[NSNumber numberWithInt:XML_BLOCK_CITAS]];
    }
    else if([elementName isEqualToString:XML_block_recetas])
    {
        //Bloque Recetas
        [types addObject:[NSNumber numberWithInt:XML_BLOCK_RECETAS]];
    }
    else if([elementName isEqualToString:XML_object_recetas] && _foundRecetas)
    {
        //Objeto recetas
        [types addObject:[NSNumber numberWithInt:XML_OBJECT_RECETAS]];
        
        //Init recetas
        _currentRecetas = [[Recetas alloc] initRecetas];
    }
    else if([elementName isEqualToString:XML_block_suegnos])
    {
        //Bloque Recetas
        [types addObject:[NSNumber numberWithInt:XML_BLOCK_SUEGNOS]];
    }
    else if([elementName isEqualToString:XML_object_suegnos] && _foundDreams)
    {
        //Objeto recetas
        [types addObject:[NSNumber numberWithInt:XML_OBJECT_SUEGNOS]];
        
        //Init recetas
        _currentDreams = [[Dreams alloc] initDreams];
    }
    else if([elementName isEqualToString:XML_canal])
    {
		//Objeto canal
        [types addObject:[NSNumber numberWithInt:XML_OBJECT_CANAL]];
        
        //Text
        _contentElement = [NSMutableString string];
        
    }
    else if([elementName isEqualToString:XML_appToken])
    {
        //Objeto canal
        [types addObject:[NSNumber numberWithInt:XML_OBJECT_TOKEN]];
        
        //Text
        _contentElement = [NSMutableString string];
    }
    else if([elementName isEqualToString:XML_object_prediccion] && _foundHoroscopo)
    {
        //Object prediccion
        [types addObject:[NSNumber numberWithInt:XML_OBJECT_PREDICCION]];
        
        //Predicción
        _currentAstro = [[HoroscopoMes alloc] init];
    }
    else
	{
		//comprobamos los campos
		if (([elementName isEqualToString:XML_imagen]) ||
			([elementName isEqualToString:XML_titulo]) ||
			([elementName isEqualToString:XML_texto]) ||
			([elementName isEqualToString:XML_tienda]) ||
			([elementName isEqualToString:XML_blog_id]) ||
			([elementName isEqualToString:XML_blog_name]) ||
			([elementName isEqualToString:XML_blog_url]) ||
			([elementName isEqualToString:XML_blog_img]) ||
			([elementName isEqualToString:XML_blog_description]) ||
			([elementName isEqualToString:XML_blog_category]) ||
			([elementName isEqualToString:XML_blog_feed]) ||
            ([elementName isEqualToString:XML_recetas_id]) ||
            ([elementName isEqualToString:XML_recetas_titulo]) ||
            ([elementName isEqualToString:XML_recetas_tipo]) ||
            ([elementName isEqualToString:XML_recetas_description]) ||
            ([elementName isEqualToString:XML_post_title]) ||
			([elementName isEqualToString:XML_post_link]) ||
			([elementName isEqualToString:XML_post_comments]) ||
			([elementName isEqualToString:XML_post_pubDate]) ||
			([elementName isEqualToString:XML_post_creator]) ||
			([elementName isEqualToString:XML_post_category]) ||
			([elementName isEqualToString:XML_post_description]) ||
            ([elementName isEqualToString:XML_post_content]) ||            
			([elementName isEqualToString:XML_post_comment_rss]) ||
			([elementName isEqualToString:XML_post_comment_number]) ||
			([elementName isEqualToString:XML_post_id]) ||
            ([elementName isEqualToString:XML_post_image]) ||
            ([elementName isEqualToString:XML_code]) ||
            ([elementName isEqualToString:XML_comment]) ||
            ([elementName isEqualToString:XML_formated_date]) ||
            ([elementName isEqualToString:XML_id]) ||
            ([elementName isEqualToString:XML_Tipo]) ||
            ([elementName isEqualToString:XML_url]) ||
            ([elementName isEqualToString:XML_Titulo]) ||
            ([elementName isEqualToString:XML_Entradilla]) ||
            ([elementName isEqualToString:XML_Autor]) ||
            ([elementName isEqualToString:XML_FotoMiniA]) ||
            ([elementName isEqualToString:XML_FotoFullA]) ||
            ([elementName isEqualToString:XML_Texto]) ||
            ([elementName isEqualToString:XML_Pasos]) ||
            ([elementName isEqualToString:XML_dia]) ||
            ([elementName isEqualToString:XML_signo]) ||
            ([elementName isEqualToString:XML_url_portadas])
            )
        {
			_contentElement = [NSMutableString string];
		}
		else	
		{
			_contentElement = nil;
		}
	}
}

/**
 *	Función de fin de elemento
 *******************************************/

- (void) endElement:(NSString *)elementName
{
    //DLog(@"%@",elementName);
    
    if([elementName isEqualToString:XML_block_iphone])
    {
        //Elimino bloque iphone
        [types removeLastObject];
    }
    else if([elementName isEqualToString:XML_block_android])
    {
        //Elimino bloque android
        [types removeLastObject];
    }
    else if([elementName isEqualToString:XML_object_app] && [[types lastObject] intValue]==XML_BLOCK_IPHONE)
	{		 
        //Envio app
        if(_delegate && [_delegate respondsToSelector:@selector(xmlReaderDidParse:withObject:)])
        {
            [_delegate xmlReaderDidParse:self withObject:_currentApp];
        }
                
        //Elimino objeto app
        [types removeLastObject];
	}
    else if([elementName isEqualToString:XML_block_blogs])
    {
        //Elimino bloque apps
        [types removeLastObject];
    }
    else if([elementName isEqualToString:XML_block_citas])
    {
        //Elimino bloque apps
        [types removeLastObject];
    }
    else if([elementName isEqualToString:XML_block_recetas])
    {
        //Elimino bloque apps
        [types removeLastObject];
    }
    else if([elementName isEqualToString:XML_object_recetas] && _foundRecetas)
    {
        //Envio data
        if(_delegate && [_delegate respondsToSelector:@selector(xmlReaderDidParse:withObject:)])
        {
            [_delegate xmlReaderDidParse:self withObject:_currentRecetas];
        }
        
        //Elimino bloque apps
        [types removeLastObject];
    }
    else if([elementName isEqualToString:XML_object_suegnos] && _foundDreams)
    {
        //Envio data
        if(_delegate && [_delegate respondsToSelector:@selector(xmlReaderDidParse:withObject:)])
        {
            [_delegate xmlReaderDidParse:self withObject:_currentDreams];
        }
        
        //Elimino bloque apps
        [types removeLastObject];
    }
    else if([elementName isEqualToString:XML_canal])
    {
        if([[types lastObject] intValue]==XML_OBJECT_CANAL)
        {
            //Envio data
            if(_delegate && [_delegate respondsToSelector:@selector(xmlReaderDidParse:withObject:)])
            {
                [_delegate xmlReaderDidParse:self withObject:_contentElement];
            }
            
            //Elimino bloque apps
            [types removeLastObject];
        }                
    }
    else if([elementName isEqualToString:XML_appToken])
    {
        if([[types lastObject] intValue]==XML_OBJECT_TOKEN)
        {
            //Envio blog
            if(_delegate && [_delegate respondsToSelector:@selector(xmlReaderDidParse:withObject:)])
            {
                [_delegate xmlReaderDidParse:self withObject:_contentElement];
            }
            
            //Elimino bloque apps
            [types removeLastObject];
        }                        
    }            
    else if ([elementName isEqualToString:XML_imagen])
    {
        if([[types lastObject] intValue]==XML_OBJECT_APP)[_currentApp setImagen:_contentElement];
    }
    else if ([elementName isEqualToString:XML_titulo])
    {
        if([[types lastObject] intValue]==XML_OBJECT_APP)[_currentApp setTitulo:_contentElement];
    }
    else if ([elementName isEqualToString:XML_texto])
    {
        if([[types lastObject] intValue]==XML_OBJECT_APP)[_currentApp setTexto:_contentElement];
        else if([[types lastObject] intValue]==XML_OBJECT_PREDICCION)
        {
            if([[_attributes objectForKey:@"tema"] isEqualToString:@"AMOR"])
            {
                [_currentAstro setAmor:_contentElement];
            }
            else if([[_attributes objectForKey:@"tema"] isEqualToString:@"SUERTE"])
            {
                [_currentAstro setSuerte:_contentElement];
            }
            else if([[_attributes objectForKey:@"tema"] isEqualToString:@"TRABAJO Y DINERO"])
            {
                [_currentAstro setTrabajo:_contentElement];
            }
        }
    }
    else if ([elementName isEqualToString:XML_tienda])
    {
        if([[types lastObject] intValue]==XML_OBJECT_APP)[_currentApp setTienda:_contentElement];
    }
    else if([elementName isEqualToString:XML_object_response])
    {
        if([[types lastObject] intValue]==XML_OBJECT_RESPONSE)
        {
            //Envio blog
            if(_delegate && [_delegate respondsToSelector:@selector(xmlReaderDidParse:withObject:)])
            {
                [_delegate xmlReaderDidParse:self withObject:_currentResponse];
            }
            
            //Elimino bloque apps
            [types removeLastObject];
        }
    }
    else if([elementName isEqualToString:XML_block_exportacion])
    {
        //Eliminino bloque exportacion
        [types removeLastObject];
    }
    else if([elementName isEqualToString:XML_object_contenido])
    {
        //Envio contenido (item)
        if(_delegate && [_delegate respondsToSelector:@selector(xmlReaderDidParse:withObject:)])
        {
            [_delegate xmlReaderDidParse:self withObject:_currentItem];
        }
        
        //Elimino objeto app
        [types removeLastObject];
    }
    else if([elementName isEqualToString:XML_id])
    {
        if([[types lastObject] intValue]==XML_OBJECT_CONTENIDO)[_currentItem setIdItem:[_contentElement stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
    }
    else if([elementName isEqualToString:XML_Tipo])
    {
        if([[types lastObject] intValue]==XML_OBJECT_CONTENIDO)[_currentItem setTemplateItem:[_contentElement stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
    }
    else if([elementName isEqualToString:XML_url])
    {
        if([[types lastObject] intValue]==XML_OBJECT_CONTENIDO)[_currentItem setShareUrl:[_contentElement stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
    }
    else if([elementName isEqualToString:XML_Titulo])
    {
        if([[types lastObject] intValue]==XML_OBJECT_CONTENIDO)[_currentItem setTitle:[_contentElement stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
        else if([[types lastObject] intValue]==XML_OBJECT_IMAGEN)[_currentImage setTitle:[_contentElement stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
    }
    else if([elementName isEqualToString:XML_Entradilla])
    {
        if([[types lastObject] intValue]==XML_OBJECT_CONTENIDO)[_currentItem setHeader:[_contentElement stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
    }
    else if([elementName isEqualToString:XML_Ingredientes])
    {
        if([[types lastObject] intValue]==XML_OBJECT_CONTENIDO)[_currentItem setHeader:[_contentElement stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
    }
    else if([elementName isEqualToString:XML_Autor])
    {
        if([[types lastObject] intValue]==XML_OBJECT_CONTENIDO)[_currentItem setHeader:[_contentElement stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
    }
    else if([elementName isEqualToString:XML_ImagePrincipal])
    {
        if([[types lastObject] intValue]==XML_OBJECT_CONTENIDO)
        {
            [_currentImage setUrl:[_contentElement stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
            [_currentImage setFull:[_contentElement stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
            [[_currentItem images] addObject:_currentImage];
        }
    }
    else if([elementName isEqualToString:XML_ImagePrincipalFull])
    {
        if([[types lastObject] intValue]==XML_OBJECT_CONTENIDO)
        {
            [_currentImage setUrl:[_contentElement stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
            [_currentImage setFull:[_contentElement stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
            [[_currentItem images] addObject:_currentImage];
        }
    }
    else if([elementName isEqualToString:XML_ImagePrincipalThumbs])
    {
        if([[types lastObject] intValue]==XML_OBJECT_CONTENIDO)
        {
            [_currentImage setUrl:[_contentElement stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
            [_currentImage setFull:[_contentElement stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
            [[_currentItem images] addObject:_currentImage];
        }
    }
    else if([elementName isEqualToString:XML_object_Imagen])
    {
        if([[types lastObject] intValue]==XML_OBJECT_IMAGEN)
        {
            //Add image
            [[_currentItem images] addObject:_currentImage];
            
            //Elimino objeto app
            [types removeLastObject];
        }
    }
    else if([elementName isEqualToString:XML_FotoMiniA])
    {
        if([[types lastObject] intValue]==XML_OBJECT_IMAGEN)[_currentImage setUrl:[_contentElement stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
    }
    else if([elementName isEqualToString:XML_FotoFullA])
    {
        if([[types lastObject] intValue]==XML_OBJECT_IMAGEN)[_currentImage setFull:[_contentElement stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
    }
    else if([elementName isEqualToString:XML_Texto])
    {
        if([[types lastObject] intValue]==XML_OBJECT_IMAGEN)[_currentImage setText:[_contentElement stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
        else if([[types lastObject] intValue]==XML_OBJECT_CONTENIDO)[_currentItem setBody:[_contentElement stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
    }
    else if([elementName isEqualToString:XML_Pasos])
    {
        if([[types lastObject] intValue]==XML_OBJECT_IMAGEN)[_currentImage setText:[_contentElement stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
        else if([[types lastObject] intValue]==XML_OBJECT_CONTENIDO)[_currentItem setBody:[_contentElement stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
    }
    else if([elementName isEqualToString:XML_url_portadas])
    {
        if([[types lastObject] intValue]==XML_OBJECT_CONTENIDO)[_currentItem setUrlPortadas:[_contentElement stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
    }
    else if([elementName isEqualToString:XML_block_horoscopos])
    {
        //Block horoscopos
        [types removeLastObject];
    }
    else if([elementName isEqualToString:XML_object_prediccion] && _foundHoroscopo)
    {
        //Reset
        _foundAstro=NO;
        
        //Envio contenido (astro)
        if(_delegate && [_delegate respondsToSelector:@selector(xmlReaderDidParse:withObject:)])
        {
            NSLog(@"Enviando objecto horoscopo");
            [_delegate xmlReaderDidParse:self withObject:_currentAstro];
        }
        
        //Elimino typo
        [types removeLastObject];
    }
    else if([elementName isEqualToString:XML_dia])
    {
        if([[types lastObject] intValue]==XML_OBJECT_PREDICCION)
        {
            //Compare dates
            NSDateFormatter* df = [[NSDateFormatter alloc] init];
            [df setDateFormat:@"dd/MM/yy"];
            NSDate* d = [df dateFromString:_contentElement];
            
            NSDate* date = [NSDate date];
            
            NSCalendar *calendar = [NSCalendar currentCalendar];
            NSInteger comps = (NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit);
            
            NSDateComponents *date1Components = [calendar components:comps fromDate: d];
            NSDateComponents *date2Components = [calendar components:comps fromDate: date];
            
            d = [calendar dateFromComponents:date1Components];
            date = [calendar dateFromComponents:date2Components];
            
            NSComparisonResult result = [d compare:date];
            
            if (result == NSOrderedAscending) {}
            else if (result == NSOrderedDescending) {}
            else
            {
                //the same date
                _foundAstro=YES;
            }
        }
    }
    else if([elementName isEqualToString:XML_signo])
    {
        if([[types lastObject] intValue]==XML_OBJECT_PREDICCION  && _foundHoroscopo)
        {
            [_currentAstro setSigno:[_contentElement stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
            //[_currentAstro setTitle:[_contentElement stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
            //[_currentAstro setSignIconPath:[_contentElement stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
        }
    }
    if ([elementName isEqualToString:XML_recetas_id] && _foundRecetas)
    {
        if([[types lastObject] intValue]==XML_OBJECT_RECETAS)[_currentRecetas setIdRecetas:_contentElement];
    }
    if ([elementName isEqualToString:XML_recetas_titulo] && _foundRecetas)
    {
        if([[types lastObject] intValue]==XML_OBJECT_RECETAS)[_currentRecetas setTitulo:_contentElement];
    }
    if ([elementName isEqualToString:XML_recetas_tipo] && _foundRecetas)
    {
        if([[types lastObject] intValue]==XML_OBJECT_RECETAS)[_currentRecetas setTipo:_contentElement];
    }
    if ([elementName isEqualToString:XML_recetas_description] && _foundRecetas)
    {
        if([[types lastObject] intValue]==XML_OBJECT_RECETAS)[_currentRecetas setTexto:_contentElement];
    }
    if ([elementName isEqualToString:XML_recetas_id] && _foundDreams)
    {
        if([[types lastObject] intValue]==XML_OBJECT_SUEGNOS)[_currentDreams setIdDreams:_contentElement];
    }
    if ([elementName isEqualToString:XML_recetas_titulo] && _foundDreams)
    {
        if([[types lastObject] intValue]==XML_OBJECT_SUEGNOS)[_currentDreams setTitulo:_contentElement];
    }
    if ([elementName isEqualToString:XML_recetas_description] && _foundDreams)
    {
        if([[types lastObject] intValue]==XML_OBJECT_SUEGNOS)[_currentDreams setTexto:_contentElement];
    }
}

///////////////////////////////////////////////////////////////////////////
//	Metodos delegados
///////////////////////////////////////////////////////////////////////////

/**
 *	Función llamada al comienzo del parser
 ************************************************************/

- (void)parserDidStartDocument:(NSXMLParser *)parser
{
//    DLog();

	elementCounter = 0;
}

/**
 *	Función llamada cuando termina de parsear el documento
 ****************************************************************/

- (void)parserDidEndDocument:(NSXMLParser *)parser
{
//    DLog();
    
    if(_delegate && [_delegate respondsToSelector:@selector(xmlReaderDidFinish:)])
    {
        [_delegate xmlReaderDidFinish:self];
    }
}

/**
 *	Función de error en el parseo
 **************************************************/

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError
{
    DLog(@"%@",[parseError description]);
    
    if(_delegate && [_delegate respondsToSelector:@selector(xmlReaderDidFail:withError:)])
    {
        [_delegate xmlReaderDidFail:self withError:parseError];
    }
}

/**
 *	Función llamada cuando encuentra una etiqueta de comienzo de un elemento
 *********************************************************************************/

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if (qName) elementName = qName;
	
//    DLog(@"%@",elementName);
	
	//comprueba el elemento
	[self startElement:elementName:attributeDict];	
}

/**
 *	Función llamada cuando encuentra una etiqueta de fin de un elemento
 ******************************************************************************/

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{     
    if (qName) elementName = qName;
    
//    DLog(@"%@",elementName);
    
	//comprueba el elemento
	[self endElement:elementName];	
}

/**
 *	Función llamada cuando encuentra caracteres entre comienzo y final del elemento
 ************************************************************************************************/

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{	
	//NSLog(@"foundCharacter: %@",string);
		
	/*if(([string isEqualToString:@"\t"]) || 
	   ([string isEqualToString:@"\n"]) || 
	   ([string isEqualToString:@"\n\t"]) || 
	   ([string isEqualToString:@"\n\t\t"]) || 
	   ([string isEqualToString:@"\n\t\t\t"]) || 
	   ([string isEqualToString:@"\n\t\t\t\t"]))*/
	
	if([string rangeOfString:@"\t"].location!=NSNotFound)
		return;
	
	if([string rangeOfString:@"\n"].location!=NSNotFound)
		return;
	
	if (_contentElement)
	{
        [_contentElement appendString:string];
    }
}

- (void)parser:(NSXMLParser *)parser foundCDATA:(NSData *)CDATABlock
{
    //NSLog(@"foundCDATA: %@",[[NSString alloc] initWithData:CDATABlock encoding:NSUTF8StringEncoding]);
    
    if (_contentElement)
	{
        NSString *content = [[NSString alloc] initWithData:CDATABlock encoding:NSUTF8StringEncoding];
        [_contentElement appendString:content];
    }
}

@end

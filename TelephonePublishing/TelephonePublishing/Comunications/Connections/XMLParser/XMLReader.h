//
//  XMLReader.h
//  
//
//  Created by Julio Rivas on 16/03/10.
//  Copyright 2010 . All rights reserved.
//

#import <Foundation/Foundation.h>

@class XMLReader;
@class Recetas;
@class Dreams;
@class App;
@class Response;
@class Item;
@class Image;
@class HoroscopoMes;

@protocol XMLReaderDelegate <NSObject>
-(void)xmlReaderDidFinish:(XMLReader *)xmlReader;
-(void)xmlReaderDidFail:(XMLReader *)xmlReader withError:(NSError *)error;
-(void)xmlReaderDidParse:(XMLReader *)xmlReader withObject:(NSObject *)object;
@end

@interface XMLReader : NSObject <NSXMLParserDelegate>
{    
    //arbol nodos
    NSMutableArray *types;
	
	//sender
	NSInteger senderType;
	
	//elementos
	NSUInteger elementCounter;
	
	//contenido de un elemento del XML
    NSMutableString *_contentElement;
    
    //attributes
    NSDictionary *_attributes;
    
    //App
    App *_currentApp;
    
    //Item
    Item *_currentItem;
    
    //Image
    Image *_currentImage;
    
    //Recetas magicas
    Recetas *_currentRecetas;
    
    //Sueños
    Dreams *_currentDreams;
    
    //Astro
    BOOL _foundAstro;
    BOOL _foundHoroscopo;
    BOOL _foundRecetas;
    BOOL _foundDreams;
    HoroscopoMes *_currentAstro;
    
 

	
	//Delegado
	@private id<XMLReaderDelegate> __delegate;
}

- (void)parseXMLFileAtData:(NSData *)data;
- (void)parseXMLFileAtData:(NSData *)data :(NSUInteger)sender;

@property (nonatomic, retain) NSMutableString *contentElement;
@property (assign) id<XMLReaderDelegate> delegate;
@property (nonatomic, retain) Recetas *currentRecetas;
@property (nonatomic, retain) App *currentApp;
@property (nonatomic, retain) Response *currentResponse;
@property (nonatomic, retain) Item *currentItem;
@property (nonatomic, retain) Image *currentImage;
@property (nonatomic, retain) HoroscopoMes *currentAstro;
@property (nonatomic, retain) Dreams *currentDreams;

@end

//
//  App.m
//  Elle
//
//  Created by Julio Rivas on 28/08/12.
//  Copyright (c) 2012 TAPTAP Networks. All rights reserved.
//

#import "App.h"

@implementation App

@synthesize imagen;
@synthesize titulo;
@synthesize texto;
@synthesize tienda;

-(id)initApp
{
    if([self init])
    {
        
    }
    return self;
}

-(void)dealloc
{
    imagen=nil;
    titulo=nil;
    texto=nil;
    tienda=nil;
}

@end

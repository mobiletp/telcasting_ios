//
//  Astro.m
//  Elle
//
//  Created by Julio Rivas on 30/08/12.
//  Copyright (c) 2012 TAPTAP Networks. All rights reserved.
//

#import "Astro.h"

@implementation Astro

@synthesize endDateMark;
@synthesize signIconPath;
@synthesize startDateMark;
@synthesize title;
@synthesize amor;
@synthesize nombre;
@synthesize suerte;
@synthesize trabajo;
@synthesize shareUrl;

-(id)initAstro
{
    if([self init])
    {
        
    }
    return self;    
}

-(void)dealloc
{
    endDateMark=nil;
    signIconPath=nil;
    startDateMark=nil;
    title=nil;
    amor=nil;
    nombre=nil;
    suerte=nil;
    trabajo=nil;
    shareUrl=nil;
}

@end

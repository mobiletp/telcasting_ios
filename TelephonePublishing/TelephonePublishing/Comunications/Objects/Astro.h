//
//  Astro.h
//  Elle
//
//  Created by Julio Rivas on 30/08/12.
//  Copyright (c) 2012 TAPTAP Networks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Astro : NSObject
{
    NSString *endDateMark;
    NSString *signIconPath;
    NSString *startDateMark;
    NSString *title;
    //NSString *text;
    
    NSString *amor;
    NSString *nombre;
    NSString *suerte;
    NSString *trabajo;
    NSString *shareUrl;
}

-(id)initAstro;

@property (nonatomic,retain) NSString *endDateMark;
@property (nonatomic,retain) NSString *signIconPath;
@property (nonatomic,retain) NSString *startDateMark;
@property (nonatomic,retain) NSString *title;
//@property (nonatomic,retain) NSString *text;
@property (nonatomic,retain) NSString *amor;
@property (nonatomic,retain) NSString *nombre;
@property (nonatomic,retain) NSString *suerte;
@property (nonatomic,retain) NSString *trabajo;
@property (nonatomic,retain) NSString *shareUrl;


@end

//
//  App.h
//  Elle
//
//  Created by Julio Rivas on 28/08/12.
//  Copyright (c) 2012 TAPTAP Networks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface App : NSObject
{
    NSString *imagen;
    NSString *titulo;
    NSString *texto;
    NSString *tienda;
}

-(id)initApp;

@property (nonatomic,retain) NSString *imagen;
@property (nonatomic,retain) NSString *titulo;
@property (nonatomic,retain) NSString *texto;
@property (nonatomic,retain) NSString *tienda;

@end

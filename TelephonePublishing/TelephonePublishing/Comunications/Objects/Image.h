//
//  Image.h
//  Elle
//
//  Created by Julio Rivas on 30/08/12.
//  Copyright (c) 2012 TAPTAP Networks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Image : NSObject
{
    NSString *url;
    NSString *title;
    NSString *full;
    NSString *text;
}

-(id)initImage;

@property (nonatomic,retain) NSString *url;
@property (nonatomic,retain) NSString *title;
@property (nonatomic,retain) NSString *full;
@property (nonatomic,retain) NSString *text;

@end

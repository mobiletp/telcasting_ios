//
//  Item.h
//  Elle
//
//  Created by Julio Rivas on 29/08/12.
//  Copyright (c) 2012 TAPTAP Networks. All rights reserved.
//

#import <Foundation/Foundation.h>

@class NItem;

@interface Item : NSObject
{
    NSString *idItem;
    NSString *title;
    NSString *header;
    NSString *body;
    NSString *date;
    NSString *section;
    NSString *subsection;
    NSString *templateItem;
    NSString *shareUrl;
    NSMutableArray *images;
    NSString *dateFavorite;
    NItem *branded;
    NSString *urlPortadas;
}

-(id)initItem;

@property (nonatomic,retain) NSString *idItem;
@property (nonatomic,retain) NSString *title;
@property (nonatomic,retain) NSString *header;
@property (nonatomic,retain) NSString *body;
@property (nonatomic,retain) NSString *date;
@property (nonatomic,retain) NSString *section;
@property (nonatomic,retain) NSString *subsection;
@property (nonatomic,retain) NSString *templateItem;
@property (nonatomic,retain) NSString *shareUrl;
@property (nonatomic,retain) NSMutableArray *images;
@property (nonatomic,retain) NSString *dateFavorite;
@property (nonatomic,retain) NItem *branded; 
@property (nonatomic,retain) NSString *urlPortadas; 

@end

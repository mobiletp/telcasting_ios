//
//  Image.m
//  Elle
//
//  Created by Julio Rivas on 30/08/12.
//  Copyright (c) 2012 TAPTAP Networks. All rights reserved.
//

#import "Image.h"

@implementation Image

@synthesize url;
@synthesize title;
@synthesize full;
@synthesize text;

-(id)initImage
{
    if([self init])
    {

    }
    return self;
}

-(void)dealloc
{
    url=nil;
    title=nil;
    full=nil;
    text=nil;
}

@end

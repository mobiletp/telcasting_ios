//
//  Item.m
//  Elle
//
//  Created by Julio Rivas on 29/08/12.
//  Copyright (c) 2012 TAPTAP Networks. All rights reserved.
//

#import "Item.h"

@implementation Item

@synthesize idItem;
@synthesize title;
@synthesize header;
@synthesize body;
@synthesize date;
@synthesize section;
@synthesize subsection;
@synthesize templateItem;
@synthesize images;
@synthesize shareUrl;
@synthesize dateFavorite;
@synthesize branded;
@synthesize urlPortadas;

-(id)initItem
{
    if([self init])
    {
        images=[[NSMutableArray alloc] initWithCapacity:0];
    }
    return self;
}

-(void)dealloc
{
    branded=nil;
    idItem=nil;
    title=nil;
    header=nil;
    body=nil;
    date=nil;
    section=nil;
    subsection=nil;
    templateItem=nil;
    shareUrl=nil;
    [images removeAllObjects];
    images=nil;
    dateFavorite=nil;
    urlPortadas=nil;
}

@end

//
//  HMDataManager.m
//  Elle
//
//  Created by Julio Rivas on 28/08/12.
//  Copyright (c) 2012 TAPTAP Networks. All rights reserved.
//

#import "HMDataManager.h"
#import "FileManager.h"
#import "Reachability.h"
#import "EL_NSData+Base64.h"


@implementation HMDataManager

@synthesize delegate=_delegate;

#pragma mark - Utils

-(BOOL)validateConnection
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    
    NetworkStatus status = [reachability currentReachabilityStatus];
    
    switch (status) {
        case NotReachable:
            
            return NO;
            
            break;
            
        case ReachableViaWiFi:
        case ReachableViaWWAN:

            return YES;
            
            break;
        default:
            break;
    }
}

- (NSString *)toBase64String:(NSString *)string {
    NSData *data = [string dataUsingEncoding: NSUTF8StringEncoding];
    NSString *ret = [data base64EncodedString];    
    return ret;
}

- (NSString *)fromBase64String:(NSString *)string {
    NSData  *base64Data = [NSData dataFromBase64String:string];
    
    NSString* decryptedStr = [[NSString alloc] initWithData:base64Data encoding:NSUnicodeStringEncoding];
    
    return decryptedStr;
}

-(DataManagerType)getDataType:(NSString *)url
{
    DataManagerType type;
    
    NSString *str=@"";
    NSArray *array = [url componentsSeparatedByString:@"."];
    if(array && [array count]>0)str=[array objectAtIndex:[array count]-1];
    
    if(array && [array count]>0)
        str = [array objectAtIndex:[array count]-1];
    
    if ([[str uppercaseString] isEqualToString:@"XML"] || [[str uppercaseString] isEqualToString:@"PHP"])
    {
        type=DataManagerTypeXML;
    }   
    else if([[str uppercaseString] isEqualToString:@"JSON"])
    {
        type=DataManagerTypeJSON;
    }
    else
    {
        type=DataManagerTypeXML;
    }
    
    //Exception
    if([url rangeOfString:@"http://api.kewego.com"].location != NSNotFound)
    {
        type=DataManagerTypeJSON;
    }
    
    return type;
}

#pragma mark - LoadData

-(void)parseData:(NSString *)url
{
    //File name
    NSString *fileName = [self toBase64String:url];
    
    if(_dataManagerType==DataManagerTypeXML)
    {
        //Obtengo el contenido y lo parseo
        [self initXML:[[FileManager sharedInstance] readFileFromRoot:fileName]];
    }
    else if(_dataManagerType==DataManagerTypeJSON)
    {
        //Obtengo el contenido y lo parseo
        [self initJSON:[[FileManager sharedInstance] readFileFromRoot:fileName]];
    }        
}

-(BOOL)existData:(NSString *)url
{
    //File name
    NSString *fileName = [self toBase64String:url];

    return [[FileManager sharedInstance] existFile:fileName];
}

-(BOOL)checkData:(NSString *)url
{
    //File name
    NSString *fileName = [self toBase64String:url];
        
    //Compruebo si el fichero existe y está en fecha
    if([[FileManager sharedInstance] existFile:fileName] && [[FileManager sharedInstance] getCreationTimeInterval:fileName]<CACHE_TIME)
    {
        //Valid content
        return YES;
    }
    //Connection
    else
    {
        //Need refresh
        return NO;
    }
}

-(void)loadData:(NSString *)url withSender:(NSInteger)sender
{
    //Sender
    _sender=sender;
    
    //load data
    [self loadData:url];
}

-(void)loadData:(NSString *)url withParameters:(NSString *)parameters
{
    //Parameters
    _parameters=parameters;
    
    //Load
    [self loadData:url];
}

-(void)loadData:(NSString *)url
{
    DLog(@"%@",url);
    
    //Type
    _dataManagerType=[self getDataType:url];
    
    //Connection
    if([self validateConnection] && ![self checkData:url])
    {        
        //Diccionario
        NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:0];
        
        //Type
        if(_parameters && [_parameters length]>0)
        {
            //Post
            [dict setValue:kConnTypePost forKey:kDictConnType];
            
            //Parameters
            [dict setValue:_parameters forKey:kDictConnParams];
        }
        else
        {
            //Get
            [dict setValue:kConnTypeGet forKey:kDictConnType];
        }
        
        //Url (encoding)    
        [dict setValue:url forKey:kDictUrl];
        
        //Lanzo conexion
        [_connection makeConnectionWithDictionary:dict];
    }
    else
    {
        //Get file
        if([self checkData:url] || ![self validateConnection])
        {
            //Parse existing data
            [self parseData:url];
        }
        else
        {
            //Error: non connection and non file
            if(_delegate && [_delegate respondsToSelector:@selector(dataManagerDidFail:withError:)])
                [_delegate dataManagerDidFail:self withError:nil];
        }
    }
}

-(void)loadWebService:(NSString *)url withEnvelope:(NSString *)envelope andBody:(NSDictionary *)body
{
    DLog(@"url: %@",url);
    DLog(@"envelope: %@",envelope);
    DLog(@"body: %@",body);

    //Connection
    if([self validateConnection])
    {
        //Diccionario
        NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:0];
        
        //Post
        [dict setValue:kConnTypePost forKey:kDictConnType];
        
        //Url (encoding)
        [dict setValue:url forKey:kDictUrl];
        
        //Envelope
        [dict setValue:envelope forKey:kDictEnvelope];
        
        //Body
        [dict setValue:body forKey:kDictBody];
        
        //Lanzo conexion
        [_connection makeConnectionWithDictionary:dict];
    }
    else
    {
        //Error
        if(_delegate && [_delegate respondsToSelector:@selector(dataManagerDidFail:withError:)])
            [_delegate dataManagerDidFail:self withError:nil];
    }
}

#pragma mark - Init Manager

-(id)initHMDataManager
{
    if([self init])
    {
        //Sender
        _sender=-1;
        
        //Objeto conexion
        _connection = [[Connection alloc] initConnection];
        [_connection setDelegate:self];
        
        //JSON Parser
        _jsonParser = [[JSONParser alloc] initJSONParser];
        [_jsonParser setDelegate:self];
        
        //XMLParser
		_xmlReader = [[XMLReader alloc] init];
		[_xmlReader setDelegate:self];
        
    }
    return self;
}

#pragma mark - XML methods

-(void)initXML:(NSData *)data
{
    [_xmlReader parseXMLFileAtData:data :_sender];
}

-(void)xmlReaderDidFinish:(XMLReader *)xmlReader
{
    DLog();

    //Fin json parser
    [_delegate dataManagerDidFinish:self];
}

-(void)xmlReaderDidFail:(XMLReader *)xmlReader withError:(NSError *)error
{
    DLog(@"error: %@",[error description]);

    //Error
    [_delegate dataManagerDidFail:self withError:error];
}

-(void)xmlReaderDidParse:(XMLReader *)xmlReader withObject:(NSObject *)object
{
    //Objetos parseados
    [_delegate dataManagerDidParse:self withObject:object];
}

#pragma mark - JSON methods


-(void)initJSON:(NSData *)data
{
    //Parseo json
    [_jsonParser parseData:data];
}

#pragma mark - JSONParser delegates

-(void)jsonParserDidFinish:(JSONParser *)jsonParser
{
    DLog();
    
    //Fin json parser
    [_delegate dataManagerDidFinish:self];
}

-(void)jsonParserDidFail:(JSONParser *)jsonParser withError:(NSError *)error
{
    DLog(@"error: %@",[error description]);

    //Error
    [_delegate dataManagerDidFail:self withError:error];
}

-(void)jsonParserDidParse:(JSONParser *)jsonParser withObject:(NSObject *)object
{
    //Objetos parseados
    [_delegate dataManagerDidParse:self withObject:object];
}

#pragma mark - Connection delegates

-(void)connection:(Connection *)connection didFinishWithData:(NSData *)data
{
    //Respuesta
#ifdef DEBUG
//    NSString *string = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
//    DLog(@"%@",string);
#endif
    
    //Save file
    if(data && [data length]>0)
    {
        //Save file
        //FileName
        NSString *fileName = [self toBase64String:[connection url]];
        
        //Guardo fichero
        [[FileManager sharedInstance] saveFileInRoot:data withName:fileName];    
    }

    //Parse
    switch (_dataManagerType) {
        case DataManagerTypeXML:
            
            [self initXML:data];
            
            break;
            
        case DataManagerTypeJSON:
            
            [self initJSON:data];
            
            break;
            
        default:
            break;
    }
}

-(void)connection:(Connection *)connection didFailWithError:(NSError *)error
{
    if([self existData:[connection url]])
    {
        [self parseData:[connection url]];
    }        
    else
    {
        //Connection error
        [_delegate dataManagerDidFail:self withError:error];
    }    
}

#pragma mark - Memory managment

-(void)dealloc
{
    [_connection setDelegate:nil];
}

@end

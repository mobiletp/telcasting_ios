//
//  FileManager.h
//  
//
//  Created by Julio Rivas on 03/11/11.
//  Copyright (c) 2012. All rights reserved.
//

#import <Foundation/Foundation.h>

#define CACHE_TIME 10*60 //Seconds

@interface FileManager : NSObject
{
    //Main path
    NSString *mainPath;
}

+ (FileManager *)sharedInstance;
- (id) initFileManager;
- (void)createFolder:(NSString *)name;
- (void)createFolder:(NSString *)name inPath:(NSString *)path;
- (void)createFile:(NSString *)file;
- (BOOL)existFolder:(NSString *)name;
- (void)saveFileInRoot:(NSData *)data withName:(NSString *)name;
- (void)saveFile:(NSData *)data inPath:(NSString *)path;
- (void)saveFile:(NSData *)data inFolder:(NSString *)folder withName:(NSString *)name;
- (BOOL)existFile:(NSString *)name inFolder:(NSString *)folder;
- (BOOL)existFile:(NSString *)name;
- (void)removeFile:(NSString *)name inFolder:(NSString *)folder;
- (NSDictionary *)getAttributesFromFile:(NSString *)file inFolder:(NSString *)folder;
- (NSData *)readFile:(NSString *)file fromFolder:(NSString *)folder;
- (NSData *)readFileFromRoot:(NSString *)file;
- (NSArray *)filesInFolder:(NSString *)folder;
- (void)writeText:(NSString *)string inFile:(NSString *)file;
- (void)copyContentOfDirectory:(NSString *)directory;
- (NSDate *)getCreationTime:(NSString *)file;
- (CGFloat)getCreationTimeInterval:(NSString *)file;
- (void)listBundleContent;

@property (nonatomic, retain) NSString *mainPath;

@end

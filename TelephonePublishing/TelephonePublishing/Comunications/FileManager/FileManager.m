//
//  FileManager.m
//  
//
//  Created by Julio Rivas on 03/11/11.
//  Copyright (c) 2012. All rights reserved.
//

#import "FileManager.h"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Está clase es un singleton, es decir: solo se crea una vez y no se elimina hasta el fin de la aplicación 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

static FileManager *sharedCLDelegate = nil;

@implementation FileManager

@synthesize mainPath;

#pragma mark - Utilidades 

/**
 *  Copia el contenido de un directorio de recursos al sistema de ficheros
 *********************************************************************************/

-(void)copyContentOfDirectory:(NSString *)directory {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *documentDBFolderPath = documentsDirectory;//[documentsDirectory stringByAppendingPathComponent:directory];
    NSString *resourceDBFolderPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:directory];
    
    /*if (![fileManager fileExistsAtPath:documentDBFolderPath]) {
     //Create Directory!
     [fileManager createDirectoryAtPath:documentDBFolderPath withIntermediateDirectories:NO attributes:nil error:&error];
     } else {
     NSLog(@"Directory exists! %@", documentDBFolderPath);
     }*/
    
    
    NSArray *fileList = [fileManager contentsOfDirectoryAtPath:resourceDBFolderPath error:&error];
    DLog(@"fileList: %@",fileList);
    
    for (NSString *s in fileList) {
        NSString *newFilePath = [documentDBFolderPath stringByAppendingPathComponent:s];
        NSString *oldFilePath = [resourceDBFolderPath stringByAppendingPathComponent:s];
        if (![fileManager fileExistsAtPath:newFilePath]) {
            //File does not exist, copy it
            [fileManager copyItemAtPath:oldFilePath toPath:newFilePath error:&error];
        }else {
          DLog(@"File exists: %@", newFilePath);
        }
    }
}


/**
 *  Guarda un archivo en una ruta dada
 *******************************************/

-(void)saveFile:(NSData *)data inPath:(NSString *)path
{
    // write to file atomically (using temp file)
    [data writeToFile:path atomically:TRUE];
}

/**
 *  Guarda un archivo en un directorio
 *******************************************/

- (void)saveFile:(NSData *)data inFolder:(NSString *)folder withName:(NSString *)name
{
    [data writeToFile:[NSString stringWithFormat:@"%@/%@/%@",mainPath,folder,name] atomically:TRUE];
}

/**
 *  Guarda un archivo en el directorio raiz con un nombre dado
 *******************************************************************/

- (void)saveFileInRoot:(NSData *)data withName:(NSString *)name
{
#ifndef NDEBUG
    NSLog(@"[%@] %@: %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd),name);
#endif 

    [data writeToFile:[NSString stringWithFormat:@"%@/%@",mainPath,name] atomically:TRUE];
}

/**
 *  Existe archivo en una ruta definida
 *******************************************/

- (BOOL)existFile:(NSString *)name inFolder:(NSString *)folder
{
    //Path
    NSString *dataPath = [NSString stringWithFormat:@"%@/%@/%@",mainPath,folder,name];
    
    //Compruebo si existe
    return [[NSFileManager defaultManager] fileExistsAtPath:dataPath];
}

/**
 *  Existe archivo en root
 *******************************************/

- (BOOL)existFile:(NSString *)name
{
    //Path
    NSString *dataPath = [NSString stringWithFormat:@"%@/%@",mainPath,name];
    
    //Compruebo si existe
    return [[NSFileManager defaultManager] fileExistsAtPath:dataPath];
}

/**
 *  Elimina un archivo en una ruta seleccionada
 *******************************************/

- (void)removeFile:(NSString *)name inFolder:(NSString *)folder
{
    //Error
    NSError *error;
    
    //Path
    NSString *dataPath = [NSString stringWithFormat:@"%@/%@/%@",mainPath,folder,name];
    
    //Borro file
    [[NSFileManager defaultManager] removeItemAtPath:dataPath error:&error];
}

/**
 *  Crea un directorio en una ruta dada
 *******************************************/

-(void)createFolder:(NSString *)name inPath:(NSString *)path
{
    //Error
    NSError *error;
    
    //Path
    NSString *dataPath = [NSString stringWithFormat:@"%@/%@",path,name];
    
    //Compruebo si ya está creado
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
    {
        //Creo el directorio
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
    }
}

-(void)createFolder:(NSString *)name
{
    [self createFolder:name inPath:mainPath];
}

/**
 *  Crea un fichero en un folder
 *******************************************/

-(void)createFile:(NSString *)file
{
    //Path
    NSString *dataPath = [NSString stringWithFormat:@"%@/%@",mainPath,file];
    
    //Compruebo si ya está creado
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
    {
        //Attributes
        NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:[NSDate date], NSFileModificationDate, nil];

        NSLog(@"dict: %@",dict);
        
        //Creo el directorio
        [[NSFileManager defaultManager] createFileAtPath:dataPath contents:nil attributes:dict];
    }
}


/**
 *  Existe directorio
 *******************************************/

- (BOOL)existFolder:(NSString *)name
{
    //Path
    NSString *dataPath = [NSString stringWithFormat:@"%@/%@",mainPath,name];
    
    //Compruebo si existe
    return [[NSFileManager defaultManager] fileExistsAtPath:dataPath];
}

/**
 *	Obtiene los atributos del fichero
 *******************************************/

-(NSDictionary *)getAttributesFromFile:(NSString *)file inFolder:(NSString *)folder
{
    NSString *dataPath = [NSString stringWithFormat:@"%@/%@/%@",mainPath,folder,file];
    
    NSFileManager* fm = [NSFileManager defaultManager];    
    NSDictionary* attrs = [fm attributesOfItemAtPath:dataPath error:nil];
    
    /*     
     if (attrs != nil) {
     return (NSDate*)[attrs objectForKey: NSFileCreationDate];
     } else {
     return nil;
     }

     */
    
    return attrs;
}

/**
 *	Obtiene la fecha de creación
 *******************************************/

- (NSDate *)getCreationTime:(NSString *)file
{
    NSString *dataPath = [NSString stringWithFormat:@"%@/%@",mainPath,file];
         
    NSError *error;
    NSDictionary* attrs = [[NSFileManager defaultManager] attributesOfItemAtPath:dataPath error:&error];

#ifdef DEBUG
    //if(error)NSLog(@"error: %@",[error description]);                 
    //NSLog(@"attrs: %@",attrs);
#endif
    
    if (attrs != nil) {
        return (NSDate*)[attrs fileModificationDate];
    } else {
        return nil;
    }
}

/**
 *	Obtiene la diferencia de tiempo en segundos
 *****************************************************/

- (CGFloat)getCreationTimeInterval:(NSString *)file
{
    NSDate *fileDate = [self getCreationTime:file];
    
    NSTimeInterval fileDiff = [fileDate timeIntervalSinceNow];
    NSTimeInterval todaysDiff = [[NSDate date] timeIntervalSinceNow];
    NSTimeInterval dateDiff = todaysDiff - fileDiff;
    
    return dateDiff;
}

/**
 *	Lee los datos de una ruta dada
 *******************************************/

- (NSData *)readFile:(NSString *)file fromFolder:(NSString *)folder
{
    //path
    NSString *dataPath = [NSString stringWithFormat:@"%@/%@/%@",mainPath,folder,file];
    
    //Obtengo data
    NSData *data = [[NSFileManager defaultManager] contentsAtPath:dataPath];    
    
    //Data
    return data;
}

/**
 *	Lee los datos de root
 *******************************************/

- (NSData *)readFileFromRoot:(NSString *)file
{
    //path
    NSString *dataPath = [NSString stringWithFormat:@"%@/%@",mainPath,file];
    
    //Obtengo data
    NSData *data = [[NSFileManager defaultManager] contentsAtPath:dataPath];    
    
    //Data
    return data;
}

/**
 *	Archivos en una carpeta
 *******************************************/

- (NSArray *)filesInFolder:(NSString *)folder
{
    DLog(@"filesInFolder:");
    
    //Contenido del folder
    NSDirectoryEnumerator* en = [[[NSFileManager alloc] init] enumeratorAtPath:[NSString stringWithFormat:@"%@/%@",mainPath,folder]];

    //Array
    NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:0];
    
    NSString* file;
    while (file = [en nextObject]) {

        //Meto nombre fichero
        [array addObject:file];
    }
    
    return array;
}

/**
 *	Método que escribe un texto pasado como parámetro en un fichero
 ***********************************************************************/

- (void)writeText:(NSString *)string inFile:(NSString *)fileName
{
    NSData* data=[string dataUsingEncoding:NSUTF8StringEncoding];        
    NSFileHandle *file = [NSFileHandle fileHandleForUpdatingAtPath:[NSString stringWithFormat:@"%@/%@",mainPath,fileName]];
    
    if (file == nil)
    {
        DLog(@"Failed to open file");
    }
    
    //[file seekToFileOffset: 10];    
    [file writeData: data];
    [file closeFile];    
}

-(void)listBundleContent
{
#ifdef DEBUG
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSArray *fileList = [fileManager contentsOfDirectoryAtPath:[[NSBundle mainBundle] resourcePath] error:&error];
    DLog(@"fileList: %@",fileList);
#endif
}

#pragma mark - Init

/**
 *	Función de inicialización
 *******************************************/

- (id) initFileManager
{
	if ([self init])
	{						
        //Obtengo path de ficheros de la app
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *dataPath = [paths objectAtIndex:0];        
        DLog(@"dataPath: %@",dataPath);
        
        //Main path
        mainPath = [[NSString alloc] initWithString:dataPath];
    }
	return self;
}

#pragma mark - Singleton

//ver "Creating a Singleton Instance" en la guia de Cocoa

+ (FileManager *)sharedInstance
{
    @synchronized(self)
	{
        if (sharedCLDelegate == nil)
		{
            [[self alloc] init];
        }
    }
    return sharedCLDelegate;
}

+ (id)allocWithZone:(NSZone *)zone
{
    @synchronized(self)
	{
        if (sharedCLDelegate == nil)
		{
            sharedCLDelegate = [super allocWithZone:zone];
			//asigna y retorna la primera vez
            return sharedCLDelegate;
        }
    }
	//retorna nill las demás veces
    return nil;
}

- (id)copyWithZone:(NSZone *)zone
{
    return self;
}
/*
- (id)retain
{
    return self;
}

- (unsigned)retainCount
{
	//nos dice que es un objeto que no puede ser "released"
    return UINT_MAX;
}

- (void)release
{
    //no hace nada
}

- (id)autorelease
{
    return self;
}*/

@end

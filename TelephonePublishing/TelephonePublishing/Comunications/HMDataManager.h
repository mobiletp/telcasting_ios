//
//  HMDataManager.h
//  Elle
//
//  Created by Julio Rivas on 28/08/12.
//  Copyright (c) 2012 TAPTAP Networks. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Connection.h"
#import "XMLReader.h"
#import "JSONParser.h"

enum {
    SENDER_FEED,
    SENDER_COMMENTS
};

typedef enum {
    DataManagerTypeXML = 0,
    DataManagerTypeJSON
} DataManagerType;

#define WEBSERVICE_URL @"wp-comments-post-app.php"

#define kCommentsUrlBlog @"UrlBlog"
#define kCommentsIdPost @"comment_post_ID"
#define kCommentsNombre @"author"
#define kCommentsEmail @"email"
#define kCommentsUrl @"url"
#define kCommentsComentario @"comment"

@class HMDataManager;

@protocol HMDataManagerDelegate <NSObject>
- (void)dataManagerDidFinish:(HMDataManager *)dataManager;
- (void)dataManagerDidParse:(HMDataManager *)dataManager withObject:(NSObject *)object;
- (void)dataManagerDidFail:(HMDataManager *)dataManager withError:(NSError *)error;
@end

@interface HMDataManager : NSObject <ConnectionDelegate,JSONParserDelegate,XMLReaderDelegate>
{
    //Type (XML or JSON)
    NSInteger _dataManagerType;
    
    //Parameters
    NSString *_parameters;
    
    //Conexion
    Connection *_connection;
    
    //JSON Parser
    JSONParser *_jsonParser;

    //XML
    XMLReader *_xmlReader;
    
    //Sender
    NSInteger _sender;
    
    //Delegado
    @private id<HMDataManagerDelegate> __delegate;
}

-(id)initHMDataManager;
-(void)loadData:(NSString *)url;
-(void)loadData:(NSString *)url withSender:(NSInteger)sender;
-(void)loadWebService:(NSString *)url withEnvelope:(NSString *)envelope andBody:(NSDictionary *)body;
-(void)loadData:(NSString *)url withParameters:(NSString *)parameters;

@property (assign) id<HMDataManagerDelegate> delegate;

@end

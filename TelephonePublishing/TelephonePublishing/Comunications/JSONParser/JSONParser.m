//
//  JSONParser.m
//  TAPStart
//
//  Created by Julio Rivas on 12/06/12.
//  Copyright (c) 2012 TAPTAPNetworks. All rights reserved.
//

#import "JSONParser.h"
#import "JSONKit.h"
#import "JSONConstants.h"
#import "TPUtils.h"
//#import "Video.h"
#import "Item.h"
#import "Image.h"
#import "Astro.h"

@implementation JSONParser

@synthesize delegate=_delegate;

#pragma mark - Init JSONParser

- (id)initJSONParser
{
    if([self init])
    {
        //Decoder
        _decoder = [[JSONDecoder alloc] init];
    }
    return self;
}


#pragma mark - Parse utilities

/**
 *  Comprueba si el diccionario contiene una key
 ******************************************************/

-(BOOL)dictContainsKey:(NSDictionary *)dict :(NSString *)key
{
    //Keys
    NSArray *array = [dict allKeys];
    
    //Recorro keys
    for(NSString *str in array)
    {
        if ([str rangeOfString:key].location != NSNotFound)
            return YES;
    }
        
    return NO;
}

#pragma mark - Private methods

/**
 *  Video parser
 **************************************/

/*-(void)parseVideos:(NSArray *)array
{
    //Recorro array
    for(NSDictionary *dict in array)
    {        
        //Video
        [_currentVideo release];
        _currentVideo = [[Video alloc] initVideo];
        
        [_currentVideo setSig:[dict valueForKey:JSON_sig]];
        [_currentVideo setCreationDate:[dict valueForKey:JSON_creation_date]];
        [_currentVideo setCreationTime:[dict valueForKey:JSON_creation_time]];
        [_currentVideo setTitle:[dict valueForKey:JSON_title]];
        [_currentVideo setCounter:[[dict valueForKey:JSON_counter] intValue]];
        [_currentVideo setRating:[[dict valueForKey:JSON_rating] intValue]];
        [_currentVideo setDuration:[[dict valueForKey:JSON_duration] intValue]];
        [_currentVideo setAuthor:[dict valueForKey:JSON_author]];
        [_currentVideo setThumbnail:[dict valueForKey:JSON_thumbnail]];
        [_currentVideo setContent:dict];
        
        //Informo objeto parseado
        [_delegate jsonParserDidParse:self withObject:_currentVideo];
    }
}*/

/*-(void)parseKewego:(NSDictionary *)dict
{
    //kewego response
    if([dict valueForKey:JSON_message])
    {
        //Dict con result
        dict = [dict valueForKey:JSON_message];
        
        //Contenido
        if([dict valueForKey:JSON_video])
        {
            if([[dict valueForKey:JSON_total_result] intValue]>1)
            {
                //Parseo videos
                [self parseVideos:[NSArray arrayWithArray:[dict valueForKey:JSON_video]]];
            }
            else
            {
                //Parse video
                [self parseVideos:[NSArray arrayWithObject:[dict valueForKey:JSON_video]]];
            }
        }
    }
}*/

/**
 *  Item parser
 **************************************/

-(void)parseImages:(NSArray *)array
{
    //Recorro array
    for(NSDictionary *dict in array)
    {        
        //[_currentImage release];
        _currentImage = [[Image alloc] initImage];
        
        [_currentImage setUrl:[dict valueForKey:JSON_url]];
        [_currentImage setTitle:[dict valueForKey:JSON_title]];
        [_currentImage setFull:[dict valueForKey:JSON_full]];
        [_currentImage setText:[dict valueForKey:JSON_text_image]];
        
        [[_currentItem images] addObject:_currentImage];
    }
}

-(void)parseItems:(NSArray *)array
{
    //Recorro array
    for(NSDictionary *dict in array)
    {        
        //Item
        //[_currentItem release];
        _currentItem = [[Item alloc] initItem];
        
        [_currentItem setIdItem:[dict valueForKey:JSON_id]];
        [_currentItem setTitle:[dict valueForKey:JSON_title]];
        [_currentItem setHeader:[dict valueForKey:JSON_header]];
        [_currentItem setBody:[dict valueForKey:JSON_body]];
        [_currentItem setDate:[dict valueForKey:JSON_date]];
        [_currentItem setSection:[dict valueForKey:JSON_section]];
        [_currentItem setSubsection:[dict valueForKey:JSON_subsection]];
        [_currentItem setTemplateItem:[dict valueForKey:JSON_template]];
        [_currentItem setShareUrl:[dict valueForKey:JSON_share_url]];
        
        //Images
        [self parseImages:[NSArray arrayWithArray:[dict valueForKey:JSON_images]]];
        
        //Informo objeto parseado
        [_delegate jsonParserDidParse:self withObject:_currentItem];
    }
}

/**
 *  Astro parser
 **************************************/

-(void)parseAstros:(NSDictionary *)dict
{        
    NSDictionary *horoscope = [[NSDictionary alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:JSON_astro_plist ofType:@"plist"]];
    NSDictionary *data;
    
    //Signes
    NSArray *signes = [NSArray arrayWithArray:[dict valueForKey:JSON_signes]];
    
    //Keys
    NSArray *keys = [[NSArray alloc] initWithArray:[horoscope valueForKey:JSON_sorted_signs]];
    for(NSString *key in keys)
    {
        //Data        
        data = [[NSMutableDictionary alloc] initWithDictionary:[[horoscope valueForKey:JSON_signs_definition] valueForKey:key]];
                
        //Astro
        //[_currentAstro release];
        _currentAstro = [[Astro alloc] initAstro];
        [_currentAstro setTitle:NSLocalizedString([data valueForKey:JSON_localizationKey], @"")];
        [_currentAstro setSignIconPath:[data valueForKey:JSON_signIconPath]];
        [_currentAstro setEndDateMark:[data valueForKey:JSON_endDateMark]];
        [_currentAstro setStartDateMark:[data valueForKey:JSON_startDateMark]];
        
        //Info
        for(NSDictionary *d in signes)
        {
            if([[d valueForKey:JSON_signes_nombre]isEqualToString:key])
            {
                [_currentAstro setNombre:[d valueForKey:JSON_signes_nombre]];
                [_currentAstro setAmor:[d valueForKey:JSON_signes_amor]];
                [_currentAstro setSuerte:[d valueForKey:JSON_signes_suerte]];
                [_currentAstro setTrabajo:[d valueForKey:JSON_signes_trabajo]];
                [_currentAstro setShareUrl:[d valueForKey:JSON_signes_share_url]];
                
                break;
            }
        }
        
        //Delegate
        [_delegate jsonParserDidParse:self withObject:_currentAstro];
    }
}

#pragma mark - Public Methods

- (void)parseData:(NSData *)data
{
    //Decode data
    NSDictionary *decodedData = [[NSDictionary alloc] initWithDictionary:[_decoder objectWithData:data]];
    
    //Parse type
    if([self dictContainsKey:decodedData :JSON_items])
    {
        //Items
        [self parseItems:[NSArray arrayWithArray:[decodedData valueForKey:JSON_items]]];
    }
    else if([self dictContainsKey:decodedData :JSON_template] && [[decodedData valueForKey:JSON_template] isEqualToString:JSON_horoscope])
    {
        //Astro
        [self parseAstros:decodedData];
    }
    
    //Informo fin parseo
    [_delegate jsonParserDidFinish:self];
}

#pragma mark - Memory managment

-(void)dealloc
{

}

@end

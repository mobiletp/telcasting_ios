//
//  JSONConstants.h
//  TAPStart
//
//  Created by Julio Rivas on 12/06/12.
//  Copyright (c) 2012 TAPTAPNetworks. All rights reserved.
//

#ifndef TAPStart_JSONConstants_h
#define TAPStart_JSONConstants_h

#define JSON_kewego_response @"kewego_response"
#define JSON_message @"message"
#define JSON_total_result @"total_result"
#define JSON_video @"video"
#define JSON_KqId @"KqId"

#define JSON_sig @"sig"
#define JSON_creation_date @"creation_date"
#define JSON_creation_time @"creation_time"
#define JSON_title @"title"
#define JSON_counter @"counter"
#define JSON_rating @"rating"
#define JSON_duration @"duration"
#define JSON_author @"author"
#define JSON_thumbnail @"thumbnail"

#define JSON_items @"items"

#define JSON_id @"id"
#define JSON_title @"title"
#define JSON_header @"header"
#define JSON_body @"body"
#define JSON_date @"date"
#define JSON_section @"section"
#define JSON_subsection @"subsection"
#define JSON_template @"template"
#define JSON_images @"images"
#define JSON_url @"url"
#define JSON_full @"full"
#define JSON_share_url @"share_url"
#define JSON_text_image @"text_image"

#define JSON_template @"template"
#define JSON_horoscope @"horoscope"
#define JSON_signes @"signes"
#define JSON_signes_nombre @"nombre"
#define JSON_signes_amor @"amor"
#define JSON_signes_suerte @"suerte"
#define JSON_signes_trabajo @"trabajo"
#define JSON_signes_share_url @"share_url"

#define JSON_astro_plist @"horoscope"
#define JSON_sorted_signs @"sortedSigns"
#define JSON_signs_definition @"signsDefinition"

#define JSON_localizationKey @"localizationKey"
#define JSON_endDateMark @"endDateMark"
#define JSON_signIconPath @"signIconPath"
#define JSON_startDateMark @"startDateMark"


#endif

//
//  JSONParser.h
//  TAPStart
//
//  Created by Julio Rivas on 12/06/12.
//  Copyright (c) 2012 TAPTAPNetworks. All rights reserved.
//

#import <Foundation/Foundation.h>

@class JSONParser;
@class JSONDecoder;
@class Video;
@class Item;
@class Image;
@class Astro;

@protocol JSONParserDelegate <NSObject>
-(void)jsonParserDidFinish:(JSONParser *)jsonParser;
-(void)jsonParserDidFail:(JSONParser *)jsonParser withError:(NSError *)error;
-(void)jsonParserDidParse:(JSONParser *)jsonParser withObject:(NSObject *)object;
@end

@interface JSONParser : NSObject
{
    //Decoder
    JSONDecoder *_decoder;
    
    //Video
    Video *_currentVideo;
    
    //Item
    Item *_currentItem;
    
    //Image
    Image *_currentImage;
    
    //Astro
    Astro *_currentAstro;
            
    //Delegado
    @private id<JSONParserDelegate> __delegate;
}

- (id)initJSONParser;
- (void)parseData:(NSData *)data;

@property (assign) id<JSONParserDelegate> delegate;

@end

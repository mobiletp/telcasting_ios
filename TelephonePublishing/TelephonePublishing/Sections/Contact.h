//
//  ContactData.h
//  HearstMagazines
//
//  Created by Julio Rivas on 14/02/14.
//  Copyright (c) 2014 TAPTAP Networks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Contact : NSObject

@property(retain) NSString *company;
@property(retain) NSString *web;
@property(retain) NSString *contactInfo;
@property(retain) NSString *logo;
@property(retain) NSString *logo2x;
@property(retain) NSString *contacto;
@property(retain) NSString *contacto2x;
@property(retain) NSString *cookieInfo;
@property(retain) NSString *sign;
@property(retain) NSString *appetecibles;

- (id) initWithDictionary:(NSDictionary *)dict;

@end

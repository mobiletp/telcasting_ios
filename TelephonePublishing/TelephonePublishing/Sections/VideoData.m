//
//  VideoData.m
//  HearstMagazines
//
//  Created by Julio Rivas on 14/02/14.
//  Copyright (c) 2014 TAPTAP Networks. All rights reserved.
//

#import "VideoData.h"

@implementation VideoData

- (id) initWithDictionary:(NSDictionary *)dict
{
    self=[super init];
    
	if (self){
        _tokenUrl=[dict valueForKey:@"tokenUrl"];
        _canal=[dict valueForKey:@"canal"];
        _desCategory=[dict valueForKey:@"desCategoria"];
    }
    return self;
}

@end

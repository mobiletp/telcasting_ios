//
//  ContactData.m
//  HearstMagazines
//
//  Created by Julio Rivas on 14/02/14.
//  Copyright (c) 2014 TAPTAP Networks. All rights reserved.
//

#import "Contact.h"

@implementation Contact

- (id) initWithDictionary:(NSDictionary *)dict{
	self=[super init];
    
	if (self){
        _company = [dict valueForKey:@"company"];
        _web = [dict valueForKey:@"web"];
        _contactInfo = [dict valueForKey:@"contactInfo"];
        _logo = [dict valueForKey:@"logo"];
        _logo2x = [dict valueForKey:@"logo2x"];
        _contacto = [dict valueForKey:@"contacto"];
        _contacto2x = [dict valueForKey:@"contacto2x"];
        _cookieInfo = [dict valueForKey:@"cookieInfo"];
        _sign = [dict valueForKey:@"sign"];
        _appetecibles = [dict valueForKey:@"appetecibles"];
    }
    return self;
}

@end

//
//  ContactData.h
//  HearstMagazines
//
//  Created by Julio Rivas on 14/02/14.
//  Copyright (c) 2014 TAPTAP Networks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Images : NSObject

@property(retain) NSString *imagesG;
@property(retain) NSString *imagesGExt;
@property(retain) NSString *imagesP;
@property(retain) NSString *imagesPExt;

- (id) initWithDictionary:(NSDictionary *)dict;

@end

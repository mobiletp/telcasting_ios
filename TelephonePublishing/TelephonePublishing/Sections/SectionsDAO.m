//
//  SectionsDAO.m
//  As.com-iPad
//
//  Created by Angel Garcia Olloqui on 11/05/10.
//  Copyright 2010 Mi Mundo iPhone. All rights reserved.
//

#import "SectionsDAO.h"
#import "Reachability.h"
#import "TPConstants.h"

#define CONFIG_SECTIONS @"sections"
#define CONFIG_CONFIG @"config"

#define CONFIG_NOTIFICATIONS @"notifications"
#define CONFIG_SONATA @"sonata"
#define CONFIG_NATIVE_ADS @"nativeAds"
#define CONFIG_NIELSEN @"nielsen"
#define CONFIG_DOUBLECLICK @"doubleClick"
#define CONFIG_COMSCORE @"comScore"
#define CONFIG_LEGAL_LIB @"legalLib"
#define CONFIG_VIDEO @"video"
#define CONFIG_CONTACT @"contact"
#define CONFIG_CARTELERAURL @"carteleraurl"
#define CONFIG_IMAGES @"images"
#define CONFIG_FAVORITE @"favourite"
#define CONFIG_SPLASH @"splash"
#define CONFIG_TUTORIAL @"tutorial"
#define CONFIG_BRAND_EXPERIENCE @"brandExperience"

@interface SectionsDAO (private)

+ (NSArray *) readLocalConfig;
+ (NSArray *) readRemoteConfig;
+ (void)showAlertMsg:(NSString*)msg;
@end

@implementation SectionsDAO

static NSArray *sMainSection=nil;
static NSString *notificationsServerUrl;
static NSString *notificationsAppId;
static SectionAdType adType;
static NSString * sonataAdsKey = @"";
static BOOL nativeAds=NO;
static NSString * nativeAdsKey = @"";
static BOOL nielsen=NO;
static NSString * nielsenPixeltrack=@"";
static NSDictionary *adProperties;
static BOOL legalLib=NO;
static NSString * legalLibUrl = @"";
static BOOL legalVersionControl=NO;
static BOOL comScore=NO;
static NSDictionary *comScoreProperties;
static BOOL video=NO;
static VideoData * videoData=nil;
static Contact * contactData=nil;
static Images * imagesData=nil;
static Section *favoriteSection=nil;
static BOOL hasFavoriteSection=NO;
static BOOL hasSplashImage=NO;
static BOOL splashAnimation=NO;
static NSString * splashUrl = @"";
static BOOL showTutorial=NO;
static BOOL brandExperience=NO;
static NSString * brandExperienceImageToken = @"";
static CGFloat adGalleryTime = 20.0; //Default time



#pragma mark - Check if the device has a retina display

+ (BOOL) isRetinaDevice
{
    CGFloat scale = 1.0;
    UIScreen* screen = [UIScreen mainScreen];
    if ([UIScreen instancesRespondToSelector:@selector(scale)])
    {
        scale = [screen scale];
    }
    
    if (2.0 == scale)
    {
        //iPhone o iPod Touch con RETINA DISPLAY
        return YES;
    } else {
        //iPhone o iPod Touch sin RETINA DISPLAY
        return NO;
    }
}

+ (NSString *)notificationsServerUrl {
    return notificationsServerUrl;
}

+ (NSString *)notificationsAppId {
    return notificationsAppId;
}

+ (CGFloat)adGalleryTime
{
    return adGalleryTime;
}

+ (SectionAdType)adType
{
    return adType;
}

+ (NSString *)sonataAdsKey
{
    return sonataAdsKey;
}




+ (BOOL)nativeAds
{
    return nativeAds;
}

+ (NSString *)nativeAdsKey
{
    return nativeAdsKey;
}

+ (BOOL)nielsen
{
    return nielsen;
}

+ (NSString *)nielsenPixeltrack
{
    return nielsenPixeltrack;
}

+ (BOOL)legal
{
    return legalLib;
}

+ (NSString *)legalUrl
{
    return legalLibUrl;
}

+ (BOOL)legalVersionControl
{
    return legalVersionControl;
}

+ (BOOL)comScore
{
    return comScore;
}

+ (NSDictionary *)comScoreProperties
{
    return comScoreProperties;
}

+ (BOOL)video
{
    return video;
}

+ (VideoData *)videoData
{
    return videoData;
}

+ (Contact *)contactData
{
    return contactData;
}

+ (Images *)imagesData
{
    return imagesData;
}

+ (NSDictionary *)adProperties
{
    return adProperties;
}

+ (Section *)favoriteSection
{
    return favoriteSection;
}

+ (BOOL)hasFavoriteSection
{
    return hasFavoriteSection;
}

+ (BOOL)hasSplashImage
{
    return hasSplashImage;
}

+ (BOOL)splashAnimation
{
    return splashAnimation;
}

+ (NSString *)splashUrl
{
    return splashUrl;
}

+ (BOOL)showTutorial
{
    return showTutorial;
}

+ (BOOL)brandExperience
{
    return brandExperience;
}

+ (NSString *)brandExperienceImageToken
{
    return brandExperienceImageToken;
}

+ (NSArray *) mainSections {
	@synchronized(self){
		if (sMainSection==nil){
			
            //Sections
            NSMutableArray *sections = [[NSMutableArray alloc]init];
			
            //Vars
			NSArray *configuration = nil;
            NSDictionary *parsed =  nil;
            NSArray *arraySections = nil;
            
            //Reachability
            Reachability *reachability = [[Reachability reachabilityForInternetConnection] retain];
            [reachability startNotifier];
            
            NetworkStatus status = [reachability currentReachabilityStatus];
            
            switch (status) {
                case NotReachable:
                    //[self showAlertMsg:NSLocalizedString(@"_Msg_no_connection",@"")];		                    
                    break;
                    
                case ReachableViaWiFi:
                case ReachableViaWWAN:
                    //Leemos el fichero de configuracion desde remoto
                    configuration = [self readRemoteConfig];
                    
                    break;
                default:
                    break;
            }
            [reachability release];
            
			if (configuration == nil)
            {
				//Si no se ha encontrado ningun fichero en remoto, y en no habia una version en cache, leemos el Default
				configuration = [self readLocalConfig];
            }
            
            //Congutation to Dictoionary
            if(configuration && [configuration isKindOfClass:[NSDictionary class]])
            {
                parsed = (NSDictionary *)configuration;
                DLog(@"%@",parsed);
            
                //Parse config
                [self parseConfig:[parsed objectForKey:CONFIG_CONFIG]];
                
                //Parse sections
                arraySections = [NSArray arrayWithArray:[parsed objectForKey:CONFIG_SECTIONS]];
                
                //Parse sections
                for (NSDictionary *dict in arraySections)
                {
                    Section *section = [[Section alloc] initWithDictionary:dict];
                    if ([self isRetinaDevice])
                    {
                        section.icon = [self downloadIcon:section.logo2x];
                    }
                    else
                    {
                        section.icon = [self downloadIcon:section.logo];
                    }
                    [sections addObject:section];
                    [section release];
                }
                sMainSection = sections;
            }
		}
	}
	return sMainSection;
}


#pragma mark - Private methods

+ (void)parseConfig:(NSDictionary *)dictionary
{
    NSDictionary *dict=nil;
    for(NSString *key in [dictionary allKeys])
    {
        dict = [dictionary valueForKey:key];
        DLog(@"%@ - %@",key,dict);
        if ([[dict valueForKey:@"enabled"] boolValue])
        {
            // Notifications
            if ([key isEqualToString:CONFIG_NOTIFICATIONS]) {
                notificationsServerUrl = [dict valueForKey:@"url"];
                notificationsAppId     = [dict valueForKey:@"appId"];
            }
            //Sonata
            else if ([key isEqualToString:CONFIG_SONATA])
            {
                adType=SectionAdTypeTapTap;
                sonataAdsKey=[dict valueForKey:@"adkey"];
                if([dict valueForKey:@"galleryTime"])
                {
                    adGalleryTime=[[dict valueForKey:@"galleryTime"] floatValue];
                }
            }
            //NativeAds
            else if([key isEqualToString:CONFIG_NATIVE_ADS])
            {
                nativeAds=YES;
                nativeAdsKey=[dict valueForKey:@"adkey"];
            }
            //Nielsen
            else if([key isEqualToString:CONFIG_NIELSEN])
            {
                nielsen=YES;
                nielsenPixeltrack=[dict valueForKey:@"pixeltrack"];
            }
            //DoubleClick
            else if([key isEqualToString:CONFIG_DOUBLECLICK])
            {
                adType=SectionAdTypeHearst;
                adProperties=[[NSDictionary alloc] initWithDictionary:dict];
            }
            //ComScore
            else if([key isEqualToString:CONFIG_COMSCORE])
            {
                comScore=YES;
                comScoreProperties=[[NSDictionary alloc] initWithDictionary:dict];
            }
            //LegalLib
            else if([key isEqualToString:CONFIG_LEGAL_LIB])
            {
                legalLib=YES;
                legalLibUrl=[dict valueForKey:@"url"];
                legalVersionControl=[[dict valueForKey:@"versionControl"] boolValue];
            }
            //Video
            else if([key isEqualToString:CONFIG_VIDEO])
            {
                video=YES;
                videoData=[[VideoData alloc] initWithDictionary:dict];
            }
            //Contact and appetecibles
            else if([key isEqualToString:CONFIG_CONTACT])
            {
                contactData=[[Contact alloc] initWithDictionary:dict];
            }
            //Images
            else if([key isEqualToString:CONFIG_IMAGES])
            {
                imagesData=[[Images alloc] initWithDictionary:dict];
            }

            //Favorite section
            else if([key isEqualToString:CONFIG_FAVORITE])
            {
                hasFavoriteSection=YES;
                favoriteSection = [[Section alloc] initWithDictionary:[dict valueForKey:@"section"]];
            }
            //Splash
            else if([key isEqualToString:CONFIG_SPLASH])
            {
                hasSplashImage=YES;
                splashAnimation= [[dict valueForKey:@"animation"] boolValue];
                splashUrl=[dict valueForKey:@"url"];
            }
            //Tutorial
            else if([key isEqualToString:CONFIG_TUTORIAL])
            {
                showTutorial=YES;
            }
            //BrandExperience
            else if([key isEqualToString:CONFIG_BRAND_EXPERIENCE])
            {
                brandExperience=YES;
                brandExperienceImageToken=[dict valueForKeyPath:@"imageToken"];
            }
           
        }else{
            if([key isEqualToString:CONFIG_IMAGES])
            {
                imagesData=[[Images alloc] initWithDictionary:dict];
            }
        }
    }
}

#define CONFIG_TUTORIAL @"tutorial"
#define CONFIG_BRAND_EXPERIENCE @"brandExperience"

+ (NSArray *) readLocalConfig
{
	//Path
	NSString *filePath = [NSString stringWithFormat:@"%@/Documents/%@", NSHomeDirectory(), CONFIG_SECTIONS_FILE_NAME];
    NSString* path = [[NSBundle mainBundle] pathForResource:@"Config"
                                                     ofType:@"json"];
    NSData *data = [[NSFileManager defaultManager] contentsAtPath:filePath];
    NSData *data2 = [[NSFileManager defaultManager] contentsAtPath:path];
    //Vars
	NSError* error = nil;
    
    //Serialize JSON
    if (data) {
        NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
        NSLog(@"error: %@",error);
        if(error || !jsonArray)
        {
            return nil;
        }
        else
        {
            return jsonArray;
        }
        
    }
    else
    {
        NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:data2 options:NSJSONReadingMutableContainers error:&error];
        return jsonArray;
    }
    return nil;
}

+ (NSArray*) readRemoteConfig
{
    //Request
	NSURLRequest* chRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:CONFIG_SECTIONS_URL] cachePolicy: NSURLRequestReloadIgnoringCacheData timeoutInterval:10];
    
    
    //Vars
	NSError* error = nil;

    //Response
    NSData* response = [NSURLConnection sendSynchronousRequest:chRequest returningResponse:nil error:&error];
    
    //Serialize JSON
    NSArray *jsonArray=nil;
    if(response && !error)
    {
        jsonArray = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:&error];
    }
    
    //Save response
    if(error || !jsonArray)
    {
        return nil;
    }
    else
    {
    	//Save data
        NSString *filePath = [NSString stringWithFormat:@"%@/Documents/%@", NSHomeDirectory(), CONFIG_SECTIONS_FILE_NAME];
        if ([jsonArray count] > 0)
        {
            [response writeToFile:filePath atomically:YES]; //Cambiado a response para guardar el json sin parsear
        }
        else
        {
            jsonArray = [NSArray arrayWithContentsOfFile:filePath];
        }

        return jsonArray;
    }
}

+ (void)showAlertMsg:(NSString*)msg {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"_Alert", @"")
                                                    message:msg
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"_Ok_alert_button_title", @"")
                                          otherButtonTitles:nil];		
    [alert show];
    [alert release];
}


+ (NSString *)downloadIcon:(NSString *)url {
	
    if (url==nil) return nil;
	NSString *nameIcon = [[url pathComponents] lastObject];
	
	NSString *iconPath = NSHomeDirectory();
	iconPath = [[iconPath stringByAppendingPathComponent:@"Documents"] 
				stringByAppendingPathComponent:nameIcon];
	
	//Si el icono no se encuentra en la carpeta local:
	if (![[NSFileManager defaultManager] fileExistsAtPath:iconPath]){
		
//		[[Reachability sharedReachability] setHostName:url];
        [[Reachability reachabilityForInternetConnection] startNotifier];	
		NetworkStatus internetConectionStatus = [[Reachability reachabilityForInternetConnection] currentReachabilityStatus];	
		
		//Si no hay conexion ponemos uno por defecto		
		if (internetConectionStatus == NotReachable)
        {
            DLog(@"Set icon");
		}
        else
        {
            DLog(@"Downloading icon");

			//En otro caso lo descargamos
			NSURLRequest *theRequest=[NSURLRequest requestWithURL:[NSURL URLWithString:url]								  
													  cachePolicy:NSURLRequestReturnCacheDataElseLoad
												  timeoutInterval:5.0];
			NSData *iconData = [NSURLConnection sendSynchronousRequest:theRequest returningResponse:nil error:nil];
			if ([iconData length]>0){
				[[NSFileManager defaultManager] createFileAtPath:iconPath contents:iconData attributes:nil];
			}
		}		
	}
	return iconPath;
}

@end

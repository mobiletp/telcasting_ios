//
//  ContactData.m
//  HearstMagazines
//
//  Created by Julio Rivas on 14/02/14.
//  Copyright (c) 2014 TAPTAP Networks. All rights reserved.
//

#import "Images.h"

@implementation Images

- (id) initWithDictionary:(NSDictionary *)dict{
	self=[super init];
    
	if (self){
        _imagesG = [dict valueForKey:@"imagenes_g"];
        _imagesGExt = [dict valueForKey:@"imag_g_ext"];
        _imagesP = [dict valueForKey:@"imagenes_p"];
        _imagesPExt = [dict valueForKey:@"imag_p_ext"];
    }
    return self;
}

@end

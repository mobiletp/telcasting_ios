//
//  SectionsDAO.h
//  As.com-iPad
//
//  Created by Angel Garcia Olloqui on 11/05/10.
//  Copyright 2010 Mi Mundo iPhone. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Section.h"
#import "VideoData.h"
#import "Contact.h"
#import "Images.h"

typedef enum {
    SectionAdTypeNone = 0,
    SectionAdTypeTapTap,
    SectionAdTypeHearst    
} SectionAdType;

#define SectionAdTypeKey @"publicidad"
#define SectionAdTypeTapTapKey @"T"
#define SectionAdTypeHearstKey @"H"
#define SectionNativeAdsKey @"nativeAds"

#define SectionNielsenKey @"marcadoNielsen"
#define SectionComscoreKey @"marcadoComscore"
#define SectionLegalLib @"showLegalLib"
#define SectionYesKey @"S"
#define SectionNoKey @"N"
#define SectionPropertiesKey @"properties"
#define SectionUrlKey @"url"
#define SectionVersionControlKey @"versionControl"

@interface SectionsDAO : NSObject

+ (NSArray *) mainSections;

//Notifications
+ (NSString *)notificationsServerUrl;
+ (NSString *)notificationsAppId;

//Sonata Ads
+ (SectionAdType)adType;
+ (NSString *)sonataAdsKey;
+ (NSDictionary *)adProperties;
+ (CGFloat)adGalleryTime;

//Native
+ (BOOL)nativeAds;
+ (NSString *)nativeAdsKey;

//Nielsen
+ (BOOL)nielsen;
+ (NSString *)nielsenPixeltrack;

//LegalLib
+ (BOOL)legal;
+ (NSString *)legalUrl;
+ (BOOL)legalVersionControl;

//ComScore
+ (BOOL)comScore;
+ (NSDictionary *)comScoreProperties;

//Video
+ (BOOL)video;
+ (VideoData *)videoData;

//Contact and appetecibles
+ (Contact *)contactData;


//Images recurso
+ (Images *)imagesData;

//Imagen Recurso List Url
+  (NSString *)legalUrl;

//Favorite
+ (BOOL)hasFavoriteSection;
+ (Section *)favoriteSection;

//Splash animation
+ (BOOL)hasSplashImage;
+ (BOOL)splashAnimation;
+ (NSString *)splashUrl;

//Tutorial
+ (BOOL)showTutorial;

//Brand experience
+ (BOOL)brandExperience;
+ (NSString *)brandExperienceImageToken;



//Icons
+ (NSString *)downloadIcon:(NSString *)url;

@end

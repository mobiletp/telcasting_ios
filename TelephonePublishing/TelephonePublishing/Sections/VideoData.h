//
//  VideoData.h
//  HearstMagazines
//
//  Created by Julio Rivas on 14/02/14.
//  Copyright (c) 2014 TAPTAP Networks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VideoData : NSObject

@property(retain) NSString *canal;
@property(retain) NSString *tokenUrl;
@property(retain) NSString *desCategory;

- (id) initWithDictionary:(NSDictionary *)dict;

@end

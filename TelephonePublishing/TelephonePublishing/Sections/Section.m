//
//  Section.m
//  iMagazine_iPhone
//
//  Created by Javier de Francisco Monte on 10/01/12.
//  Copyright (c) 2012 TAPTAPNetworks. All rights reserved.
//

#import "Section.h"
#import "TPUtils.h"
#import "TPConstants.h"

@implementation Section

@synthesize name=_name, viewType=_viewType, logo=_logo, logo2x=_logo2x, properties = _properties;
@synthesize advertiseKey = _advertiseKey, className = _className, viewName = _viewName;
@synthesize icon = _icon;
@synthesize url=_url;
@synthesize cellMenuColor=_cellMenuColor;
@synthesize imgRecursoList=_imgRecursoList;

- (id) initWithDictionary:(NSDictionary *)dict{	
	self=[super init];	
    
	if (self){		
		
		//Guardamos los valores fijos
		self.name = [dict objectForKey:@"name"];
		self.viewType = [dict objectForKey:@"type"];
		self.logo = [dict objectForKey:@"logo"];
		self.properties = [dict objectForKey:@"doubleClick"];
		self.logo2x = [dict objectForKey:@"logo2x"];
        self.advertiseKey = [dict objectForKey:@"adkey"];
        self.className = [dict objectForKey:@"className"];
        self.viewName = [dict objectForKey:@"viewName"];
        self.url = [dict objectForKey:@"url"];
        
        //Menu cell color (format: #FFCE0000)
        if([dict valueForKey:@"cellMenuColor"] && [[dict valueForKey:@"cellMenuColor"] length])
        {
            self.cellMenuColor = [TPUtils colorFromHexString:[dict valueForKey:@"cellMenuColor"]];
        }
        else
        {
            self.cellMenuColor = MENU_CELL_COLOR;
        }
        
        
        //Menu cell img Recurso List
        if([dict valueForKey:@"imgRecursoList"] && [[dict valueForKey:@"imgRecursoList"] length])
        {
            self.imgRecursoList = [dict valueForKey:@"imgRecursoList"];
        }
        else
        {
            self.imgRecursoList = @"img_listado_default.png";
        }

        
        
        
        
	}
	return self;
}

- (void) dealloc {
	self.name = nil;
	self.viewType = nil;
	self.logo = nil;
	self.logo2x = nil;
	self.properties = nil;
    self.advertiseKey = nil;
    self.className  = nil;
    self.viewName = nil;
    self.icon = nil;
    self.url = nil;
	self.cellMenuColor = nil;
    self.imgRecursoList = nil;
}

@end

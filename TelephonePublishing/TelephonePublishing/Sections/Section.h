//
//  Section.h
//  iMagazine_iPhone
//
//  Created by Javier de Francisco Monte on 10/01/12.
//  Copyright (c) 2012 TAPTAPNetworks. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kViewTypeNews       @"newsView"
#define kViewTypeGalleries  @"galleriesView"
#define kViewTypeVideos     @"videosView"
#define kViewTypeWebView    @"webView"
#define kViewTypeListView   @"listView"
#define kViewTypeVideosView @"videosView"
#define kViewTypeHoroscopoView   @"horoscopoView"
#define kViewTypePortadasView   @"portadasView"
#define kViewTypePortadaCita   @"portadaCita"

#define SectionPubsup @"pubsup"
#define SectionPubinf @"pubinf"
#define SectionPubinfdet @"pubinfdet"
#define SectionPubinfgal @"pubinfgal"

@interface Section : NSObject {
    
    NSString *_name;	
	NSString *_viewType;
	NSString *_logo;	
	NSString *_logo2x;
	NSDictionary *_properties;
    NSString *_advertiseKey;
    NSString *_className;
    NSString *_viewName;  
    NSString *_icon;
    NSString *_url;
    UIColor *_cellMenuColor;
    NSString *_imgRecursoList;
}

@property(retain) NSString *name;
@property(retain) NSString *viewType;
@property(retain) NSString *logo;
@property(retain) NSString *logo2x;
@property(retain) NSDictionary *properties;
@property(retain) NSString *advertiseKey;
@property(retain) NSString *className;
@property(retain) NSString *viewName;
@property(retain) NSString *icon;
@property(retain) NSString *url;
@property (retain) UIColor *cellMenuColor;
@property(retain) NSString *imgRecursoList;

- (id) initWithDictionary:(NSDictionary *)dict;

@end

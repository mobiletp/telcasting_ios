//
//  HMUtils.m
//  Elle
//
//  Created by Julio Rivas on 23/08/12.
//  Copyright (c) 2012 TAPTAP Networks. All rights reserved.
//

#import "TPUtils.h"
#import "TPConstants.h"
//#import "DFPBannerView.h"
//#import "TTNCustomNavBar.h"
#import "Reachability.h"
//#import "DMMenuView.h"
//#import "HMGeneral.h"
//#import "HMListViewController.h"
//#import "DMNavigationController.h"

@implementation TPUtils

#pragma mark - Reachability

+ (BOOL)validateConnection
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    
    NetworkStatus status = [reachability currentReachabilityStatus];
    
    switch (status) {
        case NotReachable:
            
            return NO;
            
            break;
            
        case ReachableViaWiFi:
        case ReachableViaWWAN:

            return YES;
            
            break;
        default:
            break;
    }

}

#pragma mark -  Labels height

/*+(CGFloat) calculateHeightOfTextFromWidth:(NSString *)text :(UIFont*)withFont :(CGFloat)width :(NSLineBreakMode)lineBreakMode
{	
	[text retain];
	[withFont retain];
	CGSize suggestedSize = [text sizeWithFont:withFont constrainedToSize:CGSizeMake(width,FLT_MAX) lineBreakMode:lineBreakMode];
	[text release];
	[withFont release];
	
	return suggestedSize.height;
}*/

+(CGFloat) calculateHeightOfLabelFromWidth:(UILabel *)label :(CGFloat)width
{
    return [self calculateHeightOfTextFromWidth:label.text :label.font :width :NSLineBreakByWordWrapping];
}

+(CGFloat) calculateHeightOfTextFromWidth:(NSString *)text :(UIFont*)withFont :(CGFloat)width
{
    return [self calculateHeightOfTextFromWidth:text :withFont :width :NSLineBreakByWordWrapping];
}

+ (CGFloat)calculateHeightOfLabel:(UILabel*)label
{        
    CGSize suggestedSize = [label.text sizeWithFont:label.font constrainedToSize:CGSizeMake(label.frame.size.width,FLT_MAX) lineBreakMode:label.lineBreakMode];
    
    return suggestedSize.height;
}

#pragma mark - Retina

+ (BOOL) isRetinaDevice
{
    CGFloat scale = 1.0;
    UIScreen* screen = [UIScreen mainScreen];
    if ([UIScreen instancesRespondToSelector:@selector(scale)])
    {
        scale = [screen scale];
    }
    
    if (2.0 == scale)
    {
        //iPhone o iPod Touch con RETINA DISPLAY
        return YES;
    } else {
        //iPhone o iPod Touch sin RETINA DISPLAY
        return NO;
    }
}

+ (BOOL) isRetina4Device
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        if (([UIScreen mainScreen].bounds.size.height * [UIScreen mainScreen].scale) >=1136)
        {
            return YES;
        }
    }
    
    return NO;
}


#pragma mark - NavBar

/**
 *  Navigation bar Left menu button
 ***********************************************/

+(UIButton *)addLeftBackButton :(id)target :(SEL)selector
{
    UIButton *aButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [aButton setImage:[UIImage imageNamed:@"icon_back02.png"] forState:UIControlStateNormal];
    //[aButton setImage:[UIImage imageNamed:@"btn_mainMenuSelected.png"] forState:UIControlStateHighlighted];    
    aButton.frame = CGRectMake(0.0, 0.0, 34, 34);
    
    // Set the Target and Action for aButton
    [aButton addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    
    return aButton;
}


+(UIBarButtonItem *)addLeftBackBarButton :(id)target :(SEL)selector
{
    // Initialize the UIButton
    UIButton *aButton = [self addLeftBackButton:target :selector];
    
    // Initialize the UIBarButtonItem
    UIBarButtonItem *aBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:aButton];
    
    // Then you can add the aBarButtonItem to the UIToolbar
    //[self.navigationItem setLeftBarButtonItem:aBarButtonItem];
    
    return aBarButtonItem;
}

/**
 *  Navigation controller
 ***********************************************/

/*+ (UINavigationController *)customizedNavigationController :(UIImage *)backgroundImage
{
    UINavigationController *navController = [[UINavigationController alloc] initWithNibName:nil bundle:nil];
    
    // Ensure the UINavigationBar is created so that it can be archived. If we do not access the
    // navigation bar then it will not be allocated, and thus, it will not be archived by the
    // NSKeyedArchvier.
    [navController navigationBar];
    
    // Archive the navigation controller.
    NSMutableData *data = [NSMutableData data];
    NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:data];
    [archiver encodeObject:navController forKey:@"root"];
    [archiver finishEncoding];
    [archiver release];
    [navController release];
    
    // Unarchive the navigation controller and ensure that our UINavigationBar subclass is used.
    NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
    [unarchiver setClass:[TTNCustomNavBar class] forClassName:@"UINavigationBar"];
    UINavigationController *customizedNavController = [unarchiver decodeObjectForKey:@"root"];
    [unarchiver finishDecoding];
    [unarchiver release];
    
    // Modify the navigation bar to have a background image.
    TTNCustomNavBar *navBar = (TTNCustomNavBar *)[customizedNavController navigationBar];
    //[navBar setTintColor:[UIColor colorWithRed:0.39 green:0.72 blue:0.62 alpha:1.0]];
    //[navBar setTintColor:[UIColor clearColor]];
    [navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
    //[navBar setBackgroundImage:[UIImage imageNamed:@"navigation-bar-bg-landscape.png"] forBarMetrics:UIBarMetricsLandscapePhone];
    
    return customizedNavController;
}*/

+ (UIButton *)backLeftButton:(id)target :(SEL)selector :(UIImage *)image :(UIColor *)color
{
    UIButton *customButton = [[UIButton alloc] initWithFrame:CGRectMake(NAVBAR_BACK_BUT_X, NAVBAR_BACK_BUT_Y, NAVBAR_BACK_BUT_WIDTH, NAVBAR_BACK_BUT_HEIGHT)];
    //[customButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    //[customButton setImage:[UIImage imageNamed:@"backButton.png"] forState:UIControlStateNormal];
    [customButton setTitle:NSLocalizedString(@"_BUT_BACK", nil) forState:UIControlStateNormal];
    [customButton.titleLabel setFont:NAVBAR_MENU_BUT_FONT];
    [customButton setTitleColor:color forState:UIControlStateNormal];
    [customButton setTitleColor:color forState:UIControlStateSelected];
    [customButton addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    [customButton setBackgroundColor:[UIColor clearColor]];
    [customButton setTitleEdgeInsets:UIEdgeInsetsMake(NAVBAR_BACK_BUT_MARGIN_TOP, NAVBAR_BACK_BUT_MARGIN_LEFT, 0, 0)];
    
    //Image
    UIImageView *img = [[UIImageView alloc] initWithImage:image];
    img.frame = CGRectMake(NAVBAR_BACK_IMG_X, NAVBAR_BACK_IMG_Y, NAVBAR_BACK_IMG_WIDTH, NAVBAR_BACK_IMG_HEIGHT);
    [customButton addSubview:img];
    
    //Target
    [customButton addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    
    return customButton;
}

+ (UIButton *)backLeftButton:(id)target :(SEL)selector
{
    return [self backLeftButton:target :selector :[UIImage imageNamed:@"ico_navbar_back@2x.png"] :[UIColor blackColor]];
}

+ (UIButton *)recognitionLeftButton:(id)target :(SEL)selector :(CGRect)frame
{
    UIButton *but = [[UIButton alloc] initWithFrame:frame];
    [but setImage:[UIImage imageNamed:@"camera"] forState:UIControlStateNormal];
    [but addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    return but;
}

/*+ (UIBarButtonItem *)backLeftButtonBar:(id)target :(SEL)selector
{
    //Back button    
    UIButton *customButton = [self backLeftButton:target :selector];    
    
    UIImageView *img = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"backButton.png"]];
    img.frame = CGRectMake(0, 7, img.image.size.width, img.image.size.height);
    [customButton addSubview:img];
    
    //[customButton addSubview:customLabel];
    //[customLabel release];
    
    UIBarButtonItem *customLeftButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:customButton] autorelease];   
    [customButton release];
    
    return customLeftButtonItem;
}*/

+ (UIButton *)favoritesRightButton:(id)target :(SEL)selector :(UIImage *)image :(UIColor *)color
{
    //Button
    UIButton *customButton = [[UIButton alloc] initWithFrame:CGRectMake(NAVBAR_FAV_BUT_X, NAVBAR_FAV_BUT_Y, NAVBAR_FAV_BUT_WIDTH, NAVBAR_FAV_BUT_HEIGHT)];    
    [customButton setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
    [customButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
    [customButton setTitle:NSLocalizedString(@"_BUT_MY_FAVORITES", nil) forState:UIControlStateNormal];
    [customButton.titleLabel setFont:NAVBAR_MENU_BUT_FONT];
    [customButton.titleLabel setTextColor:UIColorFromRGB(COLOR_BLACK)];
    [customButton setTitleColor:color forState:UIControlStateNormal];
    [customButton setTitleColor:color forState:UIControlStateSelected];
    [customButton addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    [customButton setTitleEdgeInsets:UIEdgeInsetsMake(NAVBAR_MENU_BUT_MARGIN_TOP, 0, 0, 0)];
    [customButton setBackgroundColor:[UIColor clearColor]];
    //[customButton setImage:[UIImage imageNamed:@"ico_navbar_menu_articles.png"] forState:UIControlStateNormal];
    
    //Image
    UIImageView *img = [[UIImageView alloc] initWithImage:image];
    img.frame = CGRectMake(NAVBAR_MY_ELLE_IMG_X, NAVBAR_MY_ELLE_IMG_Y, NAVBAR_MY_ELLE_IMG_WIDTH, NAVBAR_MY_ELLE_IMG_HEIGHT);
    [customButton addSubview:img];
    
    //Action
    [customButton addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    
    return customButton;

}

+ (UIButton *)favoritesRightButton:(id)target :(SEL)selector
{
    return [self favoritesRightButton :target :selector  :[UIImage imageNamed:@"ico_my_elle.png"] :[UIColor blackColor]];
}

/*+ (UIButton *)menuRightButton:(id)target :(SEL)selector :(UIImage *)image :(UIColor *)color
{
    //Button
    UIButton *customButton = [[UIButton alloc] initWithFrame:CGRectMake(NAVBAR_MENU_BUT_X, NAVBAR_MENU_BUT_Y, NAVBAR_MENU_BUT_WIDTH, NAVBAR_MENU_BUT_HEIGHT)];
    if(SECTIONS_TYPE_MENU==MENU_TYPE_WHEEL || SECTIONS_TYPE_MENU==MENU_TYPE_GRID)
    {
        [customButton setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
        [customButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
        [customButton setTitleEdgeInsets:UIEdgeInsetsMake(NAVBAR_MENU_BUT_MARGIN_TOP, 0, 0, 0)];
    }
    else if(SECTIONS_TYPE_MENU==MENU_TYPE_LIST)
    {
        [customButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [customButton setTitleEdgeInsets:UIEdgeInsetsMake(NAVBAR_MENU_LIST_BUT_MARGIN, 0, 0, 0)];
    }
    [customButton setTitle:NSLocalizedString(@"_BUT_MENU", nil) forState:UIControlStateNormal];
    [customButton.titleLabel setFont:NAVBAR_MENU_BUT_FONT];
    [customButton.titleLabel setTextColor:UIColorFromRGB(COLOR_BLACK)];
    [customButton setTitleColor:color forState:UIControlStateNormal];
    [customButton setTitleColor:color forState:UIControlStateSelected];
    [customButton addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    [customButton setBackgroundColor:[UIColor clearColor]];
    
    //Image
    UIImageView *img = [[UIImageView alloc] initWithImage:image];
    img.frame = CGRectMake(NAVBAR_MENU_IMG_X, NAVBAR_MENU_IMG_Y, NAVBAR_MENU_IMG_WIDTH, NAVBAR_MENU_IMG_HEIGHT);
    [customButton addSubview:img];
    
    //Action
    [customButton addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    
    return customButton;
}*/

/*+ (void)pushMenuInViewController:(UIViewController *)viewController
{
    //Show menu
    DMMenuView *menuView = [[DMMenuView alloc] init];
    [menuView setParentViewController:viewController];
    [viewController.view addSubview:menuView];
}*/

/*+ (void)pushHomeInViewController:(UIViewController *)viewController
{
    if([[HMGeneral sharedInstance] currentSection]!=[[[HMGeneral sharedInstance] sectionsArray] objectAtIndex:0]
       || [[HMGeneral sharedInstance] contactShown])
    {
        [[HMGeneral sharedInstance] setContactShown:NO];
        [[HMGeneral sharedInstance] setCurrentSection:[[[HMGeneral sharedInstance] sectionsArray] objectAtIndex:0]];
        
        //ViewController
        UIViewController *listController = [[HMListViewController alloc] init];
        [(HMListViewController *)listController setCurrentSection:[[HMGeneral sharedInstance] currentSection]];
        
        //Transition
        [(DMNavigationController *)viewController.navigationController setRootViewController:listController];
    }
    else
    {
        //Transition
        [(DMNavigationController *)viewController.navigationController popToRootViewControllerAnimated:YES];
    }
}*/

+ (UIButton *)menuRightButton:(id)target :(SEL)selector
{
    return [self menuRightButton:target :selector :[UIImage imageNamed:@"ico_navbar_menu_articles.png"] :[UIColor blackColor]];
}

/*+ (UIBarButtonItem *)menuRightBarButton:(id)target :(SEL)selector
{
    UIButton *customButton = [self menuRightButton:target :selector];
    
    UIBarButtonItem *customRightButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:customButton] autorelease];
    [customButton release];
    
    return customRightButtonItem;
}*/

+ (UIView *)createTitleBar:(NSString *)title
{
    UIView *titleBar = [[UIView alloc] initWithFrame:CGRectMake(TITLE_BAR_X, TITLE_BAR_Y, TITLE_BAR_WIDTH, TITLE_BAR_HEIGHT)];
    titleBar.backgroundColor = UIColorFromRGB(COLOR_BLACK);
    
    UILabel *titleLabel = [self labelCreate:TITLE_LABEL_X :TITLE_LABEL_Y :TITLE_LABEL_WIDTH :TITLE_LABEL_HEIGHT :TITLE_BAR_FONT :UIColorFromRGB(COLOR_BLACK) :1];
    titleLabel.textColor = UIColorFromRGB(COLOR_WHITE);
    titleLabel.textAlignment = NSTextAlignmentLeft;
    titleLabel.text = [title uppercaseString];
    [titleBar addSubview:titleLabel];
    //[titleBar release];
    
    return titleBar;
}

//+(UIView *)createNavBar
//{
//    //NavBar
//    UIImageView *navBar = [[UIImageView alloc] initWithFrame:CGRectMake(CVC_NAV_BAR_X, CVC_NAV_BAR_Y, CVC_NAV_BAR_WIDTH, CVC_NAV_BAR_HEIGHT)];
//    navBar.image = [UIImage imageNamed:@"navbar.png"];
//    navBar.backgroundColor = [UIColor clearColor];
//    
//    return navBar;
//}

+ (void)createNavBar:(UIViewController *)viewController :(SEL)selector
{
    //NavBar
    UIView *navBarView = [[UIView alloc] initWithFrame:CGRectMake(NAVBAR_X, NAVBAR_Y, NAVBAR_WIDTH, NAVBAR_HEIGHT)];
    [navBarView setBackgroundColor:[UIColor clearColor]];
    [viewController.view addSubview:navBarView];
    
    //NavBar
    UIImageView *navBar = [[UIImageView alloc] initWithFrame:CGRectMake(CVC_NAV_BAR_X, CVC_NAV_BAR_Y, CVC_NAV_BAR_WIDTH, CVC_NAV_BAR_HEIGHT)];
    navBar.image = [UIImage imageNamed:@"navbar.png"];
    navBar.backgroundColor = [UIColor clearColor];
    navBar.frame = CGRectMake(CVC_NAV_BAR_X, NAVBAR_HEIGHT-CVC_NAV_BAR_HEIGHT, CVC_NAV_BAR_WIDTH, CVC_NAV_BAR_HEIGHT);
    
    //Add navbar
    [navBarView addSubview:navBar];
    
    //Button
    UIButton *homeButton = [TPUtils buttonCreate:NAVBAR_HOME_BUT_X :NAVBAR_HOME_BUT_Y :NAVBAR_HOME_BUT_WIDTH :NAVBAR_HOME_BUT_HEIGHT :nil :nil :nil :@""];
    [homeButton addTarget:viewController action:selector forControlEvents:UIControlEventTouchUpInside];
    [homeButton setBackgroundColor:[UIColor clearColor]];
    [navBarView addSubview:homeButton];
}

#pragma mark - Create custom items

/*+(void)listAllFonts
{
    // List all fonts on iPhone
    NSArray *familyNames = [[NSArray alloc] initWithArray:[UIFont familyNames]];
    NSArray *fontNames;
    NSInteger indFamily, indFont;
    for (indFamily=0; indFamily<[familyNames count]; ++indFamily)
    {
        NSLog(@"Family name: %@", [familyNames objectAtIndex:indFamily]);
        fontNames = [[NSArray alloc] initWithArray:
                     [UIFont fontNamesForFamilyName:
                      [familyNames objectAtIndex:indFamily]]];
        for (indFont=0; indFont<[fontNames count]; ++indFont)
        {
            NSLog(@"    Font name: %@", [fontNames objectAtIndex:indFont]);
        }
        [fontNames release];
    }
    [familyNames release];
}*/

/**
 *  Create custom button
 ***********************************************/

+ (UIButton *)buttonCreate :(NSUInteger)x :(NSUInteger)y :(NSUInteger)width :(NSUInteger)height :(UIImage *)img :(UIColor *)textCol :(UIFont *)font :(NSString *)title
{	
	UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];	
	
	//asigna las imagenes
	if(img!=nil)[button setBackgroundImage:img forState:UIControlStateNormal];
    
	//define la posición y el tamaño del botón (coor x, cor Y, ancho, alto)
	[button setFrame:CGRectMake(x, y, width, height)];
	
	//texto del botón
	if(textCol!=nil) 
		[button setTitleColor:textCol forState:UIControlStateNormal];
	else 
		[button setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
	//[button setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
	
	//Fuente para el titulo
	if(font==nil)
		[button.titleLabel setFont:[UIFont boldSystemFontOfSize:13]];	
	else
		button.titleLabel.font = font;		
	
	//texto del botón
	if(title!=nil) [button setTitle:title forState:UIControlStateNormal];
	
	return button;
}

/**
 *  Create custom label
 ***********************************************/

+ (UILabel *)labelCreate :(NSUInteger)x :(NSUInteger)y :(NSUInteger)width :(NSUInteger)height :(UIFont *)font :(UIColor *)textCol :(NSUInteger)numLines
{
	//Creamos etiqueta
	UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
	
	//Alineacion
	label.textAlignment = NSTextAlignmentLeft;
	
	//BreakMode
	label.lineBreakMode = NSLineBreakByWordWrapping;
    
	//Número de lineas
	label.numberOfLines = numLines;
	
	//Fuente
	label.font = font;
	
	//Minima fuente
	label.adjustsFontSizeToFitWidth=YES;
	//label.minimumFontSize = 12;
    
	//Color de la fuente
	if(textCol)label.textColor = textCol;
	label.backgroundColor = [UIColor clearColor];
	
	return label;
}

#pragma mark - String

//Remove html tags into a nsstring
+ (NSString *)flattenHTML:(NSString *)html 
{	
    NSScanner *theScanner;
    NSString *text = nil;
	
    theScanner = [NSScanner scannerWithString:html];
	
    while ([theScanner isAtEnd] == NO) {
		
        // find start of tag
        [theScanner scanUpToString:@"<" intoString:NULL] ; 
		
        // find end of tag
        [theScanner scanUpToString:@">" intoString:&text] ;
		
        // replace the found tag with a space
        //(you can filter multi-spaces out later if you wish)
        html = [html stringByReplacingOccurrencesOfString:
				[ NSString stringWithFormat:@"%@>", text]
											   withString:@""];
		
    } // while //
    
    return html;
	
}

/*+ (NSString *)escapeUrlString:(NSString *)url
{
    NSString * escapedUrlString = (NSString *)CFURLCreateStringByAddingPercentEscapes(NULL, (CFStringRef)[url stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding], NULL, (CFStringRef)@"!*'();:@&=+$,/?%#[]", kCFStringEncodingUTF8);
    return escapedUrlString;
}*/

#pragma mark - Date

/*+ (NSDateFormatter *)astroSignDateFormatter
{
	static NSString *kAstroSignDateFormatter = @"kAstroSignDateFormatter";
	NSMutableDictionary *threadDict = [[NSThread currentThread] threadDictionary];
	
	NSDateFormatter *astroFriendsDateFormatter = [threadDict objectForKey:kAstroSignDateFormatter];
	if (!astroFriendsDateFormatter) {
		astroFriendsDateFormatter = [[[NSDateFormatter alloc] init] autorelease];
		astroFriendsDateFormatter.dateFormat = @"MMMM dd";
		[threadDict setObject:astroFriendsDateFormatter forKey:kAstroSignDateFormatter];
	}
	return (astroFriendsDateFormatter);
}*/

/*+ (NSDate *) dateForCurrentLocale:(NSString *)dateMark
{
	// Date marks are specified in the gregorian calendar
	NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
	// Start of range
	NSArray *startDateTokens = [dateMark componentsSeparatedByString:@"-"];
	NSInteger rangeStartMonth = [[startDateTokens objectAtIndex:0] intValue];
	NSInteger rangeStartDay = [[startDateTokens objectAtIndex:1] intValue];
	
	
	NSDateComponents *rangeDateComponents = [[NSDateComponents alloc] init];
	[rangeDateComponents setMonth:rangeStartMonth];
	[rangeDateComponents setDay:rangeStartDay];
	
	NSDate *ret = [calendar dateFromComponents:rangeDateComponents];
	[rangeDateComponents release];
	[calendar release];
	
	return ret;
}*/

+ (NSString *)currentMonth
{
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit fromDate:[NSDate date]];
	NSInteger month = [components month];
	NSString *cadenaMes = [NSString stringWithFormat:@"%d",month];
	if (month<10) {
		cadenaMes = [@"0" stringByAppendingString:cadenaMes];
	}
	return cadenaMes;
}

+(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

#pragma mark - Color

+ (UIColor *)colorFromHexString:(NSString *)hexString
{
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:colorString];
    [scanner setScanLocation:0];
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:((rgbValue & 0xFF000000) >> 24)/255.0];
}

@end

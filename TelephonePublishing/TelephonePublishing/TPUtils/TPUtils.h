//
//  HMUtils.h
//  Elle
//
//  Created by Julio Rivas on 23/08/12.
//  Copyright (c) 2012 TAPTAP Networks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TPUtils : NSObject

//Retina
+ (BOOL) isRetinaDevice;
+ (BOOL) isRetina4Device;

//Labels
+ (CGFloat)calculateHeightOfLabel:(UILabel*)label;
+ (CGFloat) calculateHeightOfTextFromWidth:(NSString *)text :(UIFont*)withFont :(CGFloat)width;
+ (CGFloat) calculateHeightOfTextFromWidth:(NSString *)text :(UIFont*)withFont :(CGFloat)width :(NSLineBreakMode)lineBreakMode;
+ (CGFloat) calculateHeightOfLabelFromWidth:(UILabel *)label :(CGFloat)width;

//NavBar
+ (UINavigationController *)customizedNavigationController :(UIImage *)backgroundImage;
+ (UIButton *)backLeftButton:(id)target :(SEL)selector;
+ (UIBarButtonItem *)backLeftButtonBar:(id)target :(SEL)selector;
+ (UIButton *)backLeftButton:(id)target :(SEL)selector :(UIImage *)image :(UIColor *)color;
+ (UIButton *)menuRightButton:(id)target :(SEL)selector;
+ (UIBarButtonItem *)menuRightBarButton:(id)target :(SEL)selector;
+ (UIButton *)menuRightButton:(id)target :(SEL)selector :(UIImage *)image :(UIColor *)color;
+ (UIButton *)favoritesRightButton:(id)target :(SEL)selector :(UIImage *)image :(UIColor *)color;
+ (UIButton *)favoritesRightButton:(id)target :(SEL)selector;
+ (UIButton *)recognitionLeftButton:(id)target :(SEL)selector :(CGRect)frame;
+ (void)pushMenuInViewController:(UIViewController *)viewController;
+ (void)pushHomeInViewController:(UIViewController *)viewController;

//UI
+ (void)listAllFonts;
+ (UIView *)createTitleBar:(NSString *)title;
//+ (void)createNavBar:(UIViewController *)viewController;
//+ (UIView *)createNavBar;
+ (void)createNavBar:(UIViewController *)viewController :(SEL)selector;
+ (UIButton *)buttonCreate :(NSUInteger)x :(NSUInteger)y :(NSUInteger)width :(NSUInteger)height :(UIImage *)img :(UIColor *)textCol :(UIFont *)font :(NSString *)title;
+ (UILabel *)labelCreate :(NSUInteger)x :(NSUInteger)y :(NSUInteger)width :(NSUInteger)height :(UIFont *)font :(UIColor *)textCol :(NSUInteger)numLines;

//Formats
+(BOOL) NSStringIsValidEmail:(NSString *)checkString;

//String
+ (NSString *)flattenHTML:(NSString *)html;
+ (NSString *)escapeUrlString:(NSString *)url;

//Date
+ (NSDate *) dateForCurrentLocale:(NSString *)dateMark;
+ (NSString *)currentMonth;

//Validate connection
+ (BOOL)validateConnection;

//Color
+ (UIColor *)colorFromHexString:(NSString *)hexString;

@end

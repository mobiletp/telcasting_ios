//
//  ContactarViewController.m
//  TelephonePublishing
//
//  Created by Tecnico IOS on 29/4/15.
//  Copyright (c) 2015 Hearst Magazine. All rights reserved.
//

#import "ContactarViewController.h"

@interface ContactarViewController ()

@end

@implementation ContactarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpVista];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setUpVista
{
    self.view.backgroundColor = [UIColor whiteColor];
    UILabel *nombreyApellidos = [[UILabel alloc]initWithFrame:CGRectMake(10, 10, 300, 20)];
    [nombreyApellidos setText:@"NOMBRE Y APELLIDOS"];
    [nombreyApellidos setTextColor:[UIColor greenColor]];
    [self.view addSubview:nombreyApellidos];
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(10, 1 + nombreyApellidos.frame.origin.y + nombreyApellidos.frame.size.height, 300, 2)];
    lineView.backgroundColor = [UIColor greenColor];
    [self.view addSubview:lineView];
    
    UITextField *tNombreyApellidos = [[UITextField alloc]initWithFrame:CGRectMake(10, 10 + lineView.frame.origin.y + lineView.frame.size.height, 300, 25)];
    tNombreyApellidos.textAlignment = NSTextAlignmentCenter;
    tNombreyApellidos.borderStyle = UITextBorderStyleRoundedRect;
    tNombreyApellidos.layer.borderWidth = 1.0f;
    tNombreyApellidos.layer.borderColor = [[UIColor grayColor] CGColor];
    tNombreyApellidos.layer.cornerRadius = 5;
    [self.view addSubview:tNombreyApellidos];
    
    UILabel *email = [[UILabel alloc]initWithFrame:CGRectMake(10, 10 + tNombreyApellidos.frame.origin.y + tNombreyApellidos.frame.size.height, 300, 20)];
    [email setText:@"CORREO ELECTRONICO"];
    [email setTextColor:[UIColor greenColor]];
    [self.view addSubview:email];
    
    lineView = [[UIView alloc] initWithFrame:CGRectMake(10, 1 + email.frame.origin.y + email.frame.size.height, 300, 2)];
    lineView.backgroundColor = [UIColor greenColor];
    [self.view addSubview:lineView];
    
    UITextField *tEmail = [[UITextField alloc]initWithFrame:CGRectMake(10, 10 + lineView.frame.origin.y + lineView.frame.size.height, 300, 25)];
    tEmail.textAlignment = NSTextAlignmentCenter;
    tEmail.borderStyle = UITextBorderStyleRoundedRect;
    tEmail.layer.borderWidth = 1.0f;
    tEmail.layer.borderColor = [[UIColor grayColor] CGColor];
    tEmail.layer.cornerRadius = 5;
    [self.view addSubview:tEmail];
    
    UILabel *consulta = [[UILabel alloc]initWithFrame:CGRectMake(10, 10 + tEmail.frame.origin.y + tEmail.frame.size.height, 300, 20)];
    [consulta setText:@"CONSULTA"];
    [consulta setTextColor:[UIColor greenColor]];
    [self.view addSubview:consulta];
    
    lineView = [[UIView alloc] initWithFrame:CGRectMake(10, 1 + consulta.frame.origin.y + consulta.frame.size.height, 300, 2)];
    lineView.backgroundColor = [UIColor greenColor];
    [self.view addSubview:lineView];
    
    UITextField *tconsulta = [[UITextField alloc]initWithFrame:CGRectMake(10, 10 + lineView.frame.origin.y + lineView.frame.size.height, 300, 275)];
    tconsulta.textAlignment = NSTextAlignmentCenter;
    tconsulta.borderStyle = UITextBorderStyleRoundedRect;
    tconsulta.layer.borderWidth = 1.0f;
    tconsulta.layer.borderColor = [[UIColor grayColor] CGColor];
    tconsulta.layer.cornerRadius = 5;
    [self.view addSubview:tconsulta];
    
    UIButton *bConsulta = [[UIButton alloc] initWithFrame:CGRectMake(12, [[UIScreen mainScreen] bounds].size.height-28, 296, 25)];
    [bConsulta setBackgroundColor:[UIColor greenColor]];
    [bConsulta setTitle:@"Hacer Consulta" forState:UIControlStateNormal];
    [bConsulta.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [self.view addSubview:bConsulta];
}

@end

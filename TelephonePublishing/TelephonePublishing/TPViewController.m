//
//  TPViewController.m
//  TelephonePublishing
//
//  Created by Tpbeca tec on 17/09/14.
//  Copyright (c) 2014 Hearst Magazine. All rights reserved.
//

#import "TPViewController.h"
#import "TPToolbarDown.h"
#import "TPToolbarLlamar.h"
#import "TPConstants.h"
#import "TPTabbar.h"
#import "SWRevealViewController.h"
#import "MiPerfil.h"
#import "addAmigo.h"
#import "TPDatePicker.h"
#import "MiPerfilDatosHoroscopo.h"
#import "MisAmigos.h"


@interface TPViewController ()

@end

@implementation TPViewController

@synthesize tabbar = _tabbar;
@synthesize miPerfil = _miPerfil;
@synthesize misAmigos = _misAmigos;
@synthesize picker = _picker;
@synthesize imageView = _imageView;

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.mas = NO;
    perfil = NO;
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    
    //Toolbar Menu Superior
    /*_topToolbar = [[TPToolbar alloc] init];
    [_topToolbar setDelegate:self];
    [self.view addSubview:_topToolbar];*/
    
    //tabbar
    /*_sectionbar = [[TPSectionbar alloc] init];
    [_sectionbar setDelegate:self];
    [self.view addSubview:_sectionbar];*/
    
    //tabbar
    _tabbar = [[TPTabbar alloc] init];
    [_tabbar setDelegate:self];
    [self.view addSubview:_tabbar];
    
    //Posicion inicial de la barra de herramientas
    NSInteger y=0;
    
    //Posicion inicial de la tabbar
    y=0;
    _tabbar.frame = CGRectMake(_tabbar.frame.origin.x, y, _tabbar.frame.size.width, _tabbar.frame.size.height);
    
    //menu lateral
    SWRevealViewController *revealController = [self revealViewController];
    
    [revealController panGestureRecognizer];
    [revealController tapGestureRecognizer];

    //Vistas del tabbar MiPerfil, MisAmigos
    //MiPerfil
    _miPerfil = [[MiPerfil alloc] initWithPerfil:@"Nombre" fecha:@"Fecha de nacimiento"];
    _miPerfil.frame = CGRectMake(0, y+_tabbar.frame.size.height,self.view.frame.size.width,70);
    _miPerfil.backgroundColor = [UIColor brownColor];
    _miPerfil.delegate = self;
    [self.view addSubview:_miPerfil];
    
    //MiPerfilDatosHoroscopo
    NSInteger maxHeight = 80+TOOLBAR_HEIGHT;
    maxHeight += _tabbar.frame.size.height+_miPerfil.frame.size.height;
    y=_tabbar.frame.size.height+_miPerfil.frame.size.height;
    _miPerfilDatosHoroscopo = [[MiPerfilDatosHoroscopo alloc] init];
    _miPerfilDatosHoroscopo.frame = CGRectMake(0,y,self.view.frame.size.width,self.view.frame.size.height - maxHeight);
    _miPerfilDatosHoroscopo.backgroundColor = [UIColor grayColor];
    [self.view addSubview:_miPerfilDatosHoroscopo];
    
    //Mis Amigos
    _misAmigos = [[MisAmigos alloc] init];
    _misAmigos.frame = CGRectMake(0, y-_miPerfil.frame.size.height,self.view.frame.size.width,_miPerfil.frame.size.height + self.view.frame.size.height - maxHeight);
    _misAmigos.backgroundColor = [UIColor darkGrayColor];
    _misAmigos.hidden = YES;
    _misAmigos.delegate = self;
    [_misAmigos viewConfig];
    [self.view addSubview:_misAmigos];
    
    //Add Amigos
    _addAmigo = [[addAmigo alloc] init];
    _addAmigo.frame = CGRectMake(0, y-_miPerfil.frame.size.height-30,self.view.frame.size.width,_miPerfil.frame.size.height + 64 +self.view.frame.size.height - maxHeight);
    _addAmigo.backgroundColor = [UIColor yellowColor];
    _addAmigo.hidden = YES;
    _addAmigo.delegate = self;
    [_addAmigo viewConfig];
    [self.view addSubview:_addAmigo];
    
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [_miPerfilDatosHoroscopo refrescarDatos:_miPerfil.fecha];
    [super viewDidAppear:animated];
}

-(void)toolbarDelegateMenu:(TPToolbar *)topToolbar
{
    //llama al menu slide
    SWRevealViewController *revealController = [self revealViewController];
    [revealController revealToggleAnimated:true];
}

-(void)toolbarDelegateHelp:(TPToolbar *)topToolbar
{
    //TODO alert view ayuda
    UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"Ayuda"
                                                     message:@"Texto de prueba"
                                                    delegate:self
                                           cancelButtonTitle:@"Cancel"
                                           otherButtonTitles: nil];
    [alert setBackgroundColor: [UIColor blackColor]];
    [alert show];
}

-(void)tabbarDelegateMiPerfil:(TPTabbar *)tabbar
{
    _miPerfil.hidden = NO;
    _miPerfilDatosHoroscopo.hidden = NO;
    _misAmigos.hidden = YES;
}

-(void)tabbarDelegateMisAmigos:(TPTabbar *)tabbar
{
    _miPerfil.hidden = YES;
    _miPerfilDatosHoroscopo.hidden = YES;
    _misAmigos.hidden = NO;
}

-(void)customViewDidPerformAction:(MiPerfil*)miPerfil
{
    CGRect frame = CGRectMake(70, _miPerfil.frame.origin.y+10, 180, 150);
    _picker = [[TPDatePicker alloc] initWithFrame:frame];
    _picker.delegate = self;
    _picker.layer.cornerRadius = 6;
    dimView = [[UIView alloc]initWithFrame:self.view.frame];
    
    dimView.backgroundColor = [UIColor blackColor];
    dimView.alpha = 0;
    [self.view addSubview:dimView];
    [self.view bringSubviewToFront:dimView];
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         dimView.alpha = 0.2; //Fondo semitransparente que impide tocar lo de detras del aler view custom, con esto se le da mas o menos opacidad
                     }];
    [self.view addSubview:_picker];
    
}

-(void)alertViewDidPerformAction:(MiPerfil*)miPerfil
{
    
}

-(void)datePickerDidPerformAction:(TPDatePicker*)tpDatePicker
{
    [_picker removeFromSuperview];
    [UIView animateWithDuration:0.3
                     animations:^{
                         dimView.alpha = 0;
                     }];
    
    [dimView removeFromSuperview];
    dimView = nil;
    if (_miPerfil.fetchedRecordsArray.count > 0)
    {
        _miPerfil.fecha = tpDatePicker.date;
        [_miPerfil updateDatosPerfil];
    }
    else{
        [_miPerfil saveData:nil fecha:tpDatePicker.date tipo:@"perfil" foto:nil];
        _miPerfil.fecha = tpDatePicker.date;
    }
    [_miPerfilDatosHoroscopo refrescarDatos:_miPerfil.fecha];
}

-(void)addAmigoDidPerformAction:(MisAmigos*)misAmigos
{
    if (![misAmigos.nombreAmigo isEqualToString:@""])
    {
        _addAmigo.nombre.text = misAmigos.nombreAmigo;
    }
    _misAmigos.hidden = YES;
    _addAmigo.hidden = NO;
    _addAmigo.editMode = NO;
}
-(void)editAmigoDidPerformAction:(MisAmigos*)misAmigos
{
    if (![misAmigos.nombreAmigo isEqualToString:@""]){
        _addAmigo.nombre.text = misAmigos.nombreAmigo;
        if(misAmigos.fotoAmigo != nil){
            UIImage *temp = [[UIImage alloc] initWithData:misAmigos.fotoAmigo];
            [_addAmigo changeImage:temp];
        }
    }
    _misAmigos.hidden = YES;
    _addAmigo.hidden = NO;
    _addAmigo.editMode = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addAmigoFinalDidPerformAction:(addAmigo*)addAmigo
{
    _misAmigos.hidden = NO;
    _addAmigo.hidden = YES;
    [_misAmigos saveData:addAmigo.nombre.text fecha:addAmigo.fechaPicker.date tipo:@"amigos" foto:addAmigo.fotoData];
    NSLog(@"Debug: Amigo Añadido, Nombre %@, Fecha: %@ ",addAmigo.nombre.text, addAmigo.fechaPicker.date);
    [_addAmigo limpiarCampos];
}

-(void)editAmigoFinalDidPerformAction:(addAmigo*)addAmigo
{
    _misAmigos.hidden = NO;
    _addAmigo.hidden = YES;
    _misAmigos.nombreAmigo = addAmigo.nombre.text;
    _misAmigos.fechaAmigo = addAmigo.fechaPicker.date;
    _misAmigos.fotoAmigo = addAmigo.fotoData;
    NSLog(@"Debug: Amigo editado, Nombre %@, Fecha: %@ ",addAmigo.nombre.text, addAmigo.fechaPicker.date);
    [_misAmigos updatePerfil];
    [_addAmigo limpiarCampos];
}

-(IBAction)botonFoto:(id)sender
{
    perfil = NO;
    NSLog(@"Debug: Boton foto pulsado");
    UIImagePickerController * picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    
    picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;

    [self presentViewController:picker animated:YES completion:nil];
}

-(IBAction)botonFotoPerfil:(id)sender
{
    perfil = YES;
    NSLog(@"Debug: Boton foto perfil amigos pulsado");
    UIImagePickerController * picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    
    picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    
    [self presentViewController:picker animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    if(perfil){
        [picker dismissViewControllerAnimated:NO completion:nil];
        UIImage *temp = [[UIImage alloc] init];
        temp = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        [_addAmigo changeImage:temp];
        perfil = NO;
    }
    else
    {
        [picker dismissViewControllerAnimated:NO completion:nil];
        UIImage *temp = [[UIImage alloc] init];
        temp = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        [_miPerfil changeImage:temp];
    }
    
}
@end

//
//  UINavigationController+Rotation.m
//  Elle
//
//  Created by Julio Rivas on 04/10/12.
//
//

#import "UINavigationController+Rotation.h"

@implementation UINavigationController (iOS6_Rotation)

- (BOOL)shouldAutorotate
{
    return self.topViewController.shouldAutorotate; //you are asking your current controller what it should do
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}

@end
//
//  UINavigationController+Rotation.h
//  Elle
//
//  Created by Julio Rivas on 04/10/12.
//
//

#import <UIKit/UIKit.h>

@interface UINavigationController (iOS6_Rotation)

-(BOOL)shouldAutorotate;
-(NSUInteger)supportedInterfaceOrientations;

@end

//
//  UIImageExtras.h
//  iMagazine_iPhone
//
//  Created by Javier de Francisco Monte on 05/01/12.
//  Copyright (c) 2012 TAPTAPNetworks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIImage (Extras)

// Escala una imagen manteniendo la proporción ocupando el tamaño especificado. Parte de la imagen puede no verse
- (UIImage *)imageByScalingToFixedSize:(CGSize)targetSize;
//Escala una imagen de forma proporcional hasta un tamanno
- (UIImage *)imageByScalingProportionallyToSize:(CGSize)targetSize;
//Guarda la imagen en un fichero en formato JPEG
- (BOOL) saveAsJPEG:(NSString *)filepath quality:(CGFloat)quality;
//Guarda la imagen en un fichero en formato PNG
- (BOOL) saveAsPNG:(NSString *)filepath;
@end;


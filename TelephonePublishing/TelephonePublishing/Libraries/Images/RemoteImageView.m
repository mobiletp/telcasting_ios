//
//  RemoteImageView.m
//  iMagazine_iPhone
//
//  Created by Javier de Francisco Monte on 05/01/12.
//  Copyright (c) 2012 TAPTAPNetworks. All rights reserved.
//

#import "RemoteImageView.h"
#import "UIImageExtras.h"
#import <CommonCrypto/CommonDigest.h>
#import "TPConstants.h"

@interface RemoteImageView (Private)

- (NSString *) getImageName:(NSURL *)url;
- (NSData *) getCachedImage:(NSURL *)url;
- (void) setCachedImage:(NSData *)data url:(NSURL *)url; 

@end
	
	
@implementation RemoteImageView

@synthesize updating = updating_;
@synthesize delegate = _delegate;

- (id) init {
	self = [super init];
    
	if (self != nil){
		updating_ = NO;
        _fixedSize = NO;
        _fullSize = NO;
        _isFilter = NO;
        _delegate = nil;
		[self setBackgroundColor:[UIColor clearColor]];
	}
	return self;
}

#pragma mark -
#pragma mark Public methods

- (void)loadImageFromURL:(NSURL*)url loadingImage:(UIImage *)loadingImage errorImage:(NSString*)errorImage fixedSize:(BOOL)fixedSize {
    _errorImage = errorImage;
    [self loadImageFromURL:url loadingImage:loadingImage fixedSize:fixedSize];
}

- (void)loadImageFromURL:(NSURL*)url loadingImage:(UIImage *)loadingImage fixedSize:(BOOL)fixedSize isFiler:(BOOL)isFilter{
    _isFilter = isFilter;
    [self loadImageFromURL:url loadingImage:loadingImage fixedSize:fixedSize];
}

- (void)loadImageFromURL:(NSURL*)url loadingImage:(UIImage *)loadingImage fixedSize:(BOOL)fixedSize {
    _fixedSize = fixedSize;
    [self loadImageFromURL:url loadingImage:loadingImage];
}

- (void)loadImageFromURL:(NSURL*)url loadingImage:(UIImage *)loadingImage fullSize:(BOOL)fullSize{
    _fullSize = fullSize;
    [self loadImageFromURL:url loadingImage:loadingImage];    
}

- (void)loadImageFromURL:(NSURL*)url loadingImage:(UIImage *)loadingImage {
	//Borramos si existiese conexiones anteriroes
    [_connection release]; _connection=nil;
	[_data release]; _data=nil;

	[self setImage:loadingImage];
	
	//Si se ha especificado una URL
	if (url != nil){
		
		//Cargamos la imagen de la cache
		NSData *imageCache = [self getCachedImage:url];
		
		//Si no hay imagen en la cache, la pedimos
		if (imageCache == nil) {
            
            _activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
            _activityView.center = CGPointMake(self.frame.size.width/2, self.frame.size.height/2);
            [_activityView setHidesWhenStopped:YES];
            [_activityView setHidden:NO];
            [_activityView startAnimating];
            
            [self addSubview:_activityView];
            
			url_ = url;
			self.updating=YES;
			
			//Lanzamos la nueva conexion
			NSURLRequest* request = [NSURLRequest requestWithURL:url
													 cachePolicy:NSURLRequestUseProtocolCachePolicy
												 timeoutInterval:5.0];
			_connection = [[NSURLConnection alloc]
						  initWithRequest:request delegate:[self retain]];
		}
		//Si habia imagen, la cargamos
		else{
			self.updating = NO;
            
			UIImage *image = [UIImage imageWithData:imageCache];
            
            _originalImageSize = image.size;
            if([self isRetinaDevice])_originalImageSize = CGSizeMake(image.size.width/2, image.size.height/2);
            
			[self setImage:image];	
            
            [self setNeedsLayout];
                        
            //Delegate
            if (_delegate && [_delegate respondsToSelector:@selector(imageLoadedWithSize:)]) {
                    [_delegate imageLoadedWithSize:_originalImageSize];
            }
            
		}
	}
}


- (void)stopLoadingImage {
#ifndef NDEBUG
    //NSLog(@"[%@] %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
#endif
    if (_connection) {
        
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(loadThumbnail:) object:_connection];
        
        [_connection cancel];
        [_connection release];
        _connection = nil;
        
        [self release];
        
        [_activityView removeFromSuperview];
        [_activityView release];
        _activityView = nil;
    }
}

#pragma mark -
#pragma mark Private methods

#pragma mark - Check if the device has a retina display

- (BOOL) isRetinaDevice
{
    CGFloat scale = 1.0;
    UIScreen* screen = [UIScreen mainScreen];
    if ([UIScreen instancesRespondToSelector:@selector(scale)])
    {
        scale = [screen scale];
    }
    
    if (2.0 == scale)
    {
        //iPhone o iPod Touch con RETINA DISPLAY
        return YES;
    } else {
        //iPhone o iPod Touch sin RETINA DISPLAY
        return NO;
    }
}

//Convierte una url en un hash para almacenar en la cache						
- (NSString *) getImageName:(NSURL *)url{
//	return [NSString stringWithFormat:@"%U-%d-%d",[[url absoluteString] hash],self.frame.size.width,self.frame.size.height];
	
    return [NSString stringWithFormat:@"%@-%f-%f", [self md5:[url absoluteString]],self.frame.size.width,self.frame.size.height];
}

//Devuelve la imagen de la cache, o nil si no existe
- (NSData *) getCachedImage:(NSURL *)url{
	NSString *imageName = [self getImageName:url];

	NSString *filePath=[NSTemporaryDirectory() stringByAppendingString:imageName];
	
	//Si no hay imagen en cache, devolvemos nil
	if (![[NSFileManager defaultManager] fileExistsAtPath:filePath])
		return nil;
	
	return [[NSFileManager defaultManager] contentsAtPath:filePath];
}


//Actualiza la imagen en la cache
- (void) setCachedImage:(NSData *)imageData url:(NSURL *)url{
    
	NSString *imageName = [self getImageName:url];
	
	NSString *filePath=[NSTemporaryDirectory() stringByAppendingString:imageName];
	
	[[NSFileManager defaultManager] createFileAtPath:filePath contents:imageData attributes:nil];	
}

- (void)connection:(NSURLConnection *)theConnection didFailWithError:(NSError *)error {
#ifndef NDEBUG
    //NSLog(@"[%@] %@: %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), [error description]);
#endif
    [_connection release]; _connection=nil;
    
    [self release];
    
    [_activityView removeFromSuperview];
    [_activityView release];
    _activityView = nil;
   
    if (_errorImage) {
        self.image = [UIImage imageNamed:_errorImage];
        
        if (_delegate && [_delegate respondsToSelector:@selector(failedToLoadImageWithSize:)]) {
            [_delegate failedToLoadImageWithSize:CGSizeMake(self.image.size.width, self.image.size.height)];
        }
    }
}

- (void)connection:(NSURLConnection *)theConnection
	didReceiveData:(NSData *)incrementalData {
    
    if (_data==nil) {
		_data = [[NSMutableData alloc] initWithCapacity:2048];
    }
    [_data appendData:incrementalData];
}

- (void)connectionDidFinishLoading:(NSURLConnection*)theConnection {
#ifndef NDEBUG
    NSLog(@"[%@] %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
#endif
    [_activityView removeFromSuperview];
    [_activityView release];
    _activityView = nil;
    
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration: 0.10];
	[self setAlpha: 0.0]; 
	[UIView commitAnimations]; 

	[self performSelector:@selector(loadThumbnail:) withObject:theConnection afterDelay:0.10];
	
	self.updating=NO;	
}

- (void) loadThumbnail:(NSURLConnection*)theConnection {
    [_connection release]; _connection=nil;
	
	UIImage *image = [UIImage imageWithData:_data];
    
	//Si la imagen es mas grande que el frame
	if ((image.size.width>self.frame.size.width) || (image.size.height>self.frame.size.height)){
		//Reescalamos la imagen al tamaño del frame
        
        CGSize frameSize = self.frame.size;
        if([self isRetinaDevice])frameSize=CGSizeMake(2*self.frame.size.width, 2*self.frame.size.height);
        
        if (_isFilter){
            image = [self colorizeImage:image withColor:[UIColor colorWithRed: 0 green:0 blue:0 alpha:IMAGE_FILTER]];
        }
        
        if (_fixedSize) {
            //image = [image imageByScalingToFixedSize:frameSize];
            image = [self resizeImage:image newSize:frameSize];
        }
        else if(!_fullSize) {
            image = [image imageByScalingProportionallyToSize:frameSize];
        }        
        
		//Guardamos la imagen reescalada 
		[self setCachedImage:UIImagePNGRepresentation(image) url:url_];		//agarcia: Se hace en PNG para mantener las transparencias
	}
	else {
		//Guardamos la imagen original
		[self setCachedImage:_data url:url_];			
	}
    _originalImageSize = image.size;
    if([self isRetinaDevice])_originalImageSize = CGSizeMake(image.size.width/2, image.size.height/2);
    
    [_activityView removeFromSuperview];
    [_activityView release];
    _activityView = nil;
    
	//Establecemos la imagen dentro de la imageView
	[self setImage:image];
	
	//Pintamos de nuevo la vista
    [self setNeedsLayout];
	
    //Delegate
    if (_delegate && [_delegate respondsToSelector:@selector(imageLoadedWithSize:)]) {
        [_delegate imageLoadedWithSize:_originalImageSize];
    }
    
	//Liberamos los datos
    [_data release];
    _data=nil;
	
	//Creamos una animacion
	[self performSelector:@selector(showThumbnail) withObject:nil afterDelay: 0.0];	
}	


- (void) showThumbnail {
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration: 0.50];
	[self setAlpha: 1.0]; 
	[UIView commitAnimations];  
	[self release];
}


- (BOOL)existImage:(NSURL *)url
{
    NSString *imageName = [self getImageName:url];    
	NSString *filePath=[NSTemporaryDirectory() stringByAppendingString:imageName];
	
	//Si no hay imagen en cache, devolvemos nil
	if (![[NSFileManager defaultManager] fileExistsAtPath:filePath])
		return NO;
	
	return YES;
}

- (NSString *)getImagePath:(NSURL *)url
{
    NSString *imageName = [self getImageName:url];    
	NSString *filePath=[NSTemporaryDirectory() stringByAppendingString:imageName];

    return filePath;
}

- (CGSize)imageSize:(NSURL *)url
{
    CGSize size;
    
    if(![self existImage:url])
    {
        size = CGSizeMake(0, 0);
    }
    else
    {
        //Cargamos la imagen de la cache
		NSData *imageCache = [self getCachedImage:url];
		
        UIImage *image = [UIImage imageWithData:imageCache];
        
        size = image.size;
    }    
    
    return size;
}

- (UIImage *)colorizeImage:(UIImage *)image withColor:(UIColor *)color {
    UIGraphicsBeginImageContext(image.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGRect area = CGRectMake(0, 0, image.size.width, image.size.height);
    
    CGContextScaleCTM(context, 1, -1);
    CGContextTranslateCTM(context, 0, -area.size.height);
    
    CGContextSaveGState(context);
    CGContextClipToMask(context, area, image.CGImage);
    
    [color set];
    CGContextFillRect(context, area);
    
    CGContextRestoreGState(context);
    
    CGContextSetBlendMode(context, kCGBlendModeMultiply);
    
    CGContextDrawImage(context, area, image.CGImage);
    
    UIImage *colorizedImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return colorizedImage;
}

- (void)dealloc {
    [_connection cancel];
    [_connection release]; _connection=nil;
    [_data release]; _data=nil;
    [super dealloc];
}

- (UIImage *)resizeImage:(UIImage*)image newSize:(CGSize)newSize {
    CGRect newRect = CGRectIntegral(CGRectMake(0, 0, newSize.width, newSize.height));
    CGImageRef imageRef = image.CGImage;
    
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetInterpolationQuality(context, kCGInterpolationHigh);
    CGAffineTransform flipVertical = CGAffineTransformMake(1, 0, 0, -1, 0, newSize.height);
    
    CGContextConcatCTM(context, flipVertical);
    CGContextDrawImage(context, newRect, imageRef);
    
    CGImageRef newImageRef = CGBitmapContextCreateImage(context);
    UIImage *newImage = [UIImage imageWithCGImage:newImageRef];
    
    CGImageRelease(newImageRef);
    UIGraphicsEndImageContext();
    
    return newImage;
}

#pragma mark -
#pragma mark Auxiliary methods

- (NSString*)md5:(NSString*)input {
    
    const char *cStr = [input UTF8String];
    unsigned char digest[16];
    CC_MD5( cStr, strlen(cStr), digest ); // This is the md5 call
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    return  output;
    
}

@end

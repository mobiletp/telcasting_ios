//
//  RemoteImageView.h
//  iMagazine_iPhone
//
//  Created by Javier de Francisco Monte on 05/01/12.
//  Copyright (c) 2012 TAPTAPNetworks. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol RemoteImageViewDelegate <NSObject>

@optional
- (void)imageLoadedWithSize:(CGSize)size;
- (void)failedToLoadImageWithSize:(CGSize)size;

@end

@interface RemoteImageView : UIImageView {
	NSURLConnection* _connection;
    NSMutableData* _data;
	NSURL *url_;
    
    NSString *_errorImage;
    
    UIActivityIndicatorView *_activityView;
    
	BOOL updating_;
    BOOL _fixedSize;
    BOOL _fullSize;
    BOOL _isFilter;
    
    id<RemoteImageViewDelegate> _delegate;
    
    CGSize _originalImageSize;
}

@property BOOL updating;
@property (nonatomic, assign) id delegate;

- (void)loadImageFromURL:(NSURL*)url loadingImage:(UIImage *)loadingImage;
- (void)loadImageFromURL:(NSURL*)url loadingImage:(UIImage *)loadingImage fixedSize:(BOOL)fixedSize;
- (void)loadImageFromURL:(NSURL*)url loadingImage:(UIImage *)loadingImage fullSize:(BOOL)fullSize;
- (void)loadImageFromURL:(NSURL*)url loadingImage:(UIImage *)loadingImage fixedSize:(BOOL)fixedSize isFiler:(BOOL)isFilter;
- (void)loadImageFromURL:(NSURL*)url loadingImage:(UIImage *)loadingImage errorImage:(NSString*)errorImage fixedSize:(BOOL)fixedSize;

- (void)stopLoadingImage;

- (BOOL)existImage:(NSURL *)url;
- (NSString *)getImagePath:(NSURL *)url;
- (CGSize)imageSize:(NSURL *)url;



@end

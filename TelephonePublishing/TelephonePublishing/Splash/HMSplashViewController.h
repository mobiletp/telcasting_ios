//
//  ELSplashViewController.h
//  Elle
//
//  Created by Julio Rivas on 08/08/12.
//  Copyright (c) 2012 TAPTAPNetworks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HMDataManager.h"
#import <MediaPlayer/MediaPlayer.h>

@import GoogleMobileAds;

@class RemoteImageView;
@class Item;

@protocol HMSplashDelegate <NSObject>
- (void)exitSplash;
@end

@interface HMSplashViewController : UIViewController
{
    //Timer
    NSTimer *_timer;
    
    //Label
    UILabel *_loadingLabel;
    
    //ActIndicator
    UIActivityIndicatorView *_actIndicator;
    
    //Image
    UIImageView *_imageView;
    
    //Portada
    RemoteImageView *_remoteImage;
    
    //Data
    HMDataManager *_dataManager;
    
    //Item
    Item *_currentItem;
    
    //Viedo
    MPMoviePlayerController *_moviePlayer;
}

-(void)startTimer;
-(void)startAnimation:(NSString *)url;
-(void)loadAds;
-(void)endSplash;

@property (assign) id<HMSplashDelegate> delegate;

@end

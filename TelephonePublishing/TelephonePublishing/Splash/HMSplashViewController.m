//
//  ELSplashViewController.m
//  Elle
//
//  Created by Julio Rivas on 08/08/12.
//  Copyright (c) 2012 TAPTAPNetworks. All rights reserved.
//

#import "HMSplashViewController.h"
#import "TPUtils.h"
#import "TPConstants.h"
#import "HMGeneral.h"
#import "RemoteImageView.h"
#import "Item.h"
#import "Image.h"
#import "DFPManager.h"
//#import "InfiniaManager.h"

@interface HMSplashViewController ()

@end

@implementation HMSplashViewController

#pragma mark - Init

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        //Image
        _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, SPLASH_IMAGE_Y, [[UIScreen mainScreen] applicationFrame].size.width, [[UIScreen mainScreen] applicationFrame].size.height+SPLASH_IMAGE_BAR)];
        if(IS_IPHONE6PLUS)
            _imageView.image = [UIImage imageNamed:@"Default-568HD@2x.png"];
        else if(IS_IPHONE6)
            _imageView.image = [UIImage imageNamed:@"Default-667h@2x.png"];
        else if(IS_IPHONE5)
            _imageView.image = [UIImage imageNamed:@"Default-568h@2x.png"];
        else
            _imageView.image = [UIImage imageNamed:@"Default@2x.png"];
        [self.view addSubview:_imageView];
        
        //activity indicator
		_actIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(SPLASH_ACTIVITY_X, [[UIScreen mainScreen] applicationFrame].size.height-SPLASH_ACTIVITY_Y, SPLASH_ACTIVITY_SIZE, SPLASH_ACTIVITY_SIZE)];
		_actIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;	
		[self.view addSubview:_actIndicator];
		[_actIndicator startAnimating];
        
        //Label
        _loadingLabel = [TPUtils labelCreate:SPLASH_LOADING_X :[[UIScreen mainScreen] applicationFrame].size.height-SPLASH_ACTIVITY_Y :SPLASH_LOADING_WIDTH :SPLASH_LOADING_HEIGHT :[UIFont systemFontOfSize:14] :[UIColor whiteColor] :1];
        _loadingLabel.text = NSLocalizedString(@"_SPLASH_LOADING", @"");
        [self.view addSubview:_loadingLabel];
        
        //Remote
        _remoteImage = [[RemoteImageView alloc] initWithFrame:CGRectZero];
        [self.view addSubview:_remoteImage];
    }
    return self;
}

-(void)startTimer
{
    //inicializa el timer (tenemos en cuenta que solo se genera una vez)
    if(!_timer)_timer = [NSTimer scheduledTimerWithTimeInterval:(SPLASH_TIMER) target:self selector:@selector(endSplash) userInfo:nil repeats:NO];
}

-(void)startAnimation:(NSString *)url
{
    DLog();
    
    //Load image
    _dataManager = [[HMDataManager alloc] initHMDataManager];
    [_dataManager setDelegate:self];
    [_dataManager loadData:url];    
}

- (void)endSplash
{
    DLog();
    
    [_timer invalidate];
    _timer=nil;
    
    if(_delegate && [_delegate respondsToSelector:@selector(exitSplash)])
    {
        [_delegate exitSplash];
    }
}

-(void)playVideo
{
    DLog();
    
    //Status
    if(IS_OS_7_OR_LATER)
    {
        [self setNeedsStatusBarAppearanceUpdate];
    }
    
//    if([HMUtils validateConnection])
//    {

        NSString *urlStr = [[NSBundle mainBundle] pathForResource:@"animation" ofType:@"m4v"];
        NSURL *urlVideo = [NSURL fileURLWithPath:urlStr];
        _moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:urlVideo];
        
        NSArray * subviews = [_moviePlayer.view subviews];
        for (UIView * v in subviews) {
            if ([v respondsToSelector:@selector(setBackgroundColor:)]) {
                [v setBackgroundColor:[UIColor whiteColor]];
            }
        }
        
        _moviePlayer.fullscreen=YES;
        _moviePlayer.controlStyle = MPMovieControlStyleNone;
        _moviePlayer.view.frame = CGRectMake(0, 0, [[UIScreen mainScreen] applicationFrame].size.width, self.view.frame.size.height);//self.view.frame;
        _moviePlayer.view.userInteractionEnabled = FALSE;
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(finishVideo:)
                                                     name:MPMoviePlayerPlaybackDidFinishNotification
                                                   object:_moviePlayer];
        [self.view addSubview:_moviePlayer.view];
        [_moviePlayer play];

}

-(void)finishVideo:(NSNotification *)notificacion
{
    DLog();
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
													name:MPMoviePlayerPlaybackDidFinishNotification
												  object:notificacion];
    
    
    //End Video
    [self endVideo];
}

-(void)endVideo
{
    //Portada
    if(_currentItem)
    {
        [_remoteImage stopLoadingImage];
        //TODO ajustar al centro en cord Y
        if ([[UIScreen mainScreen] bounds].size.width == 375)
            [_remoteImage setFrame:CGRectMake(20, self.view.frame.size.height/2-430/2, 335, 430)]; //640x848
        else if ([[UIScreen mainScreen] bounds].size.width == 414)
            [_remoteImage setFrame:CGRectMake(20, self.view.frame.size.height/2-460/2, 374, 460)]; //640x848
        else
        [_remoteImage setFrame:CGRectMake(20, self.view.frame.size.height/2-371/2, 280, 371)]; //640x848
        NSString *url = [(Image *)[_currentItem.images objectAtIndex:0] url];
        [_remoteImage loadImageFromURL:[NSURL URLWithString:url] loadingImage:nil fixedSize:YES];
        [_remoteImage setContentMode:UIViewContentModeScaleAspectFill];
        [_remoteImage setClipsToBounds:YES];
        [self.view bringSubviewToFront:_remoteImage];
        
        UIView *iv = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        [iv setBackgroundColor:[UIColor whiteColor]];
        [self.view insertSubview:iv belowSubview:_remoteImage];
        
        [self performSelector:@selector(loadAds) withObject:nil afterDelay:2.0];
    }
    else
    {
        [self endSplash];
    }
}

#pragma mark - Life cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //Status
    if(IS_OS_7_OR_LATER)
    {
        [self setNeedsStatusBarAppearanceUpdate];
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(void)loadAds
{
    /*if([[HMGeneral sharedInstance] infiniaToken])
        //[[InfiniaManager sharedInstance] getCampaigns];
    [self performSelector:@selector(showIntersitial) withObject:nil afterDelay:2.0];*/
}

-(void)showIntersitial
{
    [[DFPManager sharedInstance] requestInter:@"appstar" controller:self];
    NSString *typeDFPString = @"[Banner intersitial]";
    if(VERVOSE == 2) NSLog(@"LOG TEST: Publicidad banner Type: %@ Section: %@",typeDFPString, @"appstar");
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    if(_currentItem)
        return UIStatusBarStyleDefault;
        
    return UIStatusBarStyleLightContent;
}

#pragma mark - DataManager

- (void)dataManagerDidFinish:(HMDataManager *)dataManager
{
    DLog();
    
    [self playVideo];
}

- (void)dataManagerDidParse:(HMDataManager *)dataManager withObject:(NSObject *)object
{
    DLog(@"%@",object);
    
    if([object isKindOfClass:[Item class]])
    {
        _currentItem = (Item *)object;
    }
}

- (void)dataManagerDidFail:(HMDataManager *)dataManager withError:(NSError *)error
{
    DLog(@"error: %@",[error description]);
    
    [self endSplash];
}


#pragma mark - Rotation

//Pre iOS 6
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

//iOS 6
-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

//iOS 6
- (BOOL)shouldAutorotate
{
    return NO;
}

@end

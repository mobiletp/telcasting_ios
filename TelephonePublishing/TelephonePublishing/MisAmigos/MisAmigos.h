//
//  MisAmigos.h
//  TelephonePublishing
//
//  Created by Tecnico IOS on 03/11/14.
//  Copyright (c) 2014 Hearst Magazine. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MisAmigos;

@protocol MisAmigosDelegate <NSObject>
-(void)addAmigoDidPerformAction:(MisAmigos*)misAmigos;
-(void)editAmigoDidPerformAction:(MisAmigos*)misAmigos;
@end

@interface MisAmigos : UIView<UITableViewDelegate, UITableViewDataSource>{
    UIButton *_add;
    NSString *nombreAmigo;
    NSDate *fechaAmigo;
    NSData *fotoAmigo;
    NSInteger index;
    __weak id<MisAmigosDelegate> _delegate;
}

@property (nonatomic, strong) UITableView* tableView;

@property (nonatomic, retain, readonly) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, retain, readonly) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (nonatomic, retain) UIButton *add;
@property (nonatomic,strong)NSMutableArray*fetchedRecordsArray;

@property (retain, nonatomic) NSString* nombreAmigo;
@property (retain, nonatomic) NSDate* fechaAmigo;
@property (retain, nonatomic) NSData* fotoAmigo;
@property (nonatomic) NSInteger index;

@property (nonatomic,weak) id<MisAmigosDelegate> delegate;

- (void)viewConfig;
- (void)changeButton:(BOOL)subir;
- (void)updatePerfil;
- (void)saveData:(NSString *)nombre fecha:(NSDate *)fecha tipo:(NSString *)tipo foto:(NSData *)foto;

@end

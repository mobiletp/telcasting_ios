//
//  MisAmigosCustomCell.h
//  TelephonePublishing
//
//  Created by Tecnico IOS on 03/11/14.
//  Copyright (c) 2014 Hearst Magazine. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MisAmigosCustomCell : UITableViewCell

@property (nonatomic, strong) UILabel *nombre;
@property (nonatomic, strong) UIButton *foto;
@property (nonatomic, strong) UIButton *horoscopo;
@property (nonatomic, strong) UIButton *numerologia;
@property (nonatomic, strong) UIButton *compatibilidad;


@end

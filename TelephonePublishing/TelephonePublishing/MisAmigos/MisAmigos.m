//
//  MisAmigos.m
//  TelephonePublishing
//
//  Created by Tecnico IOS on 03/11/14.
//  Copyright (c) 2014 Hearst Magazine. All rights reserved.
//

#import "MisAmigos.h"
#import "Perfiles.h"
#import "MisAmigosCustomCell.h"
#import <CoreData/CoreData.h>

@implementation MisAmigos

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
@synthesize add = _add;
@synthesize index = _index;

- (void)viewConfig
{
    _add = [UIButton buttonWithType:UIButtonTypeCustom];
    [_add addTarget:self
               action:@selector(addAmigoButton)
   forControlEvents:UIControlEventTouchUpInside];
    [_add setTitle:@"añadir Amigo" forState:UIControlStateNormal];
    _add.backgroundColor = [UIColor greenColor];
    _add.frame = CGRectMake(10, self.frame.size.height-40, self.frame.size.width-27, 30.0);
    [self addSubview:_add];
    self.fetchedRecordsArray = [self getPerfilAmigos];
    //Punto de creacion de la tabla
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0,0,self.frame.size.width,self.frame.size.height-50)];
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self addSubview:_tableView];
}
- (void)changeButton:(BOOL)subir
{
    CGRect frame = _add.frame;
    if (subir){
        _add.frame = CGRectMake(frame.origin.x,frame.origin.y-34,frame.size.width,frame.size.height);
    }else{
        _add.frame = CGRectMake(frame.origin.x,frame.origin.y+34,frame.size.width,frame.size.height);
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSLog(@"Numero de Perfiles de amigos: %lu", (unsigned long)_fetchedRecordsArray.count);
    return _fetchedRecordsArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *CellIdentifier = @"cell";
    MisAmigosCustomCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[MisAmigosCustomCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    Perfiles *perfil = _fetchedRecordsArray[indexPath.row];
    UIImage *image;
    if(perfil.foto != nil){
        NSLog(@"Cargando Foto perfil amigo");
        image = [UIImage imageWithData:perfil.foto];
    }
    else
    {
        image = [UIImage imageNamed:@"icono-amigos.png"];
    }
    [cell.foto setImage:image forState:UIControlStateNormal];
    image = [self setHoroscopo:perfil.fecha];
    [cell.horoscopo setImage:image forState:UIControlStateNormal];
    image = [self setNumerologia:perfil.fecha];
    [cell.numerologia setImage:image forState:UIControlStateNormal];
    image = [UIImage imageNamed:@"boton-carta-astral.png"];
    [cell.compatibilidad setImage:image forState:UIControlStateNormal];
    cell.nombre.text = perfil.nombre;
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}

/*- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

}*/

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{

}

- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewRowAction *moreAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Editar" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
        // maybe show an action sheet with more options
        [self.tableView setEditing:NO];
        Perfiles *perfil = _fetchedRecordsArray[indexPath.row];
        self.nombreAmigo = perfil.nombre;
        self.fechaAmigo = perfil.fecha;
        self.fotoAmigo = perfil.foto;
        self.index = indexPath.row;
        [self editAmigo];
        NSLog(@"Editar");
    }];
    moreAction.backgroundColor = [UIColor lightGrayColor];
    UITableViewRowAction *blurAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Blur" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
        [self.tableView setEditing:NO];
    }];
    blurAction.backgroundEffect = [UIVibrancyEffect effectForBlurEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight]];
    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:@"Borrar" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
        //[self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        // Delete the row from the data source
        Perfiles *perfil = (self.fetchedRecordsArray)[indexPath.row];
        [self.fetchedRecordsArray removeObjectAtIndex:indexPath.row];
        // You might want to delete the object from your Data Store if you’re using CoreData
        [_managedObjectContext deleteObject:perfil];
        NSError *error;
        [_managedObjectContext save:&error];
        // Animate the deletion
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        // Additional code to configure the Edit Button, if any
        if (self.fetchedRecordsArray.count == 0) {
            self.tableView.editing = NO;
            //self.editButton.enabled = NO;
            //self.editButton.titleLabel.text = @"Edit";
        }
    }];
    return @[deleteAction, moreAction];
}

- (void)saveData:(NSString *)nombre fecha:(NSDate *)fecha tipo:(NSString *)tipo foto:(NSData *)foto{
    
    Perfiles * newEntry = [NSEntityDescription insertNewObjectForEntityForName:@"Perfiles"
                                                        inManagedObjectContext:self.managedObjectContext];
    if(nombre != nil){
        [newEntry setValue:nombre forKey:@"nombre"];
    }
    if(fecha != nil){
        [newEntry setValue:fecha forKey:@"fecha"];
    }
    if(tipo != nil){
        [newEntry setValue:tipo forKey:@"tipo"];
    }
    if(foto !=nil){
        [newEntry setValue:foto forKey:@"foto"];
    }
    
    NSError *error;
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Imposible grabar: %@", [error localizedDescription]);
    }
    [self reloadData];
}

-(void)updatePerfil{
    NSLog(@"Actualizando Perfil %ld", (long)index);
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Perfiles" inManagedObjectContext:self.managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"tipo == 'amigos'"];
    
    [request setEntity:entityDescription];
    
    [request setPredicate:predicate];
    
    NSError *error;
    NSArray *array = [_managedObjectContext executeFetchRequest:request error:&error];
    Perfiles *perfil = array[_index];
    perfil.nombre = _nombreAmigo;
    perfil.fecha = _fechaAmigo;
    perfil.foto = _fotoAmigo;
    
    [self.managedObjectContext save:&error];
    
    [_tableView reloadData];
}

- (NSManagedObjectContext *) managedObjectContext {
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator: coordinator];
    }
    return _managedObjectContext;
}

- (NSManagedObjectModel *)managedObjectModel {
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    _managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
    
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    NSURL *storeUrl = [NSURL fileURLWithPath: [[self applicationDocumentsDirectory]
                                               stringByAppendingPathComponent: @"Perfiles.sqlite"]];
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc]
                                   initWithManagedObjectModel:[self managedObjectModel]];
    if(![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                  configuration:nil URL:storeUrl options:nil error:&error]) {
        /*Error for store creation should be handled in here*/
    }
    
    return _persistentStoreCoordinator;
}

- (NSString *)applicationDocumentsDirectory {
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}

-(void)reloadData{
    self.fetchedRecordsArray = [self getPerfilAmigos];
    [_tableView reloadData];
}

-(NSMutableArray*)getPerfilAmigos
{
    // initializing NSFetchRequest
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    //Setting Entity to be Queried
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Perfiles"
                                              inManagedObjectContext:self.managedObjectContext];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"tipo == 'amigos'"];
    
    [fetchRequest setEntity:entity];
    [fetchRequest setPredicate:predicate];
    NSError* error;
    
    // Query on managedObjectContext With Generated fetchRequest
    NSArray *fetchedRecords = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    NSLog(@"Numero de perfiles: %lu", (unsigned long)fetchedRecords.count);
    // Returning Fetched Records
    NSMutableArray *array = [[NSMutableArray alloc] initWithArray:fetchedRecords];
    return array;
}

- (UIImage *)setHoroscopo:(NSDate *)fecha{
    //Configuracion de dateformats para calculo de horoscopo y numero de la suerte
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"d"];
    NSInteger dia = [dateFormat stringFromDate:fecha].integerValue;
    [dateFormat setDateFormat:@"M"];
    NSInteger mes = [dateFormat stringFromDate:fecha].integerValue;
    
    UIImage *fotoHoroscopo;
    
    //Codigo deteccion horoscopo
    switch (mes) {
            //Enero
        case 1:
            if (dia >=21){
                NSLog(@"Acuario");
                fotoHoroscopo = [UIImage imageNamed:@"acuario.png"];
            }else{
                NSLog(@"Capricornio");
                fotoHoroscopo = [UIImage imageNamed:@"Capricornio.png"];
            }
            break;
            //Febrero
        case 2:
            if (dia >=20){
                NSLog(@"Piscis");
                fotoHoroscopo = [UIImage imageNamed:@"piscis.png"];
            }else{
                NSLog(@"Acuario");
                fotoHoroscopo = [UIImage imageNamed:@"acuario.png"];
            }
            break;
            //Marzo
        case 3:
            if (dia >=21){
                NSLog(@"Aries");
                fotoHoroscopo = [UIImage imageNamed:@"aries.png"];
            }else{
                NSLog(@"Piscis");
                fotoHoroscopo = [UIImage imageNamed:@"piscis.png"];
            }
            break;
            //Abril
        case 4:
            if (dia >=22){
                NSLog(@"Tauro");
                fotoHoroscopo = [UIImage imageNamed:@"tauro.png"];
            }else{
                NSLog(@"Aries");
                fotoHoroscopo = [UIImage imageNamed:@"aries.png"];
            }
            break;
            //Mayo
        case 5:
            if (dia >=22){
                NSLog(@"Geminis");
                fotoHoroscopo = [UIImage imageNamed:@"geminis.png"];
            }else{
                NSLog(@"Tauro");
                fotoHoroscopo = [UIImage imageNamed:@"tauro.png"];
            }
            break;
            //Junio
        case 6:
            if (dia >=22){
                NSLog(@"Cancer");
                fotoHoroscopo = [UIImage imageNamed:@"cancer.png"];
            }else{
                NSLog(@"Geminis");
                fotoHoroscopo = [UIImage imageNamed:@"geminis.png"];
            }
            break;
            //Julio
        case 7:
            if (dia >=23){
                NSLog(@"Leo");
                fotoHoroscopo = [UIImage imageNamed:@"leo.png"];
            }else{
                NSLog(@"Cancer");
                fotoHoroscopo = [UIImage imageNamed:@"cancer.png"];
            }
            break;
            //Agosto
        case 8:
            if (dia >=23){
                NSLog(@"Virgo");
                fotoHoroscopo = [UIImage imageNamed:@"virgo.png"];
            }else{
                NSLog(@"Leo");
                fotoHoroscopo = [UIImage imageNamed:@"leo.png"];
            }
            break;
            //Septiembre
        case 9:
            if (dia >=23){
                NSLog(@"Libra");
                fotoHoroscopo = [UIImage imageNamed:@"libra.png"];
            }else{
                NSLog(@"Virgo");
                fotoHoroscopo = [UIImage imageNamed:@"virgo.png"];
            }
            break;
            //Octubre
        case 10:
            if (dia >=23){
                NSLog(@"Escorpio");
                fotoHoroscopo = [UIImage imageNamed:@"escorpio.png"];
            }else{
                NSLog(@"Libra");
                fotoHoroscopo = [UIImage imageNamed:@"libra.png"];
            }
            break;
            //Noviembre
        case 11:
            if (dia >=23){
                NSLog(@"Sagtario");
                fotoHoroscopo = [UIImage imageNamed:@"sagitario.png"];
            }else{
                NSLog(@"Escorpio");
                fotoHoroscopo = [UIImage imageNamed:@"escorpio.png"];
            }
            break;
            //Diciembre
        case 12:
            if (dia >=22){
                NSLog(@"Capricornio");
                fotoHoroscopo = [UIImage imageNamed:@"capricornio.png"];
            }else{
                NSLog(@"Sagtario");
                fotoHoroscopo = [UIImage imageNamed:@"sagitario.png"];
            }
            break;
    }
    return fotoHoroscopo;
}

- (UIImage *)setNumerologia:(NSDate *)fecha{
    //Configuracion de dateformats para calculo de horoscopo y numero de la suerte
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"d"];
    NSInteger dia = [dateFormat stringFromDate:fecha].integerValue;
    [dateFormat setDateFormat:@"M"];
    NSInteger mes = [dateFormat stringFromDate:fecha].integerValue;
    [dateFormat setDateFormat:@"YYYY"];
    NSInteger anyo = [dateFormat stringFromDate:fecha].integerValue;
    
    UIImage *fotoNumerologia;
    
    //Codigo deteccion numero de la suerte
    
    NSString *num = [NSString stringWithFormat:@"%ld %ld %ld",(long)dia ,(long)mes, (long)anyo];
    
    while (num.length > 1){
        NSInteger total = 0;
        for (int i = 0; i < num.length; i++) {
            total += [num substringWithRange:NSMakeRange(i, 1)].integerValue;
        }
        num = [NSString stringWithFormat: @"%ld", (long)total];
    }
    
    NSLog(@"Numero de la suerte: %@", num);
    
    switch (num.integerValue) {
        case 1:
            fotoNumerologia = [UIImage imageNamed:@"1.png"];
            break;
        case 2:
            fotoNumerologia = [UIImage imageNamed:@"2.png"];
            break;
        case 3:
            fotoNumerologia = [UIImage imageNamed:@"3.png"];
            break;
        case 4:
            fotoNumerologia = [UIImage imageNamed:@"4.png"];
            break;
        case 5:
            fotoNumerologia = [UIImage imageNamed:@"5.png"];
            break;
        case 6:
            fotoNumerologia = [UIImage imageNamed:@"6.png"];
            break;
        case 7:
            fotoNumerologia = [UIImage imageNamed:@"7.png"];
            break;
        case 8:
            fotoNumerologia = [UIImage imageNamed:@"8.png"];
            break;
        case 9:
            fotoNumerologia = [UIImage imageNamed:@"9.png"];
            break;
    }
    return fotoNumerologia;
}

- (void)editAmigo{
    if (self.delegate && [self.delegate respondsToSelector:@selector(addAmigoDidPerformAction:)]) {
        [self.delegate editAmigoDidPerformAction:self];
    }
}

- (void)addAmigoButton{
    if (self.delegate && [self.delegate respondsToSelector:@selector(addAmigoDidPerformAction:)]) {
        self.nombreAmigo = @"";
        self.fechaAmigo = nil;
        [self.delegate addAmigoDidPerformAction:self];
    }
}
@end

//
//  addAmigo.h
//  TelephonePublishing
//
//  Created by Tecnico IOS on 04/11/14.
//  Copyright (c) 2014 Hearst Magazine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPToolbar.h"
#import "TPSectionbar.h"

@class addAmigo;

@protocol addAmigoDelegate <NSObject>
-(void)addAmigoFinalDidPerformAction:(addAmigo*)addAmigo;
-(void)editAmigoFinalDidPerformAction:(addAmigo*)addAmigo;
-(IBAction)botonFotoPerfil:(id)sender;
@end

@interface addAmigo : UIView{
    TPToolbar *_topToolbar;
    UILabel *_fotoyNombre;
    UILabel *_fechaNacimiento;
    UITextField *_nombre;
    UIDatePicker *_fechaPicker;
    UIImage *_foto;
    UIImageView *_fotoView;
    NSData *_fotoData;
    BOOL editMode;
    __weak id<addAmigoDelegate> _delegate;
}
@property (nonatomic, strong) UILabel* fotoyNombre;
@property (nonatomic, strong) UILabel* fechaNacimiento;
@property (nonatomic, strong) UITextField* nombre;
@property (nonatomic, strong) NSData* fotoData;
@property (nonatomic, strong) UIDatePicker* fechaPicker;
@property (nonatomic) BOOL editMode;

@property (nonatomic,weak) id<addAmigoDelegate> delegate;

-(void)viewConfig;
-(void)botonAdd;
-(void)limpiarCampos;

-(void)changeImage:(UIImage*)newImage;

@end

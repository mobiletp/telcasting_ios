//
//  MisAmigosCustomCell.m
//  TelephonePublishing
//
//  Created by Tecnico IOS on 03/11/14.
//  Copyright (c) 2014 Hearst Magazine. All rights reserved.
//

#import "MisAmigosCustomCell.h"

@implementation MisAmigosCustomCell

@synthesize foto = _foto;
@synthesize horoscopo = _horoscopo;
@synthesize numerologia = _numerologia;
@synthesize compatibilidad = _compatibilidad;

-(UIButton *)foto
{
    if (!_foto)
        _foto = [UIButton buttonWithType:UIButtonTypeCustom];
    return _foto;
}
-(UIButton *)horoscopo
{
    if (!_horoscopo)
        _horoscopo = [UIButton buttonWithType:UIButtonTypeCustom];
    return _horoscopo;
}
-(UIButton *)numerologia
{
    if (!_numerologia)
        _numerologia = [UIButton buttonWithType:UIButtonTypeCustom];
    return _numerologia;
}
-(UIButton *)compatibilidad
{
    if (!_compatibilidad)
        _compatibilidad = [UIButton buttonWithType:UIButtonTypeCustom];
    return _compatibilidad;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        //set space from the borders
        float inset = 5.0;
        //how much space do we have to work with?
        CGRect bounds = [[self contentView] bounds];
        //get height and width of the bounds
        CGFloat h = bounds.size.height;
        CGFloat w = bounds.size.width;
        //calculate where the buttons should be centered
        CGFloat centerHeight = h;
        CGFloat centerWidth = (w/6)/2;
        //add buttons as subviews
        [[self contentView] addSubview:self.foto];
        [[self contentView] addSubview:self.horoscopo];
        [[self contentView] addSubview:self.numerologia];
        [[self contentView] addSubview:self.compatibilidad];
        //set frame and center for the first button
        CGRect imageFrame = CGRectMake(10, inset, 50.0, 50.0);
        [self.foto setFrame:imageFrame];
        CGPoint centerPoint = CGPointMake(centerWidth+5, centerHeight);
        [self.foto setCenter:centerPoint];
        //set frame and center for the second button
        imageFrame = CGRectMake(41.0, inset, 41.0, 41.0);
        [self.horoscopo setFrame:imageFrame];
        centerPoint = CGPointMake(centerWidth + w/4, centerHeight-4);
        [self.horoscopo setCenter:centerPoint];
        //set frame and center for the third button
        imageFrame = CGRectMake(82.0, inset, 41.0, 41.0);
        [self.numerologia setFrame:imageFrame];
        centerPoint = CGPointMake(centerWidth + 2*(w/4), centerHeight-4);
        [self.numerologia setCenter:centerPoint];
        //set frame and center for the third button
        imageFrame = CGRectMake(123.0, inset, 41.0, 41.0);
        [self.compatibilidad setFrame:imageFrame];
        centerPoint = CGPointMake(centerWidth + 3*(w/4), centerHeight-4);
        [self.compatibilidad setCenter:centerPoint];
        
        
        // configure control(s)
        self.nombre = [[UILabel alloc] initWithFrame:CGRectMake(10, -5, 300, 30)];
        self.nombre.textColor = [UIColor blackColor];
        self.nombre.font = [UIFont fontWithName:@"Arial" size:12.0f];
        
        [self addSubview:self.nombre];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

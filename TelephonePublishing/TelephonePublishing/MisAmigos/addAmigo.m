//
//  addAmigo.m
//  TelephonePublishing
//
//  Created by Tecnico IOS on 04/11/14.
//  Copyright (c) 2014 Hearst Magazine. All rights reserved.
//

#import "addAmigo.h"

@implementation addAmigo

@synthesize nombre = _nombre;
@synthesize fotoyNombre = _fotoyNombre;
@synthesize fotoData = _fotoData;
@synthesize fechaNacimiento = _fechaNacimiento;
@synthesize fechaPicker = _fechaPicker;
@synthesize editMode = _editMode;


-(instancetype)initWithPerfil:(NSString*)nombre fecha:(NSString*)fecha
{
    self = [super init];
    if (self)
    {
        //[self viewConfig];
    }
    return self;
}

- (void)viewConfig{
    //Label Foto y Nombre
    CGRect labelFrame = CGRectMake(10.0f, 10.0f, 200.0f, 25.0f);
    _fotoyNombre = [[UILabel alloc] initWithFrame:labelFrame];
    _fotoyNombre.text = @"Foto y Nombre";
    [self addSubview:_fotoyNombre];
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(10, 35, self.frame.size.width-25, 1)];
    UIColor *color = [UIColor greenColor];;
    lineView.backgroundColor = color;
    [self addSubview:lineView];
    
    //Label Fecha de nacimiento
    labelFrame = CGRectMake(10.0f, 100.0f, 200.0f, 25.0f);
    _fotoyNombre = [[UILabel alloc] initWithFrame:labelFrame];
    _fotoyNombre.text = @"Fecha de nacimiento";
    [self addSubview:_fotoyNombre];
    
    lineView = [[UIView alloc] initWithFrame:CGRectMake(10, 125, self.frame.size.width-25, 1)];
    color = [UIColor greenColor];;
    lineView.backgroundColor = color;
    [self addSubview:lineView];
    
    //Cuadro foto
    _foto = [UIImage imageNamed:@"icono-amigos.png"];
    _fotoView = [[UIImageView alloc] initWithImage:_foto];
    //Punto de creacion de la imagen y tamaño
    _fotoView.frame = CGRectMake(12, 40, 50, 50);
    //Marco para la foto
    CALayer *borderLayer = [CALayer layer];
    CGRect borderFrame = CGRectMake(0, 0, (_fotoView.frame.size.width), (_fotoView.frame.size.height));
    [borderLayer setBackgroundColor:[[UIColor clearColor] CGColor]];
    [borderLayer setFrame:borderFrame];
    [borderLayer setCornerRadius:0];
    [borderLayer setBorderWidth:2];
    [borderLayer setBorderColor:[[UIColor grayColor] CGColor]];
    [_fotoView.layer addSublayer:borderLayer];
    //Creacion de boton en la imageview
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setBackgroundColor:[UIColor clearColor]];
    [button addTarget:self action:@selector(botonFotoPerfil:)
     forControlEvents:UIControlEventTouchUpInside];
    //[button setBackgroundColor: forState:UIControlStateSelected];
    button.frame = CGRectMake(0.0, 0.0, 50.0, 50.0);
    _fotoView.userInteractionEnabled = YES;
    [_fotoView addSubview:button];
    [self addSubview:_fotoView];
    
    CGRect frame = CGRectMake(70, 65, self.frame.size.width - 85, 25);
    _nombre = [[UITextField alloc] initWithFrame:frame];
    _nombre.borderStyle = UITextBorderStyleRoundedRect;
    _nombre.backgroundColor = [UIColor whiteColor];
    [self addSubview:_nombre];
    
    _fechaPicker = [[UIDatePicker alloc] initWithFrame: CGRectMake(0, 130, 180, 180)];
    _fechaPicker.datePickerMode = UIDatePickerModeDate;
    [self addSubview: _fechaPicker];
    
    UIButton *add = [UIButton buttonWithType:UIButtonTypeCustom];
    [add addTarget:self
             action:@selector(botonAdd)
   forControlEvents:UIControlEventTouchUpInside];
    [add setTitle:@"Aceptar" forState:UIControlStateNormal];
    add.backgroundColor = [UIColor greenColor];
    add.frame = CGRectMake(10, self.frame.size.height-40, self.frame.size.width-27, 30.0);
    [self addSubview:add];
}

- (void)botonAdd{
    if ([self.nombre.text isEqual: @""]){
        NSLog(@"No se ha introducido ningun nombre"); //TODO Agregar alert view y que no cierre la vista
    }else{
        if(_editMode){
            [self.delegate editAmigoFinalDidPerformAction:self];
        }else{
            if (self.delegate && [self.delegate respondsToSelector:@selector(addAmigoFinalDidPerformAction:)]) {
                [self.delegate addAmigoFinalDidPerformAction:self];
            }
        }
        
    }
    
}

- (void)botonFotoPerfil:(id)sender{
    [_delegate botonFotoPerfil:sender];
}

- (void)limpiarCampos{
    _nombre.text = @"";
    _foto = [UIImage imageNamed:@"icono-amigos.png"];
    _fotoView.image = _foto;
    _fotoData = nil;
}

-(void)changeImage:(UIImage*)newImage
{
    NSLog(@"Debug: Cambiando foto perfil amigos %@",newImage);
    _fotoView.image = newImage;
    
    NSData *imageData = [NSData dataWithData:UIImagePNGRepresentation(newImage)];
    _fotoData = imageData;
}

@end

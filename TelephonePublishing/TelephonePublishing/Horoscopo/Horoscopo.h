//
//  Horoscopo.h
//  TelephonePublishing
//
//  Created by Tecnico IOS on 28/4/15.
//  Copyright (c) 2015 Hearst Magazine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HMDataManager.h"

@interface Horoscopo : UIViewController <HMDataManagerDelegate>
{
    NSString *type;
    
    NSString *url;
    
    //DataManager
    HMDataManager *_dataManager;
}

- (id)initWithType:(NSString *)typeHoroscopo url:(NSString *)urlData;

@property (retain, nonatomic) NSMutableArray* contentObject;

@end

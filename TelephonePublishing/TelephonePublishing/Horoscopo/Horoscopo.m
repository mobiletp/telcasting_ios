//
//  Horoscopo.m
//  TelephonePublishing
//
//  Created by Tecnico IOS on 28/4/15.
//  Copyright (c) 2015 Hearst Magazine. All rights reserved.
//

#import "Horoscopo.h"
#import "HoroscopoMes.h"
#import "TPAppDelegate.h"

@interface Horoscopo ()

@end

@implementation Horoscopo

@synthesize contentObject;

- (id)initWithType:(NSString *)typeHoroscopo url:(NSString *)urlData
{
    if (self = [super init])
    {
        type = typeHoroscopo;
        url = urlData;
    }
    return self;
}

- (void)viewDidLoad
{
    contentObject = [[NSMutableArray alloc] initWithCapacity:0];
    
    [super viewDidLoad];
    [self createButtons];
    [self loadList:url];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)createButtons
{
    NSInteger x = 10;
    NSInteger y = 30;
    NSInteger margin = 10;
    NSInteger contadorColumnas = 0;
    
    for (int i = 1; i <= 12; i++)
    {
        //Creacion de botones para las cartas
        UIButton *signo = [[UIButton alloc] initWithFrame:CGRectMake(x + margin, y + margin, 80, 80)];
        [signo setBackgroundImage:[self buttonImage:i] forState:UIControlStateNormal];
        [signo addTarget:self action:@selector(actionButton:) forControlEvents:UIControlEventTouchUpInside];
        signo.tag = i;
        [self.view addSubview:signo];
        x = signo.frame.origin.x + margin + signo.frame.size.width;
        contadorColumnas++;
        if (contadorColumnas == 3)
        {
            contadorColumnas = 0;
            x = 10;
            y = signo.frame.origin.y + margin + signo.frame.size.height;
        }
    }
    
}

-(UIImage *)buttonImage:(NSInteger)index
{
    UIImage *image;
    if([type  isEqual: @"chino"]){
        if(index == 1)
        {
            image = [UIImage imageNamed:@"rata.png"];
        }
        else if(index == 2)
        {
            image = [UIImage imageNamed:@"buey.png"];
        }
        else if(index == 3)
        {
            image = [UIImage imageNamed:@"tigre.png"];
        }
        else if(index == 4)
        {
            image = [UIImage imageNamed:@"conejo.png"];
        }
        else if(index == 5)
        {
            image = [UIImage imageNamed:@"dragon.png"];
        }
        else if(index == 6)
        {
            image = [UIImage imageNamed:@"serpiente.png"];
        }
        else if(index == 7)
        {
            image = [UIImage imageNamed:@"caballo.png"];
        }
        else if(index == 8)
        {
            image = [UIImage imageNamed:@"cabra.png"];
        }
        else if(index == 9)
        {
            image = [UIImage imageNamed:@"mono.png"];
        }
        else if(index == 10)
        {
            image = [UIImage imageNamed:@"gallo.png"];
        }
        else if(index == 11)
        {
            image = [UIImage imageNamed:@"perro.png"];
        }
        else if(index == 12)
        {
            image = [UIImage imageNamed:@"cerdo.png"];
        }
    }
    else
    {
        if(index == 1)
        {
            image = [UIImage imageNamed:@"acuario.png"];
        }
        else if(index == 2)
        {
            image = [UIImage imageNamed:@"piscis.png"];
        }
        else if(index == 3)
        {
            image = [UIImage imageNamed:@"aries.png"];
        }
        else if(index == 4)
        {
            image = [UIImage imageNamed:@"tauro.png"];
        }
        else if(index == 5)
        {
            image = [UIImage imageNamed:@"geminis.png"];
        }
        else if(index == 6)
        {
            image = [UIImage imageNamed:@"cancer.png"];
        }
        else if(index == 7)
        {
            image = [UIImage imageNamed:@"leo.png"];
        }
        else if(index == 8)
        {
            image = [UIImage imageNamed:@"virgo.png"];
        }
        else if(index == 9)
        {
            image = [UIImage imageNamed:@"libra.png"];
        }
        else if(index == 10)
        {
            image = [UIImage imageNamed:@"escorpio.png"];
        }
        else if(index == 11)
        {
            image = [UIImage imageNamed:@"sagitario.png"];
        }
        else if(index == 12)
        {
            image = [UIImage imageNamed:@"capricornio.png"];
        }
    }
    return image;
}

-(IBAction)actionButton:(id)sender
{
    UIButton *button = sender;
    if([type  isEqual: @"chino"]){
        [self loadHoroscopoChino:button.tag-1];
    }
    else
    {
        [self loadHoroscopoMes:button.tag-1];
    }
}

-(void)loadHoroscopoMes:(NSInteger)idx
{
    NSLog(@"Count: %lu",(unsigned long)contentObject.count);
    [((TPAppDelegate*)[UIApplication sharedApplication].delegate) goDetail:@"horoscopomes" array:contentObject idx:idx];
}

-(void)loadHoroscopoChino:(NSInteger)idx
{
    [((TPAppDelegate*)[UIApplication sharedApplication].delegate) goDetail:@"horoscopochino" array:contentObject idx:idx];
}

#pragma mark - Load data

-(void)loadList:(NSString *)url
{
    _dataManager = [[HMDataManager alloc] initHMDataManager];
    [_dataManager setDelegate:self];
    
    //Error flag
    //_isError=NO;
    
    //Loading state
    //_isLoading=YES;
    
    //conn
    [_dataManager loadData:url withSender:SENDER_FEED];
}

#pragma mark - DataManager delegate

- (void)dataManagerDidFinish:(HMDataManager *)dataManager
{
    //End loading
    //_isLoading=NO;
}

- (void)dataManagerDidParse:(HMDataManager *)dataManager withObject:(NSObject *)object
{
    //Add object
    if([type  isEqual: @"normal"]){
        [contentObject addObject:(HoroscopoMes *)object];
    }
    else if([type  isEqual: @"horoscopochino"])
    {
        
    }
}

- (void)dataManagerDidFail:(HMDataManager *)dataManager withError:(NSError *)error
{
    //Fin estado carga
    //_isLoading=NO;
    
    //Estado error
    //_isError=YES;
    
    //Recargo tabla
}

@end

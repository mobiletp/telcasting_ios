//
//  TPBotonAyuda.m
//  TelephonePublishing
//
//  Created by Tpbeca tec on 17/09/14.
//  Copyright (c) 2014 Hearst Magazine. All rights reserved.
//

#import "TPBotonAyuda.h"

@interface TPBotonAyuda (Private)

- (void) drawRoundedRect:(CGRect) rect inContext:(CGContextRef)

context withRadius:(CGFloat) radius;

@end

@implementation TPBotonAyuda

static UIColor *fillColor = nil;
static UIColor *borderColor = nil;

- (void) setBackgroundColor:(UIColor *) background withStrokeColor:(UIColor *) stroke
{
    fillColor = background;
    borderColor = stroke;
}



- (instancetype) initWithFrame:(CGRect)frame

{
    if((self = [super initWithFrame:frame]))
    {
        if(fillColor == nil)
        {
            
        }
    }
    return self;
}



- (void) layoutSubviews

{
    for (UIView *sub in [self subviews])
    {
        if([sub class] == [UIImageView class] && sub.tag == 0)
        {
            [sub removeFromSuperview];
            break;
        }
    }
}



- (void) drawRect:(CGRect)rect
{
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextClearRect(context, rect);
    CGContextSetAllowsAntialiasing(context, true);
    CGContextSetLineWidth(context, 0.0);
    CGContextSetAlpha(context, 0.8);
    CGContextSetLineWidth(context, 2.0);
    CGContextSetStrokeColorWithColor(context, [borderColor CGColor]);
    CGContextSetFillColorWithColor(context, [fillColor CGColor]);
    
    
    
    CGFloat backOffset = 2;
    CGRect backRect = CGRectMake(rect.origin.x + backOffset,
                                 rect.origin.y + backOffset,
                                 rect.size.width - backOffset*2,
                                 rect.size.height - backOffset*2);
    
    
    
    [self drawRoundedRect:backRect inContext:context withRadius:8];
    CGContextDrawPath(context, kCGPathFillStroke);
    
    CGRect clipRect = CGRectMake(backRect.origin.x + backOffset-1,
                                 backRect.origin.y + backOffset-1,
                                 backRect.size.width - (backOffset-1)*2,
                                 backRect.size.height - (backOffset-1)*2);
    
    [self drawRoundedRect:clipRect inContext:context withRadius:8];
    CGContextClip (context);
    
    CGGradientRef glossGradient;
    CGColorSpaceRef rgbColorspace;
    size_t num_locations = 2;
    CGFloat locations[2] = { 0.0, 1.0 };
    CGFloat components[8] = { 1.0, 1.0, 1.0, 0.35, 1.0, 1.0, 1.0, 0.06 };
    rgbColorspace = CGColorSpaceCreateDeviceRGB();
    glossGradient = CGGradientCreateWithColorComponents(rgbColorspace,
                                                        components, locations, num_locations);
    
    CGRect ovalRect = CGRectMake(-130, -115, (rect.size.width*2),
                                 rect.size.width/2);
    
    
    
    CGPoint start = CGPointMake(rect.origin.x, rect.origin.y);
    CGPoint end = CGPointMake(rect.origin.x, rect.size.height/5);
    
    CGContextSetAlpha(context, 1.0);
    CGContextAddEllipseInRect(context, ovalRect);
    CGContextClip (context);
    
    CGContextDrawLinearGradient(context, glossGradient, start, end, 0);
    
    CGGradientRelease(glossGradient);
    CGColorSpaceRelease(rgbColorspace);
}



- (void) drawRoundedRect:(CGRect) rrect inContext:(CGContextRef) context
              withRadius:(CGFloat) radius
{
    CGContextBeginPath (context);
    
    CGFloat minx = CGRectGetMinX(rrect), midx = CGRectGetMidX(rrect),
    maxx = CGRectGetMaxX(rrect);
    
    CGFloat miny = CGRectGetMinY(rrect), midy = CGRectGetMidY(rrect),
    maxy = CGRectGetMaxY(rrect);
    
    CGContextMoveToPoint(context, minx, midy);
    CGContextAddArcToPoint(context, minx, miny, midx, miny, radius);
    CGContextAddArcToPoint(context, maxx, miny, maxx, midy, radius);
    CGContextAddArcToPoint(context, maxx, maxy, midx, maxy, radius);
    CGContextAddArcToPoint(context, minx, maxy, minx, midy, radius);
    CGContextClosePath(context);
}

/*- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)layoutSubviews
{
    for (UIView *subview in self.subviews){ //Fast Enumeration
        if ([subview isMemberOfClass:[UIImageView class]]) {
            subview.hidden = YES; //Hide UIImageView Containing Blue Background
        }
        
        if ([subview isMemberOfClass:[UILabel class]]) { //Point to UILabels To Change Text
            UILabel *label = (UILabel*)subview; //Cast From UIView to UILabel
            label.textColor = [UIColor colorWithRed:210.0f/255.0f green:210.0f/255.0f blue:210.0f/255.0f alpha:1.0f];
            label.shadowColor = [UIColor blackColor];
            label.shadowOffset = CGSizeMake(0.0f, 1.0f);
        }
    }
}
*/

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}

*/
@end

//
//  TPToolBar.m
//  TelephonePublishing
//
//  Created by Tpbeca tec on 17/09/14.
//  Copyright (c) 2014 Hearst Magazine. All rights reserved.
//

#import "TPToolbar.h"
#import "TPBotonAyuda.h"
#import "TPConstants.h"
#import "TPUtils.h"

@implementation TPToolbar

@synthesize botonMenu=_botonMenu;
@synthesize botonAyuda=_botonAyuda;

#pragma mark - Button methods

-(void)pushMenu:(id)sender
{
    if(_delegate && [_delegate respondsToSelector:@selector(toolbarDelegateMenu:)])
        [_delegate toolbarDelegateMenu:self];
}

-(void)pushHelp:(id)sender
{
    if(_delegate && [_delegate respondsToSelector:@selector(toolbarDelegateHelp:)])
        [_delegate toolbarDelegateHelp:self];
}

#pragma mark - Init

-(instancetype)init
{
    if (self = [super initWithFrame:CGRectMake(TOOLBAR_X, [[UIScreen mainScreen] applicationFrame].size.height-TOOLBAR_HEIGHT, TOOLBAR_WIDTH, TOOLBAR_HEIGHT)])
	{
        //color fondo
        [self setBackgroundColor:[UIColor greenColor]]; //para fondos poner clearcolor
        //background
        
        //Variables
        NSInteger totalButtons=4;
        NSInteger width=TOOLBAR_WIDTH/totalButtons;
        NSInteger x = 0*width;
        
        //menu
        _botonMenu = [TPUtils buttonCreate:x+15 :0 :16 :TOOLBAR_BUT_HEIGHT :nil :nil :nil :@""];
        [_botonMenu addTarget:self action:@selector(pushMenu:) forControlEvents:UIControlEventTouchUpInside];
        [_botonMenu setBackgroundColor:[UIColor clearColor]];
        //[_botonMenu setImage:[UIImage imageNamed:@"botonera-pulse.png"] forState:UIControlStateHighlighted]; //imagen pulsado
        UIImageView *icon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"botonera-pulse.png"]]; //imagen normal
        icon.frame = CGRectMake(0, TOOLBAR_BUT_HEIGHT/2-18/2, 16, 18);
        [_botonMenu addSubview:icon];
        [self addSubview:_botonMenu];
        
        //Etiqueta titulo aplicacion
        x += width;
        _appName = [TPUtils buttonCreate:x :0 :width*2 :TOOLBAR_BUT_HEIGHT :nil :nil :nil :APP_NAME];
        [_appName addTarget:self action:nil forControlEvents:UIControlEventTouchUpInside];
        [_appName setBackgroundColor:[UIColor clearColor]];
        [self addSubview:_appName];
        x += width;
        //Ayuda
        x += width;
        _botonAyuda = [TPUtils buttonCreate:x+((totalButtons-1)*14) :0 :16 :TOOLBAR_BUT_HEIGHT :nil :nil :nil :@""];
        [_botonAyuda addTarget:self action:@selector(pushHelp:) forControlEvents:UIControlEventTouchUpInside];
        [_botonAyuda setBackgroundColor:[UIColor clearColor]];
        //[_botonAyuda setImage:[UIImage imageNamed:@"ayuda-pulse.png"] forState:UIControlStateHighlighted]; //imagen pulsado
        icon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ayuda-pulse.png"]]; //imagen normal
        icon.frame = CGRectMake(0, TOOLBAR_BUT_HEIGHT/2-18/2, 16, 18);
        [_botonAyuda addSubview:icon];
        [self addSubview:_botonAyuda];
        
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end

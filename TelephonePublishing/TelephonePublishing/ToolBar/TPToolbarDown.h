//
//  TPToolBarDown.h
//  TelephonePublishing
//
//  Created by Tpbeca tec on 17/09/14.
//  Copyright (c) 2014 Hearst Magazine. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TPToolbarDown;

@protocol TPToolbarDownDelegate <NSObject>
-(void)toolbarDownDelegateMas:(TPToolbarDown *)downToolbar;
@end

@interface TPToolbarDown : UIView
{
    //Fondo
    
    //Botones
    UIButton *_appName;
    UIButton *_botonMas;
}

- (instancetype) init NS_DESIGNATED_INITIALIZER;

@property (assign) id<TPToolbarDownDelegate> delegate;
@property (nonatomic,retain) UIButton *botonMas;

@end


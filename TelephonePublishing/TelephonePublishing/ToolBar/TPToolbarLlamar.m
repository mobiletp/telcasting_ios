//
//  TPToolBarDown.m
//  TelephonePublishing
//
//  Created by Tpbeca tec on 17/09/14.
//  Copyright (c) 2014 Hearst Magazine. All rights reserved.
//

#import "TPToolbarLlamar.h"
#import "TPConstants.h"
#import "TPUtils.h"

@implementation TPToolbarLlamar

@synthesize botonLlamar=_botonLlamar;

#pragma mark - Button methods

-(void)pushLlamar:(id)sender
{
    if(_delegate && [_delegate respondsToSelector:@selector(toolbarDownDelegateLlamar:)])
        [_delegate toolbarDownDelegateLlamar:self];
}

#pragma mark - Init

-(instancetype)init
{
    if (self = [super initWithFrame:CGRectMake(TOOLBAR_X, [[UIScreen mainScreen] applicationFrame].size.height-TOOLBAR_HEIGHT, TOOLBAR_WIDTH, TOOLBAR_HEIGHT)])
	{
        //color fondo
        [self setBackgroundColor:[UIColor greenColor]]; //para fondos poner clearcolor
        //background
        
        //Variables
        NSInteger width=TOOLBAR_WIDTH;
        NSInteger x = width;
        
        //¿Quieres saber más?
        x = 0;
        _botonLlamar = [TPUtils buttonCreate:x :0 :width :TOOLBAR_BUT_HEIGHT :nil :nil :nil :@"Contacta con mi equipo de expertos"]; //TODO Cambiar a constante
        [_botonLlamar addTarget:self action:@selector(pushLlamar:) forControlEvents:UIControlEventTouchUpInside];
        [_botonLlamar setBackgroundColor:[UIColor clearColor]];
        [self addSubview:_botonLlamar];
        
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end

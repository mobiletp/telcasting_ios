//
//  TPToolBar.h
//  TelephonePublishing
//
//  Created by Tpbeca tec on 17/09/14.
//  Copyright (c) 2014 Hearst Magazine. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TPToolbar;

@protocol TPToolbarDelegate <NSObject>
-(void)toolbarDelegateMenu:(TPToolbar *)topToolbar;
-(void)toolbarDelegateHelp:(TPToolbar *)topToolbar;
@end

@interface TPToolbar : UIView
{
    //Fondo
    
    //Botones
    UIButton *_botonMenu;
    UIButton *_appName;
    UIButton *_botonAyuda;
}

- (instancetype) init NS_DESIGNATED_INITIALIZER;

@property (assign) id<TPToolbarDelegate> delegate;
@property (nonatomic,retain) UIButton *botonMenu;
@property (nonatomic,retain) UIButton *botonAyuda;

@end


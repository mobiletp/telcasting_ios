//
//  TPToolBarDown.m
//  TelephonePublishing
//
//  Created by Tpbeca tec on 17/09/14.
//  Copyright (c) 2014 Hearst Magazine. All rights reserved.
//

#import "TPToolbarDown.h"
#import "TPConstants.h"
#import "TPUtils.h"

@implementation TPToolbarDown

@synthesize botonMas=_botonMas;

#pragma mark - Button methods

-(void)pushMas:(id)sender
{
    if(_delegate && [_delegate respondsToSelector:@selector(toolbarDownDelegateMas:)])
        [_delegate toolbarDownDelegateMas:self];
}

#pragma mark - Init

-(instancetype)init
{
    if (self = [super initWithFrame:CGRectMake(TOOLBAR_X, [[UIScreen mainScreen] applicationFrame].size.height-TOOLBAR_HEIGHT, TOOLBAR_WIDTH, TOOLBAR_HEIGHT)])
	{
        //color fondo
        [self setBackgroundColor:[UIColor greenColor]]; //para fondos poner clearcolor
        //background
        
        //Variables
        NSInteger width=TOOLBAR_WIDTH;
        NSInteger x;
        
        //¿Quieres saber más?
        x = 0;
        _botonMas = [TPUtils buttonCreate:x :0 :width :TOOLBAR_BUT_HEIGHT :nil :nil :nil :@"¿Quieres saber más?"]; //TODO cambiar a constante
        [_botonMas addTarget:self action:@selector(pushMas:) forControlEvents:UIControlEventTouchUpInside];
        [_botonMas setBackgroundColor:[UIColor clearColor]];
        [self addSubview:_botonMas];
        
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end

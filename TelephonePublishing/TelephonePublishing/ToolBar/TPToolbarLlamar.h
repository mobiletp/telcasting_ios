//
//  TPToolBarDown.h
//  TelephonePublishing
//
//  Created by Tpbeca tec on 17/09/14.
//  Copyright (c) 2014 Hearst Magazine. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TPToolbarLlamar;

@protocol TPToolbarLlamarDelegate <NSObject>
-(void)toolbarDownDelegateLlamar:(TPToolbarLlamar *)llamarToolbar;
@end

@interface TPToolbarLlamar : UIView
{
    //Fondo
    
    //Botones
    UIButton *_botonLlamar;
}

- (instancetype) init NS_DESIGNATED_INITIALIZER;

@property (assign) id<TPToolbarLlamarDelegate> delegate;
@property (nonatomic,retain) UIButton *botonLlamar;

@end


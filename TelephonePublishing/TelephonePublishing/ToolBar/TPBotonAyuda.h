//
//  TPBotonAyuda.h
//  TelephonePublishing
//
//  Created by Tpbeca tec on 17/09/14.
//  Copyright (c) 2014 Hearst Magazine. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TPBotonAyuda : UIAlertView

-(void) setBackgroundColor:(UIColor *) background withStrokeColor:(UIColor *) stroke;

@end

//
//  NumerologiaViewController.m
//  TelephonePublishing
//
//  Created by Tecnico IOS on 28/4/15.
//  Copyright (c) 2015 Hearst Magazine. All rights reserved.
//

#import "NumerologiaViewController.h"

@interface NumerologiaViewController ()

@end

@implementation NumerologiaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpVista];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setUpVista
{
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    UIImage *imagenFondo = [UIImage imageNamed:@"fondo-numeros.png"];
    UIImageView *fondo = [[UIImageView alloc]initWithImage:imagenFondo];
    [fondo setFrame:CGRectMake(0, 0, 320, 100)];
    [self.view addSubview:fondo];
    
    UILabel *descripcion = [[UILabel alloc]initWithFrame:CGRectMake(10, 10 + fondo.frame.size.height, 300, 120)];
    [descripcion setNumberOfLines:0];
    [descripcion setText:@"Es la ciencia de los números. Tenemos conocimiento de ella hace tantos siglos que no es posible definir el momento preciso en el que nace. Fue utilizada por el hombre desde que los simbolos entraron en su vida, y desarrollada a través de civilizaciones tan antiguas como la caldea y la egipcia."];
    [self.view addSubview:descripcion];
    
    UILabel *titulo = [[UILabel alloc]initWithFrame:CGRectMake(10, 10 + descripcion.frame.origin.y + descripcion.frame.size.height, 300, 20)];
    [titulo setText:@"Introduce tu fecha de nacimiento"];
    [titulo setTextColor:[UIColor greenColor]];
    [self.view addSubview:titulo];
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(10, 1 + titulo.frame.origin.y + titulo.frame.size.height, 300, 2)];
    lineView.backgroundColor = [UIColor greenColor];
    [self.view addSubview:lineView];
    
    UITextField *tDia = [[UITextField alloc]initWithFrame:CGRectMake(10, 10 + lineView.frame.origin.y + lineView.frame.size.height, 96, 25)];
    tDia.textAlignment = NSTextAlignmentCenter;
    tDia.borderStyle = UITextBorderStyleRoundedRect;
    tDia.layer.borderWidth = 1.0f;
    tDia.layer.borderColor = [[UIColor grayColor] CGColor];
    tDia.layer.cornerRadius = 5;
    [self.view addSubview:tDia];
    
    UITextField *tMes = [[UITextField alloc]initWithFrame:CGRectMake(6 + tDia.frame.origin.x + tDia.frame.size.width, 10 + lineView.frame.origin.y + lineView.frame.size.height, 96, 25)];
    tMes.textAlignment = NSTextAlignmentCenter;
    tMes.borderStyle = UITextBorderStyleRoundedRect;
    tMes.layer.borderWidth = 1.0f;
    tMes.layer.borderColor = [[UIColor grayColor] CGColor];
    tMes.layer.cornerRadius = 5;
    [self.view addSubview:tMes];
    
    UITextField *tAnyo = [[UITextField alloc]initWithFrame:CGRectMake(6 + tMes.frame.origin.x + tMes.frame.size.width, 10 + lineView.frame.origin.y + lineView.frame.size.height, 96, 25)];
    tAnyo.textAlignment = NSTextAlignmentCenter;
    tAnyo.borderStyle = UITextBorderStyleRoundedRect;
    tAnyo.layer.borderWidth = 1.0f;
    tAnyo.layer.borderColor = [[UIColor grayColor] CGColor];
    tAnyo.layer.cornerRadius = 5;
    [self.view addSubview:tAnyo];
    
    UILabel *lDia = [[UILabel alloc]initWithFrame:CGRectMake(10, 5 + tDia.frame.origin.y + tDia.frame.size.height, 96, 25)];
    lDia.textAlignment = NSTextAlignmentCenter;
    lDia.text = @"Día";
    [self.view addSubview:lDia];
    
    UITextField *lMes = [[UITextField alloc]initWithFrame:CGRectMake(6 + lDia.frame.origin.x + lDia.frame.size.width, 5 + tMes.frame.origin.y + tMes.frame.size.height, 96, 25)];
    lMes.textAlignment = NSTextAlignmentCenter;
    lMes.text = @"Mes";
    [self.view addSubview:lMes];
    
    UITextField *lAnyo = [[UITextField alloc]initWithFrame:CGRectMake(6 + lMes.frame.origin.x + lMes.frame.size.width, 5 + tAnyo.frame.origin.y + tAnyo.frame.size.height, 96, 25)];
    lAnyo.textAlignment = NSTextAlignmentCenter;
    lAnyo.text = @"Año";
    [self.view addSubview:lAnyo];
    
    UIButton *calcular = [[UIButton alloc] initWithFrame:CGRectMake(12, [[UIScreen mainScreen] bounds].size.height-28, 296, 25)];
    [calcular setBackgroundColor:[UIColor greenColor]];
    [calcular setTitle:@"Cálcular" forState:UIControlStateNormal];
    [calcular.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [self.view addSubview:calcular];
    
    //TODO ACCION BOTON CALCULAR
}

@end

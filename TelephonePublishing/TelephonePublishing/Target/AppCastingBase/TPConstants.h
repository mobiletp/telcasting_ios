//
//  HMConstants.h
//  TelephonePublishing - Berta
//
//  Created by David Fernandez on 18/09/14.
//  Copyright (c) 2014 Hearst Magazine. All rights reserved.
//

#ifndef HearstMagazines_HMConstants_h
#define HearstMagazines_HMConstants_h
#define APP_DEF 3



//  Macro iOS
//-----------------------------------------------------

#define IS_IPHONE5          (([[UIScreen mainScreen] bounds].size.height-568)?NO:YES)
#define IS_IPHONE4          (([[UIScreen mainScreen] bounds].size.height-480)?NO:YES)
#define IS_IPHONE6          ([[UIScreen mainScreen] bounds].size.height == 667.0)
#define IS_IPHONE6PLUS      ([[UIScreen mainScreen] bounds].size.height == 736.0)
#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#define STATUS_BAR_HEIGHT 20


//  Macro UIColorFromRGB
//------------------------------------------------------------------------------

#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]


//  Bitly
//-----------------------------------------------------

#define kBitLyUser          @"dmapp2012"
#define kBitLyAPIKey        @"R_d86e5dd91df031d2c3b60d2945afde8c"

//  BugSense
//-----------------------------------------------------

static NSString *const BugSenseApiKey = @"a84ed9e8";

//  Flurry
//-----------------------------------------------------

static NSString *const FlurryApiKey = @"WKP4QKCY6PXG39WFYXHY";

//  Connections
//-----------------------------------------------------

//#define CONFIG_SECTIONS_URL @"http://especiales.hearst.es/app/iphone/berta/berta_sections_V10_i.json"
#define CONFIG_SECTIONS_URL @"http://especiales.hearst.es/app/comun/casting/casting_params2.json"

#define CONFIG_SECTIONS_FILE_NAME @"Config.json"

#define KEWEGO_URL_VIDEOS @"http://api.kewego.com/iphone/getChannelVideos/"
#define KEWEGO_URL_SELECTED_VIDEO @"http://api.kewego.com/iphone/getStream/"

#define APP_WEBSITE @"http://www.quemedices.es"
#define APP_HOROSCOPE @"http://www.quemedices.es/horoscopo/"
#define APP_ITUNES @"https://itunes.apple.com/us/app/que-me-dices-famosos-moda/id450709241?l=es&ls=1&mt=8"

//  Sections
//-----------------------------------------------------

//  Sections
//-----------------------------------------------------

typedef NS_ENUM(NSUInteger, MENU_TYPE) {
    MENU_TYPE_SLIDE = 0,
    MENU_TYPE_TILEVIEW_SLIDE,
    MENU_TYPE_HORIZONTAL_SLIDE
};

#define SECTIONS_TYPE_MENU MENU_TYPE_SLIDE

#define MENU_CELL_HEIGHT 35
#define MENU_IMAGE_ARROW_OFFSET 18
#define MENU_TABLE_X 170
#define MENU_TABLE_Y 55
#define MENU_TABLE_WIDTH 138
#define MENU_TABLE_HEIGHT 280//239

#define MENU_CELL_ICON_WIDTH 30
#define MENU_CELL_ICON_HEIGHT 25

#define MENU_CELL_FONT [UIFont fontWithName:FONT_VECTORIAL_MEDIUM size: 14]
#define MENU_CELL_COLOR [UIColor whiteColor]


//  Colors
//-----------------------------------------------------

#define COLOR_APP 0x009FE4 //Blue QMD
#define COLOR_WHITE 0xffffff
#define COLOR_BLACK 0x000000
#define COLOR_GRAY_SOFT 0x777777
#define COLOR_GRAY_VERY_SOFT 0xeaeaea
#define IMAGE_FILTER 0.4

#define COLOR_ARTICLE_TITLE COLOR_BLACK

//  Macro UIColorFromRGB
//------------------------------------------------------------------------------

#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]


//  Fonts
//-----------------------------------------------------

#define FONT_TTF @"ttf"
#define FONT_OTF @"otf"

#define FONT_VECTORIAL_BOOK @"VectoraLTStd-Roman"
#define FONT_FUTURA_BOOK @"FuturaStd-Book"
#define FONT_FUTURA_BOLD @"FuturaStd-Bold"
#define FONT_FUTURA_MEDIUM @"FuturaStd-Medium"
#define FONT_HELVETICA_NEUE_BOLD @"HelveticaNeueLTStd-Bd"
#define FONT_HELVETICA_NEUE_LIGHT @"HelveticaNeueLTStd-Lt"
#define FONT_HELVETICA_NEUE_ROMAN @"HelveticaNeueLTStd-Roman"
#define FONT_HELVELTICA_NEUE_MEDIUM @"HelveticaNeueLTStd-Md"

#define FONT_TITULO             @"FjallaOne-Regular"        //Titulo
#define FONT_ENTRADILLA         @"HelveticaNeueLTStd-Bd"    //entradillas
#define FONT_TEXTOS             @"HelveticaNeueLTStd-Roman" //textos
#define FONT_TITULO_SECUNDARIO  FONT_TITULO                 //Titulo celdas secundarias

//  App
//-----------------------------------------------------

#define MARGIN 10
#define APP_NAME @"Berta De La Prada"
#define UD_HM_VIDEO_TOKEN @"QueMeDices_UserDefault_VideoToken"

//  Splash
//-----------------------------------------------------

#define SPLASH_IMAGE_X 0
#define SPLASH_IMAGE_Y (IS_OS_7_OR_LATER ? 0 : -20)
#define SPLASH_IMAGE_WIDTH 320
#define SPLASH_IMAGE_HEIGHT 480
#define SPLASH_IMAGE_BAR 20

#define SPLASH_ACTIVITY_X SPLASH_LOADING_X-SPLASH_ACTIVITY_SIZE-10
#define SPLASH_ACTIVITY_Y 50
#define SPLASH_ACTIVITY_SIZE 26

#define SPLASH_LOADING_X 320/2-15
#define SPLASH_LOADING_WIDTH 320
#define SPLASH_LOADING_HEIGHT 25

#define SPLASH_TIMER 1.5


//  TitleBar
//-----------------------------------------------------

#define TITLE_BAR_FONT [UIFont fontWithName:FONT_VECTORIAL_BOOK size:13]
#define TITLE_BAR_X MARGIN
#define TITLE_BAR_Y NAVBAR_HEIGHT
#define TITLE_BAR_WIDTH (320-2*MARGIN)
#define TITLE_BAR_HEIGHT (18)

#define TITLE_LABEL_X 10
#define TITLE_LABEL_Y 2
#define TITLE_LABEL_WIDTH (TITLE_BAR_WIDTH-TITLE_LABEL_X)
#define TITLE_LABEL_HEIGHT (TITLE_BAR_HEIGHT)

//  NavBar
//-----------------------------------------------------

#define NAVBAR_X 0
#define NAVBAR_Y 0
#define NAVBAR_WIDTH 320
#define NAVBAR_HEIGHT (IS_OS_7_OR_LATER ? 64 : 44)

#define NAVBAR_MENU_BUT_FONT [UIFont fontWithName:FONT_HELVETICA_NEUE_BOLD size:13]
#define NAVBAR_MENU_BUT_X (320-NAVBAR_MENU_BUT_WIDTH-5)
#define NAVBAR_MENU_BUT_Y (IS_OS_7_OR_LATER ? 20 : 0)
#define NAVBAR_MENU_BUT_WIDTH (/*45*/70)
#define NAVBAR_MENU_BUT_HEIGHT (44)

#define NAVBAR_MENU_BUT_MARGIN_TOP (27)
#define NAVBAR_MENU_LIST_BUT_MARGIN (6)

#define NAVBAR_MENU_IMG_X (NAVBAR_MENU_BUT_WIDTH-NAVBAR_MENU_IMG_WIDTH-5)
#define NAVBAR_MENU_IMG_Y 6
#define NAVBAR_MENU_IMG_WIDTH (29)
#define NAVBAR_MENU_IMG_HEIGHT (34)

#define NAVBAR_BACK_BUT_FONT (NAVBAR_MENU_BUT_FONT)
#define NAVBAR_BACK_BUT_X 5
#define NAVBAR_BACK_BUT_Y (IS_OS_7_OR_LATER ? 20 : 0)
#define NAVBAR_BACK_BUT_WIDTH 60
#define NAVBAR_BACK_BUT_HEIGHT 44

#define NAVBAR_BACK_IMG_X 5
#define NAVBAR_BACK_IMG_Y (NAVBAR_BACK_BUT_HEIGHT/2-NAVBAR_BACK_IMG_HEIGHT/2)
#define NAVBAR_BACK_IMG_WIDTH (24/2)
#define NAVBAR_BACK_IMG_HEIGHT (46/2)

#define NAVBAR_BACK_BUT_MARGIN_LEFT (20)
#define NAVBAR_BACK_BUT_MARGIN_TOP (6)

#define NAVBAR_FAV_BUT_X (NAVBAR_MENU_BUT_X-NAVBAR_FAV_BUT_WIDTH)
#define NAVBAR_FAV_BUT_Y (IS_OS_7_OR_LATER ? 20 : 0)
#define NAVBAR_FAV_BUT_WIDTH (NAVBAR_MENU_BUT_WIDTH)
#define NAVBAR_FAV_BUT_HEIGHT (NAVBAR_MENU_BUT_HEIGHT)

#define NAVBAR_MY_ELLE_IMG_X (NAVBAR_FAV_BUT_WIDTH/2-NAVBAR_MY_ELLE_IMG_WIDTH/2)
#define NAVBAR_MY_ELLE_IMG_Y 6
#define NAVBAR_MY_ELLE_IMG_WIDTH (64/2)
#define NAVBAR_MY_ELLE_IMG_HEIGHT (40/2)

#define NAVBAR_RECOGNITION_X 15
#define NAVBAR_RECOGNITION_Y (IS_OS_7_OR_LATER ? 25 : 5)
#define NAVBAR_RECOGNITION_WIDTH 34
#define NAVBAR_RECOGNITION_HEIGHT 34


//  Toolbar
//-----------------------------------------------------

#define TOOLBAR_X 0
#define TOOLBAR_Y (460-TOOLBAR_HEIGHT)
#define TOOLBAR_WIDTH [[UIScreen mainScreen] bounds].size.width
#define TOOLBAR_HEIGHT (68/2)

#define TOOLBAR_BUT_HEIGHT (TOOLBAR_HEIGHT)


//  Slide Menu
//-----------------------------------------------------

#define SLIDE_MENU_BACKGROUNDCOLOR 0xffffff
#define SLIDE_MENU_HEIGHT_ROW 32
#define SLIDE_MENU_HEIGHT_HEADER 30 //64

#define SLIDE_MENU_ROW_TEXT_FONT [UIFont fontWithName:FONT_TITULO size:20.0]
#define SLIDE_MENU_ROW_TEXT_COLOR 0x000000
#define SLIDE_MENU_ROW_SELECTION_COLOR 0x666666
#define SLIDE_MENU_ROW_BACKGROUNDCOLOR 0x666666

#define SLIDE_MENU_SEPARATORBAR 5
#define SLIDE_MENU_SEPARATOR_BACKGROUNDCOLOR 0x000000

#define SLIDE_MENU_SECTION_NAME @""
#define SLIDE_MENU_SECTION_TEXT_FONT [UIFont fontWithName:FONT_HELVETICA size:18.0]
#define SLIDE_MENU_SECTION_TEXT_COLOR 0x666666
#define SLIDE_MENU_SECTION_BACKGROUNDCOLOR 0xffffff

#define MENU_CELL_ICON_WIDTH 30
#define MENU_CELL_ICON_HEIGHT 25

//  Sectionbar
//-----------------------------------------------------

#define SECTIONBAR_X 0
#define SECTIONBAR_HEIGHT 24
#define SECTIONBAR_WIDTH (640/2)

//  Tabbar
//-----------------------------------------------------

#define TABBAR_X 0
#define TABBAR_HEIGHT 30
#define TABBAR_WIDTH (640/2)

//  Share view
//-----------------------------------------------------

#define SHARE_VIEW_X 320-SHARE_VIEW_WIDTH-10
#define SHARE_VIEW_Y (TOOLBAR_Y-SHARE_VIEW_HEIGHT)
#define SHARE_VIEW_WIDTH 262
#define SHARE_VIEW_HEIGHT 79

#define SHARE_VIEW_BUT_TOTAL 4

#define SHARE_VIEW_BUT_WIDTH (SHARE_VIEW_WIDTH/SHARE_VIEW_BUT_TOTAL)
#define SHARE_VIEW_BUT_HEIGHT 60

#define SHARE_VIEW_BUT_MARGIN_BOTTON 6
#define SHARE_VIEW_BUT_LABEL_HIEGHT 21

#define SHARE_VIEW_FONT [UIFont fontWithName:FONT_HELVETICA_NEUE_BOLD size:11]

//  Contact
//-----------------------------------------------------

#define CVC_NAV_BAR_X 0
#define CVC_NAV_BAR_Y 0
#define CVC_NAV_BAR_WIDTH NAVBAR_WIDTH
#define CVC_NAV_BAR_HEIGHT 44

#define NAVBAR_HOME_BUT_X (320/2-NAVBAR_HOME_BUT_WIDTH/2)
#define NAVBAR_HOME_BUT_Y (IS_OS_7_OR_LATER ? ((CVC_NAV_BAR_HEIGHT/2-NAVBAR_HOME_BUT_HEIGHT/2) + 20) : (CVC_NAV_BAR_HEIGHT/2-NAVBAR_HOME_BUT_HEIGHT/2))
#define NAVBAR_HOME_BUT_WIDTH 100
#define NAVBAR_HOME_BUT_HEIGHT 40

#define CVC_IMG_X (CVC_NAV_BAR_WIDTH/2-CVC_IMG_WIDTH/2)
#define CVC_IMG_Y (CVC_NAV_BAR_HEIGHT/2-CVC_IMG_HEIGHT/2)
#define CVC_IMG_WIDTH (135*0.8)
#define CVC_IMG_HEIGHT (45*0.8)

#define CVC_BAR_CONTACT_Y (CVC_NAV_BAR_Y+CVC_NAV_BAR_HEIGHT)

#define CVC_INFO_LABEL_X MARGIN
#define CVC_INFO_LABEL_HEIGHT 145
#define CVC_INFO_LABEL_WIDTH (320-2*CVC_INFO_LABEL_X)
#define CVC_INFO_LABEL_FONT [UIFont fontWithName:FONT_HELVELTICA_NEUE_MEDIUM size:14]

#define CVC_IMAGE_X 0
#define CVC_IMAGE_WIDTH 320
#define CVC_IMAGE_HEIGHT 26

#define CVC_TABLE_X 0
#define CVC_TABLE_WIDTH (320-2*CVC_TABLE_X)


//  List
//-----------------------------------------------------

#define LVC_TABLE_X 0
#define LVC_TABLE_Y (TITLE_BAR_Y+TITLE_BAR_HEIGHT)
#define LVC_TABLE_WIDTH 320
#define LVC_TABLE_HEIGHT (460-LVC_TABLE_Y)

#define BVC_TABLE_X 0
#define BVC_TABLE_Y (TITLE_BAR_Y+TITLE_BAR_HEIGHT/*+BVC_TITLE_HEIGHT*/)
#define BVC_TABLE_WIDTH 320

#define BVC_TITLE_X 10
#define BVC_TITLE_Y (TITLE_BAR_Y+TITLE_BAR_HEIGHT)
#define BVC_TITLE_WIDTH (320-2*BVC_TITLE_X)
#define BVC_TITLE_HEIGHT 30

#define BVC_TITLE_FONT [UIFont fontWithName:FONT_VECTORIAL_BOLD size:13]

#define BVC_TABLE_HEADER_X 0
#define BVC_TABLE_HEADER_Y 0
#define BVC_TABLE_HEADER_WIDTH 320
#define BVC_TABLE_HEADER_HEIGHT 30

#define BVC_HEADER_FONT [UIFont fontWithName:FONT_VECTORIAL_MEDIUM size:14]


//  Cells
//-----------------------------------------------------

typedef NS_ENUM(NSInteger, CELL_TYPE_VIEW) {
    CELL_TYPE_ITEM = 0,
    CELL_TYPE_ARTICLE
} ;

#define CELL_TYPE CELL_TYPE_ARTICLE

#define CELL_MARGIN 10

#define CELL_DEFAULT_HEIGHT 50

#define CELL_ARTICLE_BIG_IMAGE_WIDTH ([[UIScreen mainScreen] bounds].size.width-2*CELL_MARGIN)
#define CELL_ARTICLE_BIG_IMAGE_HEIGHT ([[UIScreen mainScreen] bounds].size.width-2*CELL_MARGIN)

#define CELL_IMAGE_SIZE 90//60
#define CELL_IMAGE_SIZENEW 200
#define CELL_IMAGE_SIZENEW_Y 320

#define CELL_TEXT_MAX_HEIGHT 40
#define CELL_APP_TEXT_MAX_HEIGHT 60

#define CELL_PLAY_ICON_WIDTH 68/2
#define CELL_PLAY_ICON_HEIGHT 60/2

#define CELL_TYPE CELL_TYPE_ARTICLE

#define CELL_IMAGE_SIZE 90//60
#define CELL_IMAGE_SIZE_HEIGHT CELL_IMAGE_SIZE

#define CELL_PLAY_ICON_WIDTH 68/2
#define CELL_PLAY_ICON_HEIGHT 60/2

#define CELL_SPECIAL_FONT [UIFont fontWithName:FONT_TEXTOS size:15]

#define CELL_ITEM_SECTION 0 //0 para desactivar la seccion
#define CELL_ITEM_SECTION_LAST @""
#define CELL_ITEM_SECTION_LIVING @""
#define CELL_ITEM_SECTION_FONT [UIFont fontWithName:FONT_ENTRADILLA size:12]
#define CELL_ITEM_SECTION_FONT_COLOR 0X666666
#define CELL_ITEM_CENTER 1

#define CELL_BLOG_TITLE_MAYUS 0
#define CELL_BLOG_TITLE_FONT [UIFont fontWithName:FONT_TITULO size:16]
#define CELL_BLOG_TEXT_FONT [UIFont fontWithName:FONT_TEXTOS size:12]
#define CELL_BLOG_CENTER 1

#define CELL_APP_TITLE_FONT (CELL_BLOG_TITLE_FONT)
#define CELL_APP_TEXT_FONT [UIFont fontWithName:FONT_TEXTOS size:14]

#define CELL_ASTRO_TITLE_FONT [UIFont fontWithName:FONT_TITULO size:14]
#define CELL_ASTRO_TEXT_FONT [UIFont fontWithName:FONT_TEXTOS size:13]

#define CELL_ARTICLE_TITLE_FONT (CELL_BLOG_TITLE_FONT)
#define CELL_ARTICLE_ENTRADILLA_FONT [UIFont fontWithName:FONT_TEXTOS size:12]

#define CELL_ARTICLE_BIG_TITLE_FONT [UIFont fontWithName:FONT_TITULO size: 18]
#define CELL_ARTICLE_BIG_ENTRADILLA_FONT [UIFont fontWithName:FONT_TEXTOS size:14]

#define CELL_ARTICLE_TITLE_SECUNDARIO_FONT [UIFont fontWithName:FONT_TITULO_SECUNDARIO size: 11]

#define CELL_CITADIA_TITLE_FONT [UIFont fontWithName:FONT_TITULO size:18]
#define CELL_CITADIA_CITA_FONT_IOS4 [UIFont fontWithName:FONT_TEXTOS size:24]
#define CELL_CITADIA_CITA_FONT [UIFont fontWithName:FONT_TEXTOS size:30]
#define CELL_CITADIA_AUTOR_FONT [UIFont fontWithName:FONT_ENTRADILLA size:15]

#define CELL_RECETAS_TITLE_FONT [UIFont fontWithName:FONT_TITULO size:22]
#define CELL_RECETAS_INT_TITLE_FONT [UIFont fontWithName:FONT_TITULO size:18]
#define CELL_RECETAS_TEXT_FONT [UIFont fontWithName:FONT_ENTRADILLA size:14]

#define CELL_DESCRIPTION 0
#define CELL_ARTICLE_CELL_DESCRIPTION 0
#define CELL_DESCRIPTION_BLOG 0
#define CELL_BLOG_CENTER 1
#define CELL_ITEM_CENTER 1
#define CELL_DESTACADO_TYPE 0
#define BLOG_SUBTITLE 0
#define DETAIL_GALERY_IMAGE_CROP 0
#define CELL_VIDEO_CENTER 0
#define IS_BLOG_NAME_IN_POST 0

//Galerias de fotos (con cuadrados)
//-----------------------------------------------------


//CABECERA DE LAS GALERIAS (TITULO GENERAL GALERIAS)
#define GALLERY_HEADER_HEIGHT 0
#define GALLERY_HEADER_WIDTH 320
#define GALLERY_HEADER_TEXT_COLOR 0x000000 //COLOR DEL TITULO DE GALERIA GENERAL
#define GALLERY_HEADER_TEXT_FONT [UIFont fontWithName:FONT_TITULO size:16.0]

//ITEMS GALERIA
#define GALLERY_CELL_MARGIN 0 //MARGEN ENTRE CUADRADOS DE GALERIA (SE PODRIA SEPARAR EN X,Y,ALTO Y ANCHO)
#define GALLERY_CELL_HEIGHT_IMPORTANT 200
#define GALLERY_CELL_WIDTH_IMPORTANT ([[UIScreen mainScreen] bounds].size.width)
#define GALLERY_CELL_HEIGHT 140
#define CELL_MARGIN_GALLERY CELL_MARGIN + 3
#define GALLERY_CELL_WIDTH [[UIScreen mainScreen] bounds].size.width/2 - 5
#define GALLERY_CELL_TEXT_COLOR 0xffffff //COLOR DEL TITULO DE LOS CUADRADOS DE GALERIA
#define GALLERY_CELL_TEXT_FONT [UIFont fontWithName:FONT_TITULO size:15.0]
#define GALLERY_CELL_DESTACADA_PORTADA 1

//  Detail
//-----------------------------------------------------

#define DVC_SCROLL_X 0
#define DVC_SCROLL_Y (TITLE_BAR_Y+TITLE_BAR_HEIGHT)
#define DVC_SCROLL_WIDTH 320
#define DVC_SCROLL_HEIGHT (460-DVC_SCROLL_Y/*-TOOLBAR_HEIGHT*/)

typedef NS_ENUM(NSInteger, DetailViewType) {
    DetailViewArticle = 0,
    DetailViewGallery,
    DetailViewAstro
} ;

typedef NS_ENUM(NSInteger, Template) {
    TemplateArticle=0,
    TemplateGallery
} ;

#define TemplateArticleKey @"articulo"
#define TemplateGalleryKey @"galeria"

#define DETAIL_MARGIN 10
/*
 #define DETAIL_TITLE_FONT [UIFont fontWithName:FONT_FUTURA_MEDIUM size:18]
 #define DETAIL_ENTRADILLA_FONT [UIFont fontWithName:FONT_HELVETICA_NEUE_BOLD size:14]
 #define DETAIL_TEXTO_FONT [UIFont fontWithName:FONT_HELVETICA_NEUE_ROMAN size:14]
 133 172
 */
#define DETAIL_IMAGE_DEFAULT_WIDTH 133
#define DETAIL_IMAGE_DEFAULT_HEIGHT 172


#define DETAIL_GALLERY_PHOTOS_IMAGE_NUMBER 4

#define DETAIL_GALLERY_PHOTOS_WIDTH (320-2*DETAIL_MARGIN)

#define DETAIL_GALLERY_IMAGE_WIDTH 74
#define DETAIL_GALLERY_IMAGE_HEIGHT 85
#define DETAIL_GALLERY_IMAGE_VERTICAL_MARGIN 10
#define DETAIL_GALLERY_IMAGE_HORIZONTAL_MARGIN 10
#define DETAIL_GALLERY_VIEW_HEIGHT (DETAIL_GALLERY_IMAGE_HEIGHT+DETAIL_GALLERY_IMAGE_VERTICAL_MARGIN)

#define MAX_LENGTH 400

#define DETAIL_ACT_INDICATOR_SIZE 30

//  Detail HTML
//-----------------------------------------------------

//Detalle HTML
#define DETAIL_UPPERCASE 1

//Fuentes
#define DETALLE_FUENTE_1 FONT_TITULO
#define DETALLE_FUENTE_2 FONT_ENTRADILLA
#define DETALLE_FUENTE_3 FONT_TEXTOS
#define DETALLE_FUENTE_4 FONT_TEXTOS

//Imagen
#define DETALLE_IMAGEN_WIDTH_IPHONE4  @"320px"
#define DETALLE_IMAGEN_WIDTH_IPHONE5  @"320px"
#define DETALLE_IMAGEN_WIDTH_IPHONE6  @"375px"
#define DETALLE_IMAGEN_WIDTH_IPHONE6P @"414px"

#define DETALLE_IMAGEN_TEXTO_WIDTH_IPHONE4  @"310px"
#define DETALLE_IMAGEN_TEXTO_WIDTH_IPHONE5  @"310px"
#define DETALLE_IMAGEN_TEXTO_WIDTH_IPHONE6  @"355px"
#define DETALLE_IMAGEN_TEXTO_WIDTH_IPHONE6P @"394px"

//Seccion
#define DETALLE_SECCION_SIZE_IPHONE4  @"14px"
#define DETALLE_SECCION_SIZE_IPHONE5  @"14px"
#define DETALLE_SECCION_SIZE_IPHONE6  @"15px"
#define DETALLE_SECCION_SIZE_IPHONE6P @"17px"

//Titulo
#define DETALLE_TITULO_SIZE_IPHONE4  @"20px"
#define DETALLE_TITULO_SIZE_IPHONE5  @"20px"
#define DETALLE_TITULO_SIZE_IPHONE6  @"22px"
#define DETALLE_TITULO_SIZE_IPHONE6P @"24px"

//Entradilla
#define DETALLE_ENTRADILLA_SIZE_IPHONE4  @"14px"
#define DETALLE_ENTRADILLA_SIZE_IPHONE5  @"14px"
#define DETALLE_ENTRADILLA_SIZE_IPHONE6  @"16px"
#define DETALLE_ENTRADILLA_SIZE_IPHONE6P @"18px"

//Texto
#define DETALLE_TEXTO_SIZE_IPHONE4  @"13px"
#define DETALLE_TEXTO_SIZE_IPHONE5  @"13px"
#define DETALLE_TEXTO_SIZE_IPHONE6  @"15px"
#define DETALLE_TEXTO_SIZE_IPHONE6P @"17px"

//  Astro
//-----------------------------------------------------

#define AVC_SCROLL_X 0
#define AVC_SCROLL_Y (TITLE_BAR_Y+TITLE_BAR_HEIGHT)
#define AVC_SCROLL_WIDTH 320
#define AVC_SCROLL_HEIGHT (460-AVC_SCROLL_Y-TOOLBAR_HEIGHT)

#define ASTRO_IMAGE_WIDTH 300
#define ASTRO_IMAGE_HEIGHT 117

//  Alerts
//-----------------------------------------------------

enum  {
    ALERT_PINTEREST_TAG = 0,
    ALERT_FAVOURITES_TAG,
    ALERT_FAVOURITES_DELETE_TAG
};

//  Comments
//-----------------------------------------------------

#define COMMENTS_VIEW_X 0
#define COMMENTS_VIEW_Y 0
#define COMMENTS_VIEW_WIDTH 320
#define COMMENTS_VIEW_HEIGHT 300

#define COMMENT_VIEW_Y_INIT (TITLE_BAR_Y+TITLE_BAR_HEIGHT+COMMENT_UP_MARGIN)

#define COMMENT_IMG_X (135)
#define COMMENT_IMG_Y COMMENT_VIEW_Y_INIT
#define COMMENT_IMG_WIDTH (39/2)
#define COMMENT_IMG_HEIGHT (33/2)

#define COMMENT_LEFT_MARGIN 10
#define COMMENT_UP_MARGIN 10

#define COMMENT_LABEL_WIDTH 300
#define COMMENT_LABEL_HEIGHT 20

#define COMMENT_FONT_INFO [UIFont fontWithName:FONT_HELVETICA_NEUE_BOLD size:13]
#define COMMENT_FONT_DATA [UIFont fontWithName:FONT_HELVELTICA_NEUE_MEDIUM size:12]
#define COMMENT_FONT_REQUIRED [UIFont fontWithName:FONT_HELVETICA_NEUE_BOLD size:10]

#define COMMENT_TF_X (80)
#define COMMENT_TF_WIDTH (320-COMMENT_TF_X-10)
#define COMMENT_TF_HEIGHT 15
#define COMMENT_TF_HEIGHT_LONG 120

#define COMMENT_BUT_X (310-COMMENT_BUT_WIDTH)
#define COMMENT_BUT_WIDTH 80
#define COMMENT_BUT_HEIGHT (25)

#define COMMENT_BUT_FONT [UIFont fontWithName:FONT_VECTORIAL_MEDIUM size:13]

#define CELL_COMMENT_MARGIN 10

#define CELL_COMMENT_BUT_X CELL_COMMENT_MARGIN
#define CELL_COMMENT_BUT_Y CELL_COMMENT_MARGIN
#define CELL_COMMENT_BUT_WIDTH 80
#define CELL_COMMENT_BUT_HEIGHT 17

#define CELL_COMMENT_FONT_BUT [UIFont fontWithName:FONT_HELVETICA_NEUE_BOLD size:13]
#define CELL_COMMENT_FONT_AUTHOR [UIFont fontWithName:FONT_HELVETICA_NEUE_BOLD size:14]
#define CELL_COMMENT_FONT_NUMBER [UIFont fontWithName:FONT_HELVETICA_NEUE_ROMAN size:14]
#define CELL_COMMENT_FONT_URL [UIFont fontWithName:FONT_HELVETICA_NEUE_ROMAN size:13]




#define DFP_TARGETING_DETALLE @{@"pos":@"detalle"}
#define DFP_TARGETING_GALERIA @{@"pos":@"galeria"}
#define DFP_TARGETING_INICIO  @{@"pos":@"inicio"}
#define DFP_TARGETING_LISTADO @{@"pos":@"listado"}

//  URLS SERVICIO
//-----------------------------------------------------

#define URL_PARTICIPACION       @"http://casting.telephonepublishing.es/CastingsService/NuevaParticipacion"
#define URL_FORMULARIOACCESO    @"http://casting.telephonepublishing.es/CastingsService/FormularioAcceso"
#define URL_CONFIRMACIONSMS     @"http://casting.telephonepublishing.es/CastingsService/ConfirmarverificacionSMS"
#define URL_VERIFICACIONTEL     @"http://casting.telephonepublishing.es/CastingsService/VerificacionTel"
#define URL_CONFIRMARSUBIDA     @"http://casting.telephonepublishing.es/CastingsService/ConfirmarSubidos"
#define URL_ENVIOSMS            @"http://casting.telephonepublishing.es/CastingsService/EnvioSMS"
#define URL_VERIFICACIONTELAUX  @"http://casting.telephonepublishing.es/CastingsService/VerificacionTelAux"
#define URL_SUBIDACONTENIDO     @"http://casting.telephonepublishing.es/CastingsService/SubidaContenido"

//TEST

//#define URL_PARTICIPACION       @"http://10.241.36.166:8080/Castings/NuevaParticipacion"
//#define URL_FORMULARIOACCESO    @"http://10.241.36.166:8080/Castings/FormularioAcceso"
//#define URL_CONFIRMACIONSMS     @"http://10.241.36.166:8080/Castings/ConfirmarverificacionSMS"
//#define URL_VERIFICACIONTEL     @"http://10.241.36.166:8080/Castings/VerificacionTel"
//#define URL_CONFIRMARSUBIDA     @"http://10.241.36.166:8080/Castings/ConfirmarSubidos"
//#define URL_ENVIOSMS            @"http://10.241.36.166:8080/Castings/EnvioSMS"
//#define URL_VERIFICACIONTELAUX  @"http://10.241.36.166:8080/Castings/VerificacionTelAux"
//#define URL_SUBIDACONTENIDO     @"http://10.241.36.166:8080/Castings/SubidaContenido"


#endif

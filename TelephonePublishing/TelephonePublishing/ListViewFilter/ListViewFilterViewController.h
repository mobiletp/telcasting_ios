//
//  ListViewFilterViewController.h
//  TelephonePublishing
//
//  Created by Tecnico IOS on 14/5/15.
//  Copyright (c) 2015 Hearst Magazine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HMDataManager.h"

@interface ListViewFilterViewController : UIViewController <UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource,HMDataManagerDelegate>
{
    NSString *type;
    NSString *url;
    UISearchBar *searchBar;
    BOOL isFiltered;
    
    //DataManager
    HMDataManager *_dataManager;
}


@property (strong, nonatomic) NSMutableArray* allTableData;
@property (strong, nonatomic) NSMutableArray* filteredTableData;
@property (strong, nonatomic) UITableView* tableViewData;

- (id)initWithType:(NSString *)typeData url:(NSString *)urlData;

@end
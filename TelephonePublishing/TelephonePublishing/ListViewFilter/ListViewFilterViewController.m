//
//  ListViewFilterViewController.m
//  TelephonePublishing
//
//  Created by Tecnico IOS on 14/5/15.
//  Copyright (c) 2015 Hearst Magazine. All rights reserved.
//

#import "ListViewFilterViewController.h"
#import "Recetas.h"
#import "Dreams.h"
#import "HMDataManager.h"
#import "TPAppDelegate.h"

@interface ListViewFilterViewController ()

@end

@implementation ListViewFilterViewController

@synthesize filteredTableData, allTableData, tableViewData;

- (id)initWithType:(NSString *)typeData url:(NSString *)urlData
{
    if (self = [super init])
    {
        type = typeData;
        url = urlData;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Reset variables
    isFiltered = NO;
    
    //Content
    [allTableData removeAllObjects];
    allTableData = [[NSMutableArray alloc] initWithCapacity:0];
    
    //Load data
    [self loadList:url];
    
    //Recargo tabla
    [tableViewData reloadData];
    
    //Inicializar vistas
    searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, 320, 40)];
    [searchBar setDelegate:self];
    [self.view addSubview:searchBar];
    tableViewData = [[UITableView alloc] initWithFrame:CGRectMake(0, searchBar.frame.size.height, 320, [[UIScreen mainScreen] bounds].size.height- 120 - searchBar.frame.size.height)];
    [tableViewData setDataSource:self];
    [tableViewData setDelegate:self];
    tableViewData.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero]; //Quitar lineas de decoracion sobrantes
    tableViewData.bounces = NO;
    [self.view addSubview:tableViewData];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)text
{
    if(text.length == 0)
    {
        isFiltered = FALSE;
    }
    else
    {
        isFiltered = true;
        filteredTableData = [[NSMutableArray alloc] init];
        if ([type  isEqual: @"recetas"])
        {
            for (Recetas* recetas in allTableData)
            {
                NSRange nameRange = [recetas.titulo rangeOfString:text options:NSCaseInsensitiveSearch];
                NSRange descriptionRange = [recetas.tipo rangeOfString:text options:NSCaseInsensitiveSearch];
                if(nameRange.location != NSNotFound || descriptionRange.location != NSNotFound)
                {
                    [filteredTableData addObject:recetas];
                }
            }
        }
        else if ([type  isEqual: @"dreams"]){
            for (Dreams* dreams in allTableData)
            {
                NSRange nameRange = [dreams.titulo rangeOfString:text options:NSCaseInsensitiveSearch];
                //NSRange descriptionRange = [dreams.texto rangeOfString:text options:NSCaseInsensitiveSearch];
                if(nameRange.location != NSNotFound)
                {
                    [filteredTableData addObject:dreams];
                }
            }
        }
        
    }
    
    [tableViewData reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger rowCount;
    if(isFiltered)
        rowCount = filteredTableData.count;
    else
        rowCount = allTableData.count;
    NSLog(@"Numero recetas/sueños: %ld", (long)rowCount);
    return rowCount;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    
    Recetas* recetas;
    Dreams* dreams;
    if([type  isEqual: @"recetas"]){
        if(isFiltered)
            recetas = [filteredTableData objectAtIndex:indexPath.row];
        else
            recetas = [allTableData objectAtIndex:indexPath.row];
        cell.textLabel.text = recetas.titulo;
        NSLog(@"Titulo receta: %@",recetas.idRecetas);
        
    }
    else if ([type  isEqual: @"dreams"]){
        if(isFiltered)
            dreams = [filteredTableData objectAtIndex:indexPath.row];
        else
            dreams = [allTableData objectAtIndex:indexPath.row];
        cell.textLabel.text = dreams.titulo;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([type  isEqual: @"recetas"])
    {
        if(isFiltered)
            [((TPAppDelegate*)[UIApplication sharedApplication].delegate) goDetail:@"recetas" array:filteredTableData idx:indexPath.row];
        else
            [((TPAppDelegate*)[UIApplication sharedApplication].delegate) goDetail:@"recetas" array:allTableData idx:indexPath.row];
    }
    else if([type  isEqual: @"dreams"])
    {
        if(isFiltered)
            [((TPAppDelegate*)[UIApplication sharedApplication].delegate) goDetail:@"dreams" array:filteredTableData idx:indexPath.row];
        else
            [((TPAppDelegate*)[UIApplication sharedApplication].delegate) goDetail:@"dreams" array:allTableData idx:indexPath.row];
    }
}

#pragma mark - Load data

-(void)loadList:(NSString *)url
{
    _dataManager = [[HMDataManager alloc] initHMDataManager];
    [_dataManager setDelegate:self];
    
    //Error flag
    //_isError=NO;
    
    //Loading state
    //_isLoading=YES;
    
    //conn
    [_dataManager loadData:url withSender:SENDER_FEED];
}

#pragma mark - DataManager delegate

- (void)dataManagerDidFinish:(HMDataManager *)dataManager
{
    //End loading
    //_isLoading=NO;
    
    [tableViewData reloadData];
}

- (void)dataManagerDidParse:(HMDataManager *)dataManager withObject:(NSObject *)object
{
    //Add object
    [allTableData addObject:object];
}

- (void)dataManagerDidFail:(HMDataManager *)dataManager withError:(NSError *)error
{
    //Fin estado carga
    //_isLoading=NO;
    
    //Estado error
    //_isError=YES;
    
    //Recargo tabla
}

@end

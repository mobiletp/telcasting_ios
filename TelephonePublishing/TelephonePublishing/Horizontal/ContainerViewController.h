//
//  ContainerViewController.h
//  Fotogramas
//
//  Created by Tecnico IOS on 20/10/14.
//
//

#import <UIKit/UIKit.h>
#import "sectionBar.h"
//#import "HMShareView.h"
//#import "HMToolBar.h"
//#import "SonataAdsDelegate.h"
#import "TPToolbarDown.h"
#import "TPToolbarLlamar.h"

@interface ContainerViewController : UIViewController <TPToolbarDownDelegate, TPToolbarLlamarDelegate, UIScrollViewDelegate/*,SonataAdsDelegate*/>{
    //Toolbar
    //HMToolBar *_toolBar;
    
    //SectionBar
    sectionBar *bar;
    
    //Share
    //HMShareView *_shareView;
    
    //Navigation Menu
    UINavigationBar *navbar;
    
    TPToolbarDown *_downToolbar;
    TPToolbarLlamar *_llamarToolbar;
    
}
@property (retain, nonatomic) UIScrollView *scrollView;
@property (nonatomic, assign, getter=isSubsection) BOOL subsection;
@property (nonatomic, assign) BOOL useToolbar;
@property (nonatomic, assign) BOOL isPelicula;
@property (nonatomic, assign) BOOL backButton;
@property (nonatomic, assign) BOOL shareButton;
@property (nonatomic, strong) NSArray *object;

@property (strong, nonatomic) TPToolbarDown* downToolbar;
@property (strong, nonatomic) TPToolbarLlamar* llamarToolbar;

@property (assign) BOOL mas;

//@property (retain, nonatomic) IBOutlet UINavigationItem *navBar;

- (id)initWithArray:(NSArray *)array nameArray:(NSArray *)names index:(int *)index usetoolbar:(BOOL)useToolbar shareButton:(BOOL)shareButton isPelicula:(BOOL)isPelicula backButton:(BOOL)backButton object:(NSArray *)object preload:(BOOL *)preload;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil initWithArray:(NSArray *)array nameArray:(NSArray *)names index:(int *)index preload:(BOOL *)preload usetoolbar:(BOOL)useToolbar shareButton:(BOOL)shareButton;

- (void)setIndex:(int)index;
- (NSInteger)getIndex;
- (BOOL)getTipoPelicula;
-(void)callPubli;

@end



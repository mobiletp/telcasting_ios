//
//  ContainerViewController.m
//  Fotogramas
//
//  Created by Tecnico IOS on 20/10/14.
//
//

#import "ContainerViewController.h"
#import "SWRevealViewController.h"
#import "TPAppDelegate.h"
#import "sectionBar.h"
#import "TPConstants.h"
#import "HMGeneral.h"


@interface ContainerViewController ()

@property (strong, nonatomic) NSArray *viewControllers;
@property (strong, nonatomic) NSArray *viewControllersName;
@property (nonatomic, assign) int totalPages;
@property (nonatomic, assign) int idx;
@property (nonatomic, assign) BOOL preload;

@end

@implementation ContainerViewController{
    //SonataAds *_ads;
}

- (id)initWithArray:(NSArray *)array nameArray:(NSArray *)names index:(int *)index usetoolbar:(BOOL)useToolbar shareButton:(BOOL)shareButton isPelicula:(BOOL)isPelicula backButton:(BOOL)backButton object:(NSArray *)object preload:(BOOL *)preload
{
    self = [super self];
    if (self) {
        self.viewControllers = array;
        self.viewControllersName = names;
        self.idx = index;
        self.subsection = NO;
        self.useToolbar = useToolbar;
        self.isPelicula = isPelicula;
        self.backButton = backButton;
        self.object = object;
        self.shareButton = shareButton;
        self.preload = preload;
    }
    return self;
}




- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil initWithArray:(NSArray *)array nameArray:(NSArray *)names index:(int *)index preload:(BOOL *)preload usetoolbar:(BOOL)useToolbar shareButton:(BOOL)shareButton

{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.viewControllers = array;
        self.idx = index;
        self.preload = preload;
        self.subsection = NO;
        self.viewControllersName = names;
        self.useToolbar = useToolbar;

        self.shareButton = shareButton;

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.mas = NO;
    
    //Navigationbar
    self.view.backgroundColor = [UIColor whiteColor];
    SWRevealViewController *revealController = self.revealViewController;
    navbar = [[UINavigationBar alloc]initWithFrame:CGRectMake(0, 20, self.view.frame.size.width, 50)];
    UIImage *image = [UIImage imageNamed: @"cabecera.png"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage: image];
    imageView.frame = CGRectMake(75, 0, 170, 40);
    [navbar addSubview:imageView];
    //do something like background color, title, etc you self
    [self.view addSubview:navbar];
    navbar.translucent = NO;
    UIButton *menubtn = [UIButton buttonWithType:UIButtonTypeCustom];
    menubtn.frame = CGRectMake(10, 3, 60, 30);
    [menubtn setTitle:NSLocalizedString(@"_BUT_MENU", @"") forState:UIControlStateNormal];
    [menubtn.titleLabel setFont:[UIFont boldSystemFontOfSize:10.0]];
    [menubtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    //[menubtn setBackgroundImage:[UIImage imageNamed:@"ico_menu_white@2x.png"] forState:UIControlStateNormal];
    UIImage *btnMenu = [self imageWithImage:[UIImage imageNamed:@"ico_menu_white@2x.png"] scaledToSize:CGSizeMake(30, 35)];
    [menubtn setImage:btnMenu forState:UIControlStateNormal];
    [menubtn setTitleEdgeInsets:UIEdgeInsetsMake(0.0, -3.0, -6.0, 0.0)];
    [menubtn setImageEdgeInsets:UIEdgeInsetsMake(0.0, -25.0, 0.0, 0.0)]; //donde esta el -25 se pone en -8 si hay texto
    [menubtn addTarget:revealController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    menubtn.tintColor = [UIColor blackColor];
    [navbar addSubview:menubtn];//set new button
    if(_backButton){
    UIButton *menuback = [UIButton buttonWithType:UIButtonTypeCustom];
    menuback.frame = CGRectMake(self.view.frame.size.width-60, 5, 60, 30);
    //[menuback setBackgroundImage:[UIImage imageNamed:@"ico_navbar_back@2x.png"] forState:UIControlStateNormal];
    UIImage *btnImage = [self imageWithImage:[UIImage imageNamed:@"ico_navbar_back@2x.png"] scaledToSize:CGSizeMake(12, 12)];
    [menuback setTitle:NSLocalizedString(@"_BUT_BACK", @"") forState:UIControlStateNormal];
    [menuback setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    //menuback.titleLabel.textColor = [UIColor blackColor];
    [menuback.titleLabel setFont:[UIFont boldSystemFontOfSize:10.0]];
    [menuback setTitleEdgeInsets:UIEdgeInsetsMake(0.0, -btnImage.size.width, -2.0, 0.0)];
    [menuback setImage:btnImage forState:UIControlStateNormal];
    [menuback setImageEdgeInsets:UIEdgeInsetsMake(0.0, -btnImage.size.width-10, -2.0, 0.0)];
    [menuback addTarget:self action:@selector(backButton:) forControlEvents:UIControlEventTouchUpInside];
    menuback.tintColor = [UIColor blackColor];
    //[navbar addSubview:menuback];//set new button
    }
    
    /*[self.navBar setRightBarButtonItem:[[UIBarButtonItem alloc] initWithImage:[self resizeImage:[UIImage imageNamed:@"ico_navbar_menu_articles@2x.png"] newSize:CGSizeMake(20,20)] style:UIBarButtonItemStyleBordered target:revealController action:@selector(rightRevealToggle:)]];*/
    //Title bar
    if (!_useToolbar){
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, navbar.frame.origin.y + navbar.frame.size.height + 7, 320, self.view.frame.size.height - navbar.frame.origin.y - navbar.frame.size.height - 7)];
        _scrollView.contentSize=CGSizeMake(320,self.view.frame.size.height-navbar.frame.size.height - 5);
    }else{
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, navbar.frame.origin.y + navbar.frame.size.height + 7, 320, self.view.frame.size.height - navbar.frame.origin.y - navbar.frame.size.height - 35)];
        _scrollView.contentSize=CGSizeMake(320,self.view.frame.size.height-navbar.frame.size.height - 33 -TOOLBAR_HEIGHT);
    }
    _scrollView.showsHorizontalScrollIndicator = YES;
    [self.view addSubview:_scrollView];
    bar = [[sectionBar alloc] initWithFrame:CGRectMake(0, 70, 320, 20)];
    bar.backgroundColor = [UIColor blackColor];
    bar.frame = CGRectMake(bar.frame.origin.x ,bar.frame.origin.y-6, bar.frame.size.width, bar.frame.size.height);
    bar.label1.text = [[self setSectionTitle:_idx] uppercaseString];
    [self.view addSubview:bar];
    
    self.totalPages = self.viewControllers.count;
    [self setUpScrollView];
    if(_preload){
        [self loadScrollViewWithPage:_idx];
        for (int i = 0; i<self.viewControllers.count; i++) {
            [self loadScrollViewWithPage:i];
        }
    }else{
        //for (int i=_idx-1; i>0 && i<=_idx+1; i++) {
        if(_shareButton){
            //[self loadScrollViewWithPage:_idx-1];
            [self loadScrollViewWithPage:_idx];
            //[self loadScrollViewWithPage:_idx+1];

        }else{
            [self loadScrollViewWithPage:_idx];
        }
        
        //}
        /*if (_idx == 0) {
            [self loadScrollViewWithPage:0];
        }*/
    }
    
    //toolbar
    /*if (self.useToolbar == YES){
        if(_shareButton){
            _toolBar = [[HMToolBar alloc] init];
            [_toolBar setDelegate:self];
            _toolBar.frame = CGRectMake(0, self.view.frame.size.height-TOOLBAR_HEIGHT, self.view.frame.size.width, TOOLBAR_HEIGHT);
            [self.view addSubview:_toolBar];
        
            //Share
            _shareView = [[HMShareView alloc] init];
            [_shareView setDelegate:self];
            [self.view addSubview:_shareView];
            [_shareView setHidden:YES];
            
        }else
        {
            _toolBar = [[HMToolBar alloc] initWithShareHidden:YES];
            [_toolBar setDelegate:self];
            _toolBar.frame = CGRectMake(0, self.view.frame.size.height-TOOLBAR_HEIGHT, self.view.frame.size.width, TOOLBAR_HEIGHT);
            [self.view addSubview:_toolBar];
            
            
        }
        
        
    }*/
    
    //Toolbar Quieres Saber Mas
    _downToolbar = [[TPToolbarDown alloc] init];
    [_downToolbar setDelegate:self];
    [self.view addSubview:_downToolbar];
    
    //Toolbar Llamar
    _llamarToolbar = [[TPToolbarLlamar alloc] init];
    [_llamarToolbar setDelegate:self];
    [self.view addSubview:_llamarToolbar];
    
    //Posicion inicial de la barra de herramientas saber mas
    int y=[[UIScreen mainScreen] applicationFrame].size.height-TOOLBAR_HEIGHT+20;
    
    _downToolbar.frame = CGRectMake(_downToolbar.frame.origin.x, y, _downToolbar.frame.size.width, _downToolbar.frame.size.height);
    
    //Posicion inicial de la barra de herramientas llamar
    y=[[UIScreen mainScreen] applicationFrame].size.height-TOOLBAR_HEIGHT+20;
    _llamarToolbar.frame = CGRectMake(_downToolbar.frame.origin.x, y, _downToolbar.frame.size.width, _downToolbar.frame.size.height);
    self.llamarToolbar.hidden = YES;
    
    [self.scrollView setContentOffset:CGPointMake(self.scrollView.frame.size.width*_idx, 0.0f) animated:YES];
    //[self loadAds];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
}

- (void) setIndex:(int)index{
    self.idx = index;
    [self.scrollView setContentOffset:CGPointMake(self.scrollView.frame.size.width*_idx, 0.0f) animated:NO];
    bar.label1.text = [self setSectionTitle:_idx];
    UIViewController *uivc = [self.viewControllers objectAtIndex:index];
    /*if ([[self.viewControllers objectAtIndex:index] isKindOfClass:[HMListViewController class]]){
        [(HMListViewController *)uivc reloadData];
    }
    if ([[self.viewControllers objectAtIndex:index] isKindOfClass:[HMDetailViewController class]]){
        [(HMDetailViewController *)uivc reloadData];
    }*/

}

- (void)setUpScrollView {
    self.scrollView.pagingEnabled = YES;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.showsVerticalScrollIndicator = NO;
    self.scrollView.delegate = self;
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width * self.totalPages, self.scrollView.frame.size.height);
}

- (void)loadScrollViewWithPage:(int)page {
    if (page < 0 || page >= self.totalPages) {
        return;
    }
    //[((FotogramasAppDelegate*)[[UIApplication sharedApplication] delegate])setIndexDetailShare:page];
    UIViewController *uivc = [self.viewControllers objectAtIndex:page];
    if (uivc.view.superview == nil) {
        CGRect frame = self.scrollView.frame;
        frame.origin.x = frame.size.width * (page);
        frame.origin.y = 4;
        uivc.view.frame = frame;
        [self.scrollView addSubview:uivc.view];
    }
}

-(NSString *)setSectionTitle:(int)index
{
    NSString *title = @"";
    if (_subsection){
        return title;
    }
    else{
        if(index < self.viewControllersName.count){
            title = [self.viewControllersName objectAtIndex:index];
        }
    }
    return title;
}

/*-(void)setSubsectionTitle:(NSString *)title
{
    bar.label1.text = _title;tecnic
}*/

- (void)tilePages
{
    /*if(!_isPrincipal){
        for ( int i = 0; i < _totalPages; i++ ) {
            if ( (i < (_idx-1) || i > (_idx+1)) && [self.scrollView viewWithTag:(i+1)] ) {
                //[[self.scrollView viewWithTag:(i+1)] removeFromSuperview];
                //NSLog(@"Limpiando la casa");
            }
        }
    }*/
}

#pragma mark UIScrollViewDelegateMethods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
        //[[NSURLCache sharedURLCache] removeAllCachedResponses];
        NSNotification *note = [NSNotification notificationWithName:@"dismissKeyboard" object:nil];
        [[NSNotificationCenter defaultCenter]postNotification:note];
    
        CGFloat pageWidth = self.scrollView.frame.size.width;
        int page = floor((self.scrollView.contentOffset.x - pageWidth/2) / pageWidth) + 1;
        if(_shareButton){
            //[self loadScrollViewWithPage:page-1];
            [self loadScrollViewWithPage:page];
            //[self loadScrollViewWithPage:page+1];
        }else{
            [self loadScrollViewWithPage:page];
        }
    
        if (_idx != page){
            if (_shareButton){
                if (page > self.viewControllers.count -1){
                    page = 0;
                }
                else if(page < 0){
                    page = 0;
                }
                //[((FotogramasAppDelegate*)[[UIApplication sharedApplication] delegate])getDetail:page];
            }
            else{
                if (page > self.viewControllers.count -1){
                    page = 0;
                }
                else if(page < 0){
                    page = 0;
                }
                //[((FotogramasAppDelegate*)[[UIApplication sharedApplication] delegate])firesComscoreHorizontal:page controller:self comScoreView:@"" postTitle:@"" isVideo:NO];
            }
            //[self loadAds];
            /*if ([[self.viewControllers objectAtIndex:page] isKindOfClass:[HMListViewController class]]){
                NSLog(@"Reload View");
                UIViewController *uivc = [self.viewControllers objectAtIndex:page];
                //[(HMListViewController *)uivc reloadData];
            }
            if ([[self.viewControllers objectAtIndex:page] isKindOfClass:[HMDetailViewController class]]){
                NSLog(@"Reload View");
                UIViewController *uivc = [self.viewControllers objectAtIndex:page];
                [(HMDetailViewController *)uivc reloadData];
            }*/
        }
        _idx = page;
        if (_idx < self.viewControllers.count || _idx > 0){
            //[((FotogramasAppDelegate*)[[UIApplication sharedApplication] delegate])setIndexDetailShare:_idx];
        }
    
        //[self loadScrollViewWithPage:page - 1];
    
        //[self loadScrollViewWithPage:page + 1];

        bar.label1.text = [self setSectionTitle:page];

}

#pragma mark - Toolbar delegate

//-(void)toolBarDelegateBack:(HMToolBar *)toolBar
//{
    /*if(_idx>0)
    {
        _idx--;
        
        CGRect frame = _scrollView.frame;
        frame.origin.x = frame.size.width * _idx;
        frame.origin.y = 0;
        [_scrollView scrollRectToVisible:frame animated:NO];
        [((FotogramasAppDelegate*)[[UIApplication sharedApplication] delegate])setIndexDetailShare:_idx];
        [((FotogramasAppDelegate*)[[UIApplication sharedApplication] delegate])firesComscoreHorizontal:_idx controller:self];
    }*/
    //[((FotogramasAppDelegate*)[[UIApplication sharedApplication] delegate]) selectBackSection];
//}

//-(void)toolBarDelegateFoward:(HMToolBar *)toolBar
//{
    /*if(_idx<_totalPages-1)
    {
        _idx++;
        
        CGRect frame = _scrollView.frame;
        frame.origin.x = frame.size.width * _idx;
        frame.origin.y = 0;
        [_scrollView scrollRectToVisible:frame animated:NO];
        [((FotogramasAppDelegate*)[[UIApplication sharedApplication] delegate])setIndexDetailShare:_idx];
        [((FotogramasAppDelegate*)[[UIApplication sharedApplication] delegate])firesComscoreHorizontal:_idx controller:self];
    }*/
//}

//-(void)toolBarDelegateShare:(HMToolBar *)toolBar
/*{
    //    //Activity
    //    [[HMGeneral sharedInstance] shareActivity:[_contentArray objectAtIndex:_currentPage] inViewController:self];
    //
    //    //Refresh
    //    [_toolBar resetToolbar];
    
    //View controller
    [_shareView setViewController:self];
    
    [self shareEvent];
    
    
    //Frame
    NSInteger x = 320-SHARE_VIEW_WIDTH-10;
    if(![[HMGeneral sharedInstance] hasFavoriteSection]) x = 20;
    _shareView.frame = CGRectMake(x, _toolBar.frame.origin.y-_shareView.frame.size.height, _shareView.frame.size.width, _shareView.frame.size.height);
    
    //Bring to front
    [self.view bringSubviewToFront:_shareView];
    
    //Share view hide or show
    [_shareView setHidden:![_shareView isHidden]];
}

-(void)shareEvent{
    if(_object != nil){
        
        [_shareView setSharedObject:[_object objectAtIndex:_idx]];
    }
    else{
        NSArray *temparray = [((FotogramasAppDelegate*)[[UIApplication sharedApplication] delegate])_objectShare];
        //NSObject *temp = [temparray objectAtIndex:0];
        Itemactualidad *temp = [temparray objectAtIndex:_idx];
        [_shareView setSharedObject:temp];
    }
}
*/
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

/*- (UIImage *)resizeImage:(UIImage*)image newSize:(CGSize)newSize {
    CGRect newRect = CGRectIntegral(CGRectMake(0, 0, newSize.width, newSize.height));
    CGImageRef imageRef = image.CGImage;
    
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetInterpolationQuality(context, kCGInterpolationHigh);
    CGAffineTransform flipVertical = CGAffineTransformMake(1, 0, 0, -1, 0, newSize.height);
    
    CGContextConcatCTM(context, flipVertical);
    CGContextDrawImage(context, newRect, imageRef);
    
    CGImageRef newImageRef = CGBitmapContextCreateImage(context);
    UIImage *newImage = [UIImage imageWithCGImage:newImageRef];
    
    CGImageRelease(newImageRef);
    UIGraphicsEndImageContext();
    
    return newImage;
}*/

-(BOOL)shouldAutorotate
{
    return YES;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

-(void)backButton:(UIButton *)sender
{
    //[((FotogramasAppDelegate*)[[UIApplication sharedApplication] delegate]) selectBackSection];
}

-(NSInteger)getIndex{
    if (_idx > 0 && _idx < 1000)
        return _idx;
    else
        return 0;
}

- (BOOL)getTipoPelicula{
    return _isPelicula;
}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    //UIGraphicsBeginImageContext(newSize);
    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
    // Pass 1.0 to force exact pixel size.
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

-(void)toolbarDownDelegateMas:(TPToolbarDown *)downToolbar
{
    //LLama a la tool bar llamar y desplaza esta barra hacia arriba
    if(self.mas){
        NSInteger y=[[UIScreen mainScreen] applicationFrame].size.height-TOOLBAR_HEIGHT+20;
        _downToolbar.frame = CGRectMake(_downToolbar.frame.origin.x, y, _downToolbar.frame.size.width, _downToolbar.frame.size.height);
        self.mas = NO;
        self.llamarToolbar.hidden = YES;
        _scrollView.frame = CGRectMake(_scrollView.frame.origin.x, _scrollView.frame.origin.y, _scrollView.frame.size.width, _scrollView.frame.size.height-TOOLBAR_HEIGHT);
    }
    else{
        NSInteger y=[[UIScreen mainScreen] applicationFrame].size.height-(TOOLBAR_HEIGHT*2)+20;
        _downToolbar.frame = CGRectMake(_downToolbar.frame.origin.x, y, _downToolbar.frame.size.width, _downToolbar.frame.size.height);
        self.mas = YES;
        self.llamarToolbar.hidden = NO;
        _scrollView.frame = CGRectMake(_scrollView.frame.origin.x, _scrollView.frame.origin.y, _scrollView.frame.size.width, _scrollView.frame.size.height+TOOLBAR_HEIGHT);
    }

}

-(void)toolbarDownDelegateLlamar:(TPToolbarLlamar *)llamarToolbar
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel:806402759"]]; //Verificar si llama a la aplicacion de marcado
}

#pragma mark - Ads

/*-(void)loadSonata
{
    DLog();
    
    //Banner
    SonataConfig *config = [[SonataConfig alloc] init];
    config.bannerInitialPosition = CGPointMake(0, self.view.frame.size.height);
    config.bannerFinalPosition = CGPointMake(0, self.view.frame.size.height-config.bannerHeight);
    
    //Load ad
    _ads = [[SonataAds alloc] initWithSectionName:[[[HMGeneral sharedInstance] currentSection] advertiseKey] viewContainer:self.view config:config];
    [_ads setDelegate:self];
    [_ads showAds];
    
    //Config release
    //[config release];
}*/

/*-(void)loadBanner
{
    DLog();
    
    SonataConfig *config = [[SonataConfig alloc] init];
    config.bannerInitialPosition = CGPointMake(0, self.view.frame.size.height);
    config.bannerFinalPosition = CGPointMake(0, self.view.frame.size.height-config.bannerHeight);
    
    //Load ad
    _ads = [[SonataAds alloc] initWithSectionName:[[[HMGeneral sharedInstance] currentSection] advertiseKey] viewContainer:self.view config:config];
    [_ads setDelegate:self];
    [_ads showAds];
    
    //Config release
    //[config release];
}*/

/*-(void)hideAds
{
    
    if([[HMGeneral sharedInstance] adType]==SectionAdTypeTapTap)
    {
        //Remove previous
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(loadAds) object:nil];
        
        //Cancel
        [_ads cancel];
    }
}*/

/*- (void)loadAds
{
    //Ad
    /*if([[self.viewControllers objectAtIndex:_idx] isKindOfClass:[Itemactualidad class]] && [(Itemactualidad *)[self.viewControllers objectAtIndex:_idx] branded])
    {
        DLog(@"Do not show PremiumAds in NativeAds detail");
    }
    else
    {
        [self loadBanner];
    }*/
    //[self loadBanner];
//}

/*-(void)removeAds
{
    //Toolbar in init position
    
    if([[HMGeneral sharedInstance] adType]==SectionAdTypeTapTap)
    {
        //Remove previous
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(loadAds) object:nil];
        
        //Cancel
        [_ads cancel];
    }
}

#pragma mark - SonataAdsDelegate methods

- (void)SonataAdsDidFinish:(SonataAds*)ads withStatus:(SonataAdsStatus)status
{
    DLog(@"%@", ads);
}

- (void)SonataAdsDidCancel:(SonataAds*)ads
{
    DLog(@"%@", ads);
}

- (void)SonataAds:(SonataAds*)ads didFailWithStatus:(SonataAdsStatus)status
{
    DLog(@"%@", ads);
}

-(void)callPubli
{
    [self loadAds];
}

/*- (void)dealloc {
    [super dealloc];
    if (!_isPrincipal){
        //[self.viewControllers release];
    }
}*/

@end

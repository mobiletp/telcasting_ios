//
//  sectionBar.m
//  Fotogramas
//
//  Created by Tecnico IOS on 01/12/14.
//
//

#import "sectionBar.h"
#import "TPConstants.h"

@implementation sectionBar

@synthesize label1 = _label1;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _label1 = [[UILabel alloc] initWithFrame:CGRectMake(5, 1, TITLE_LABEL_WIDTH, TITLE_LABEL_HEIGHT)];
        _label1.textAlignment = NSTextAlignmentLeft;
        _label1.textColor = [UIColor whiteColor];
      
        _label1.font=TITLE_BAR_FONT;
 
        _label1.text = @"" ;
   
        
        [self addSubview:_label1];
    }
    
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end

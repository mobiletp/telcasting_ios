//
//  Item.h
//  Elle
//
//  Created by Julio Rivas on 29/08/12.
//  Copyright (c) 2012 TAPTAP Networks. All rights reserved.
//

#import <Foundation/Foundation.h>

@class NRecetas;

@interface Recetas : NSObject
{
    NSString *idRecetas;
    NSString *titulo;
    NSString *tipo;
    NSString *texto;
}

-(id)initRecetas;

@property (nonatomic,retain) NSString *idRecetas;
@property (nonatomic,retain) NSString *titulo;
@property (nonatomic,retain) NSString *tipo;
@property (nonatomic,retain) NSString *texto;

@end

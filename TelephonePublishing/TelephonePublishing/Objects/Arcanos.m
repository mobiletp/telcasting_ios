//
//  Item.m
//  Elle
//
//  Created by Julio Rivas on 29/08/12.
//  Copyright (c) 2012 TAPTAP Networks. All rights reserved.
//

#import "Arcanos.h"

@implementation Arcanos

@synthesize idArcano;
@synthesize titulo;
@synthesize cartas;
@synthesize texto;

-(id)initArcanos
{
    if([self init])
    {
        
    }
    return self;
}

-(void)dealloc
{
    idArcano=nil;
    titulo=nil;
    cartas=nil;
    texto=nil;
}

@end

//
//  Item.h
//  Elle
//
//  Created by Julio Rivas on 29/08/12.
//  Copyright (c) 2012 TAPTAP Networks. All rights reserved.
//

#import <Foundation/Foundation.h>

@class NDreams;

@interface Dreams : NSObject
{
    NSString *idDreams;
    NSString *titulo;
    NSString *texto;
}

-(id)initDreams;

@property (nonatomic,retain) NSString *idDreams;
@property (nonatomic,retain) NSString *titulo;
@property (nonatomic,retain) NSString *texto;

@end

//
//  Item.h
//  Elle
//
//  Created by Julio Rivas on 29/08/12.
//  Copyright (c) 2012 TAPTAP Networks. All rights reserved.
//

#import <Foundation/Foundation.h>

@class NHoroscopoMes;

@interface HoroscopoMes : NSObject
{
    NSString *idHoroscopo;
    NSString *signo;
    NSString *texto;
    NSString *amor;
    NSString *suerte;
    NSString *trabajo;
}

-(id)initHoroscopoMes;

@property (nonatomic,retain) NSString *idHoroscopo;
@property (nonatomic,retain) NSString *signo;
@property (nonatomic,retain) NSString *texto;
@property (nonatomic,retain) NSString *amor;
@property (nonatomic,retain) NSString *suerte;
@property (nonatomic,retain) NSString *trabajo;

@end

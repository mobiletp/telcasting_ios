//
//  Item.m
//  Elle
//
//  Created by Julio Rivas on 29/08/12.
//  Copyright (c) 2012 TAPTAP Networks. All rights reserved.
//

#import "Dreams.h"

@implementation Dreams

@synthesize idDreams;
@synthesize titulo;
@synthesize texto;

-(id)initDreams
{
    if([self init])
    {
        
    }
    return self;
}

-(void)dealloc
{
    idDreams=nil;
    titulo=nil;
    texto=nil;
}

@end

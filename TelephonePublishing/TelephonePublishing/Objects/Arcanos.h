//
//  Item.h
//  Elle
//
//  Created by Julio Rivas on 29/08/12.
//  Copyright (c) 2012 TAPTAP Networks. All rights reserved.
//

#import <Foundation/Foundation.h>

@class NRecetas;

@interface Arcanos : NSObject
{
    NSString *idArcano;
    NSString *titulo;
    NSString *cartas;
    NSString *texto;
}

-(id)initArcanos;

@property (nonatomic,retain) NSString *idArcano;
@property (nonatomic,retain) NSString *titulo;
@property (nonatomic,retain) NSString *cartas;
@property (nonatomic,retain) NSString *texto;

@end

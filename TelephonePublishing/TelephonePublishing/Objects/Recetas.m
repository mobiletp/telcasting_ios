//
//  Item.m
//  Elle
//
//  Created by Julio Rivas on 29/08/12.
//  Copyright (c) 2012 TAPTAP Networks. All rights reserved.
//

#import "Recetas.h"

@implementation Recetas

@synthesize idRecetas;
@synthesize titulo;
@synthesize tipo;
@synthesize texto;

-(id)initRecetas
{
    if([self init])
    {
        
    }
    return self;
}

-(void)dealloc
{
    idRecetas=nil;
    titulo=nil;
    tipo=nil;
    texto=nil;
}

@end

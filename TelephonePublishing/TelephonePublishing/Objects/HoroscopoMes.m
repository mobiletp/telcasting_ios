//
//  Item.m
//  Elle
//
//  Created by Julio Rivas on 29/08/12.
//  Copyright (c) 2012 TAPTAP Networks. All rights reserved.
//

#import "HoroscopoMes.h"

@implementation HoroscopoMes

@synthesize idHoroscopo;
@synthesize signo;
@synthesize texto;
@synthesize amor;
@synthesize suerte;
@synthesize trabajo;

-(id)initHoroscopoMes
{
    if([self init])
    {
        
    }
    return self;
}

-(void)dealloc
{
    idHoroscopo=nil;
    signo=nil;
    texto=nil;
    amor=nil;
    suerte=nil;
    trabajo=nil;
}

@end

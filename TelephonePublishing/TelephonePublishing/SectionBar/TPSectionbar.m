//
//  TPToolBarDown.m
//  TelephonePublishing
//
//  Created by Tpbeca tec on 17/09/14.
//  Copyright (c) 2014 Hearst Magazine. All rights reserved.
//

#import "TPSectionbar.h"
#import "TPConstants.h"
#import "TPUtils.h"

@implementation TPSectionbar

@synthesize labelSection=_labelSection;

#pragma mark - Button methods



#pragma mark - Init

-(instancetype)init
{
    if (self = [super initWithFrame:CGRectMake(TABBAR_X, [[UIScreen mainScreen] applicationFrame].size.height-SECTIONBAR_HEIGHT, SECTIONBAR_WIDTH, SECTIONBAR_HEIGHT)])
	{
        //color fondo
        [self setBackgroundColor:[UIColor purpleColor]]; //para fondos poner clearcolor
        //background
        
        //Variables
        NSInteger totalButtons=1;
        NSInteger ajusteIzquierdo=5;
        NSInteger width=(SECTIONBAR_WIDTH/totalButtons) - ajusteIzquierdo;
        NSInteger x = 0*width;
        
        //Label Section
        x = ajusteIzquierdo;
        _labelSection = [TPUtils labelCreate:x :0 :width :SECTIONBAR_HEIGHT :nil :nil :1]; //TODO cambiar a constante
        _labelSection.text = @"Mi Perfil";
        //[_labelSection addTarget:self action:nil forControlEvents:UIControlEventTouchUpInside];
        [_labelSection setBackgroundColor:[UIColor purpleColor]];
        [self addSubview:_labelSection];
        
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end

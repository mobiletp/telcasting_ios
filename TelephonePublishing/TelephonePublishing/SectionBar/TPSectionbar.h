//
//  TPToolBarDown.h
//  TelephonePublishing
//
//  Created by Tpbeca tec on 17/09/14.
//  Copyright (c) 2014 Hearst Magazine. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TPSectionbar;

@protocol TPSectionbarDelegate <NSObject>
-(void)SectionbarDelegateMas:(TPSectionbar *)Sectionbar;
@end

@interface TPSectionbar : UIView
{
    //Fondo
    
    //Botones
    UILabel *_labelSection;
}

- (instancetype) init NS_DESIGNATED_INITIALIZER;

@property (assign) id<TPSectionbarDelegate> delegate;
@property (nonatomic,retain) UILabel *labelSection;

@end


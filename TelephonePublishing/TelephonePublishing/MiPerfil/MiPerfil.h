//
//  MiPerfil.h
//  TelephonePublishing
//
//  Created by Tpbeca tec on 01/10/14.
//  Copyright (c) 2014 Hearst Magazine. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MiPerfil;

@protocol MiPerfilDelegate <NSObject>
-(void)customViewDidPerformAction:(MiPerfil*)miPerfil;
-(void)alertViewDidPerformAction:(MiPerfil*)miPerfil;
-(IBAction)botonFoto:(id)sender;

@end

@interface MiPerfil : UIView<UITableViewDelegate, UITableViewDataSource>{
    
    //UITableView *_tableview;
    UIImage *_foto;
    NSData *_fotoPerfil;
    UIButton *_fotoButton;
    UIImageView *_fotoView;
    UIAlertView* dialog;
    NSDate *_fecha;
    NSString* _nombre;
    __weak id<MiPerfilDelegate> _delegate;
}

@property (nonatomic, retain, readonly) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, retain, readonly) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (nonatomic, strong) UITableView* tableView;
@property (retain, nonatomic) NSDate *fecha;

@property (nonatomic,weak) id<MiPerfilDelegate> delegate;

@property (nonatomic,strong)NSArray* fetchedRecordsArray;

-(instancetype)initWithPerfil:(NSString*)nombre fecha:(NSString*)fecha NS_DESIGNATED_INITIALIZER;
-(void)saveData:(NSString *)nombre fecha:(NSDate *)fecha tipo:(NSString *)tipo foto:(NSData *)foto;
-(void)updateDatosPerfil;
//Delegate
-(void)action;
-(void)alertViewRefresh;
-(void)changeImage:(UIImage*)newImage;

-(IBAction)botonFoto:(id)sender;

@end


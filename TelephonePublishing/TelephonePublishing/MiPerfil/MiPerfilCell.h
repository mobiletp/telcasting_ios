//
//  MiPerfilCell.h
//  TelephonePublishing
//
//  Created by Tecnico IOS on 28/10/14.
//  Copyright (c) 2014 Hearst Magazine. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MiPerfilCell : UITableViewCell{
    UIImageView *fotoView;
}

@property (nonatomic, strong) UIView *view;
@property (nonatomic, strong) UIImage *foto;
@property (nonatomic, retain) UIImageView *fotoView;
@property (nonatomic, strong) UILabel *lNombre;
@property (nonatomic, strong) UILabel *lDate;

@end

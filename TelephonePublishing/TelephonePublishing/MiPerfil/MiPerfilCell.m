//
//  MiPerfilCell.m
//  TelephonePublishing
//
//  Created by Tecnico IOS on 28/10/14.
//  Copyright (c) 2014 Hearst Magazine. All rights reserved.
//

#import "MiPerfilCell.h"

@implementation MiPerfilCell

@synthesize view;
@synthesize foto;
@synthesize lNombre;
@synthesize lDate;
@synthesize fotoView;

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        view = [[UIView alloc] initWithFrame:self.frame];
        [self addSubview:view];
        foto = [UIImage imageNamed:@"fotoVacia.png"];
        fotoView = [[UIImageView alloc] initWithImage:foto];
        //Punto de creacion de la imagen y tamaño
        fotoView.frame = CGRectMake(10, 10, 40, 40);
        //Punto de creacion de los label Nombre y Fecha de nacimiento
        lNombre = [[UILabel alloc] initWithFrame:CGRectMake(60,10,150,20)];
        lDate = [[UILabel alloc] initWithFrame:CGRectMake(60,30,150,20)];
        [view addSubview:lNombre];
        [view addSubview:lDate];
    }
    return self;
}

@end

//
//  TPDatePicker.m
//  TelephonePublishing
//
//  Created by Tecnico IOS on 29/10/14.
//  Copyright (c) 2014 Hearst Magazine. All rights reserved.
//

#import "TPDatePicker.h"

#define MyDateTimePickerPickerHeight 108
#define MyDateTimePickerToolbarHeight 40

@interface TPDatePicker()

@property (nonatomic, assign, readwrite) UIDatePicker *picker;
@property (nonatomic, assign) CGRect originalFrame;

@property (nonatomic, assign) id doneTarget;
@property (nonatomic, assign) SEL doneSelector;

- (void) donePressed;

@end

@implementation TPDatePicker

@synthesize picker = _picker;
@synthesize originalFrame = _originalFrame;

@synthesize doneTarget = _doneTarget;
@synthesize doneSelector = _doneSelector;

@synthesize date = _date;

- (instancetype) initWithFrame: (CGRect) frame {
    if ((self = [super initWithFrame: frame])) {
        self.originalFrame = frame;
        UIColor *color = [[UIColor alloc] initWithRed:255.0 green:255.0 blue:255.0 alpha:0.99];
        self.backgroundColor = color;
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, 300, 20)];
        
        [label setTextColor:[UIColor blackColor]];
        [label setBackgroundColor:[UIColor clearColor]];
        [label setFont:[UIFont fontWithName: @"Trebuchet MS" size: 10.0f]];
        label.text = @"Introduce tu fecha de nacimiento";
        [self addSubview:label];
        
        //CGFloat width = self.bounds.size.width;
        UIDatePicker *picker = [[UIDatePicker alloc] initWithFrame: CGRectMake(-70, -35, 180, MyDateTimePickerPickerHeight)];
        picker.datePickerMode = UIDatePickerModeDate;
        
        CGAffineTransform s0 = CGAffineTransformMakeScale(0.5, 0.5);
        CGAffineTransform t1 = CGAffineTransformMakeTranslation(0,0);
        picker.transform = CGAffineTransformConcat(s0, t1);
        
        [self addSubview: picker];
        
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, self.picker.frame.origin.y+118, self.frame.size.width, 0.5)];
        color = [UIColor colorWithRed:200/255.0 green:199/255.0 blue:204/255.0 alpha:1.0];;
        lineView.backgroundColor = color;
        [self addSubview:lineView];
        
        UIButton *but=[UIButton buttonWithType:UIButtonTypeRoundedRect];
        but.frame= CGRectMake(44, self.frame.size.height-38, 90, 38);
        [but setTitle:@"Hecho" forState:UIControlStateNormal];
        [but addTarget:self action:@selector(action) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:but];
        
        self.picker = picker;
    }
    return self;
}

- (void) setMode: (UIDatePickerMode) mode {
    self.picker.datePickerMode = mode;
}

- (void) addTargetForDoneButton: (id) target action: (SEL) action {
    self.doneTarget = target;
    self.doneSelector = action;
}

- (void) setHidden: (BOOL) hidden animated: (BOOL) animated {
    CGRect newFrame = self.originalFrame;
    newFrame.origin.y += hidden ? MyDateTimePickerHeight : 0;
    if (animated) {
        [UIView beginAnimations: @"animateDateTimePicker" context: nil];
        [UIView setAnimationDuration: 2];
        [UIView setAnimationCurve: UIViewAnimationCurveEaseOut];
        
        self.frame = newFrame;
        
        [UIView commitAnimations];
    } else {
        self.frame = newFrame;      
    }
}

-(void)action {
    if (self.delegate && [self.delegate respondsToSelector:@selector(datePickerDidPerformAction:)]) {
        _date = self.picker.date;
        [self.delegate datePickerDidPerformAction:self];
    }
}


@end

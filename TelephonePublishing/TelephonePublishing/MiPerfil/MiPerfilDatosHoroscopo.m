//
//  MiPerfilDatosHoroscopo.m
//  TelephonePublishing
//
//  Created by Tecnico IOS on 30/10/14.
//  Copyright (c) 2014 Hearst Magazine. All rights reserved.
//

#import "MiPerfilDatosHoroscopo.h"

@implementation MiPerfilDatosHoroscopo

-(void)viewConfig{
    
}

-(void)refrescarDatos:(NSDate *)fecha{
    NSLog(@"Refrescando datos %@", fecha);
    BOOL isFirstTime = YES;
    CGFloat midPositioning = self.frame.size.height/2;
    CGFloat buttonPositioning = self.frame.size.width/7;
    //TODO añadir si es la primera vez y crear los botones, si no actualizarlo
    if(isFirstTime){
    
    //Configuracion de dateformats para calculo de horoscopo y numero de la suerte
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"d"];
    NSInteger dia = [dateFormat stringFromDate:fecha].integerValue;
    [dateFormat setDateFormat:@"M"];
    NSInteger mes = [dateFormat stringFromDate:fecha].integerValue;
        [dateFormat setDateFormat:@"YYYY"];
    NSInteger anyo = [dateFormat stringFromDate:fecha].integerValue;
    
    //Codigo deteccion horoscopo
    switch (mes) {
        //Enero
        case 1:
            if (dia >=21){
                NSLog(@"Acuario");
                _fotoHoroscopo = [UIImage imageNamed:@"acuario.png"];
            }else{
                NSLog(@"Capricornio");
                _fotoHoroscopo = [UIImage imageNamed:@"Capricornio.png"];
            }
            break;
        //Febrero
        case 2:
            if (dia >=20){
                NSLog(@"Piscis");
                _fotoHoroscopo = [UIImage imageNamed:@"piscis.png"];
            }else{
                NSLog(@"Acuario");
                _fotoHoroscopo = [UIImage imageNamed:@"acuario.png"];
            }
            break;
        //Marzo
        case 3:
            if (dia >=21){
                NSLog(@"Aries");
                _fotoHoroscopo = [UIImage imageNamed:@"aries.png"];
            }else{
                NSLog(@"Piscis");
                _fotoHoroscopo = [UIImage imageNamed:@"piscis.png"];
            }
            break;
        //Abril
        case 4:
            if (dia >=22){
                NSLog(@"Tauro");
                _fotoHoroscopo = [UIImage imageNamed:@"tauro.png"];
            }else{
                NSLog(@"Aries");
                _fotoHoroscopo = [UIImage imageNamed:@"aries.png"];
            }
            break;
        //Mayo
        case 5:
            if (dia >=22){
                NSLog(@"Geminis");
                _fotoHoroscopo = [UIImage imageNamed:@"geminis.png"];
            }else{
                NSLog(@"Tauro");
                _fotoHoroscopo = [UIImage imageNamed:@"tauro.png"];
            }
            break;
        //Junio
        case 6:
            if (dia >=22){
                NSLog(@"Cancer");
                _fotoHoroscopo = [UIImage imageNamed:@"cancer.png"];
            }else{
                NSLog(@"Geminis");
                _fotoHoroscopo = [UIImage imageNamed:@"geminis.png"];
            }
            break;
        //Julio
        case 7:
            if (dia >=23){
                NSLog(@"Leo");
                _fotoHoroscopo = [UIImage imageNamed:@"leo.png"];
            }else{
                NSLog(@"Cancer");
                _fotoHoroscopo = [UIImage imageNamed:@"cancer.png"];
            }
            break;
        //Agosto
        case 8:
            if (dia >=23){
                NSLog(@"Virgo");
                _fotoHoroscopo = [UIImage imageNamed:@"virgo.png"];
            }else{
                NSLog(@"Leo");
                _fotoHoroscopo = [UIImage imageNamed:@"leo.png"];
            }
            break;
        //Septiembre
        case 9:
            if (dia >=23){
                NSLog(@"Libra");
                _fotoHoroscopo = [UIImage imageNamed:@"libra.png"];
            }else{
                NSLog(@"Virgo");
                _fotoHoroscopo = [UIImage imageNamed:@"virgo.png"];
            }
            break;
        //Octubre
        case 10:
            if (dia >=23){
                NSLog(@"Escorpio");
                _fotoHoroscopo = [UIImage imageNamed:@"escorpio.png"];
            }else{
                NSLog(@"Libra");
                _fotoHoroscopo = [UIImage imageNamed:@"libra.png"];
            }
            break;
        //Noviembre
        case 11:
            if (dia >=23){
                NSLog(@"Sagtario");
                _fotoHoroscopo = [UIImage imageNamed:@"sagitario.png"];
            }else{
                NSLog(@"Escorpio");
                _fotoHoroscopo = [UIImage imageNamed:@"escorpio.png"];
            }
            break;
        //Diciembre
        case 12:
            if (dia >=22){
                NSLog(@"Capricornio");
                _fotoHoroscopo = [UIImage imageNamed:@"capricornio.png"];
            }else{
                NSLog(@"Sagtario");
                _fotoHoroscopo = [UIImage imageNamed:@"sagitario.png"];
            }
            break;
        default:
            _fotoHoroscopo = [UIImage imageNamed:@"boton-horoscopo.png"];
            
    }
    //Codigo deteccion numero de la suerte

    NSString *num = [NSString stringWithFormat:@"%ld %ld %ld",(long)dia ,(long)mes, (long)anyo];
    
    while (num.length > 1){
        NSInteger total = 0;
        for (int i = 0; i < num.length; i++) {
            total += [num substringWithRange:NSMakeRange(i, 1)].integerValue;
        }
        num = [NSString stringWithFormat: @"%ld", (long)total];
    }
    
    NSLog(@"Numero de la suerte: %@", num);
    
    switch (num.integerValue) {
        case 1:
            _fotoNumerologia = [UIImage imageNamed:@"1.png"];
            break;
        case 2:
            _fotoNumerologia = [UIImage imageNamed:@"2.png"];
            break;
        case 3:
            _fotoNumerologia = [UIImage imageNamed:@"3.png"];
            break;
        case 4:
            _fotoNumerologia = [UIImage imageNamed:@"4.png"];
            break;
        case 5:
            _fotoNumerologia = [UIImage imageNamed:@"5.png"];
            break;
        case 6:
            _fotoNumerologia = [UIImage imageNamed:@"6.png"];
            break;
        case 7:
            _fotoNumerologia = [UIImage imageNamed:@"7.png"];
            break;
        case 8:
            _fotoNumerologia = [UIImage imageNamed:@"8.png"];
            break;
        case 9:
            _fotoNumerologia = [UIImage imageNamed:@"9.png"];
            break;
        default:
            _fotoNumerologia = [UIImage imageNamed:@"boton-numerologia.png"];
    }
    
    _fotoHoroscopoView = [[UIImageView alloc] initWithImage:_fotoHoroscopo];
    //Punto de creacion de la imagen y tamaño
    _fotoHoroscopoView.frame = CGRectMake((buttonPositioning*2)-51 , midPositioning - 60 , 51, 51);
    //Marco para la foto
    CALayer *borderLayer = [CALayer layer];
    CGRect borderFrame = CGRectMake(0, 0, (_fotoHoroscopoView.frame.size.width), (_fotoHoroscopoView.frame.size.height));
    [borderLayer setBackgroundColor:[[UIColor clearColor] CGColor]];
    [borderLayer setFrame:borderFrame];
    [borderLayer setCornerRadius:0];
    [borderLayer setBorderWidth:2];
    [borderLayer setBorderColor:[[UIColor grayColor] CGColor]];
    [_fotoHoroscopoView.layer addSublayer:borderLayer];
    //Creacion de boton en la imageview
    UIButton *buttonHoroscopo = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonHoroscopo setBackgroundColor:[UIColor clearColor]];
    [buttonHoroscopo addTarget:self action:@selector(botonHoroscopo:)
     forControlEvents:UIControlEventTouchUpInside];
    buttonHoroscopo.frame = CGRectMake(0.0, 0.0, 51.0, 51.0);
    _fotoHoroscopoView.userInteractionEnabled = YES;
    [_fotoHoroscopoView addSubview:buttonHoroscopo];
    [self addSubview:_fotoHoroscopoView];
    
    _fotoNumerologiaView = [[UIImageView alloc] initWithImage:_fotoNumerologia];
    //Punto de creacion de la imagen y tamaño
    _fotoNumerologiaView.frame = CGRectMake((buttonPositioning*4)-51 , midPositioning - 60 , 51, 51);
    //Marco para la foto
    borderLayer = [CALayer layer];
    borderFrame = CGRectMake(0, 0, (_fotoNumerologiaView.frame.size.width), (_fotoNumerologiaView.frame.size.height));
    [borderLayer setBackgroundColor:[[UIColor clearColor] CGColor]];
    [borderLayer setFrame:borderFrame];
    [borderLayer setCornerRadius:0];
    [borderLayer setBorderWidth:2];
    [borderLayer setBorderColor:[[UIColor grayColor] CGColor]];
    [_fotoHoroscopoView.layer addSublayer:borderLayer];
    //Creacion de boton en la imageview
    UIButton *buttonNumerologia = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonNumerologia setBackgroundColor:[UIColor clearColor]];
    [buttonNumerologia addTarget:self action:@selector(botonNumerologia:)
              forControlEvents:UIControlEventTouchUpInside];
    buttonNumerologia.frame = CGRectMake(0.0, 0.0, 51.0, 51.0);
    _fotoNumerologiaView.userInteractionEnabled = YES;
    [_fotoNumerologiaView addSubview:buttonNumerologia];
    [self addSubview:_fotoNumerologiaView];
    
    _fotoCartaAstral = [UIImage imageNamed:@"boton-carta-astral.png"];
    _fotoCartaAstralView = [[UIImageView alloc] initWithImage:_fotoCartaAstral];
    //Punto de creacion de la imagen y tamaño
    _fotoCartaAstralView.frame = CGRectMake((buttonPositioning*6)-51 , midPositioning - 60 , 51, 51);
    //Marco para la foto
    borderLayer = [CALayer layer];
    borderFrame = CGRectMake(0, 0, (_fotoCartaAstralView.frame.size.width), (_fotoCartaAstralView.frame.size.height));
    [borderLayer setBackgroundColor:[[UIColor clearColor] CGColor]];
    [borderLayer setFrame:borderFrame];
    [borderLayer setCornerRadius:0];
    [borderLayer setBorderWidth:2];
    [borderLayer setBorderColor:[[UIColor grayColor] CGColor]];
    [_fotoHoroscopoView.layer addSublayer:borderLayer];
    //Creacion de boton en la imageview
    UIButton *buttonCartaAstral = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonCartaAstral setBackgroundColor:[UIColor clearColor]];
    [buttonCartaAstral addTarget:self action:@selector(botonCartaAstral:)
                forControlEvents:UIControlEventTouchUpInside];
    buttonCartaAstral.frame = CGRectMake(0.0, 0.0, 51.0, 51.0);
    _fotoCartaAstralView.userInteractionEnabled = YES;
    [_fotoCartaAstralView addSubview:buttonCartaAstral];
    [self addSubview:_fotoCartaAstralView];
    
    UILabel *labelHoroscopo = [[UILabel alloc] initWithFrame:CGRectMake((buttonPositioning*2)-47, midPositioning-10, 100, 12)];
    labelHoroscopo.text = @"Horóscopo";
    labelHoroscopo.textColor = [UIColor greenColor];
    labelHoroscopo.font = [UIFont fontWithName:@"Arial" size:9.0];
    [self addSubview:labelHoroscopo];
        
    UILabel *labelNumerologia = [[UILabel alloc] initWithFrame:CGRectMake((buttonPositioning*4)-51, midPositioning-10, 100, 12)];
    labelNumerologia.text = @"Numerología";
    labelNumerologia.textColor = [UIColor greenColor];
    labelNumerologia.font = [UIFont fontWithName:@"Arial" size:9.0];
    [self addSubview:labelNumerologia];
        
    UILabel *labelCartaAstral = [[UILabel alloc] initWithFrame:CGRectMake((buttonPositioning*6)-48, midPositioning-10, 100, 12)];
    labelCartaAstral.text = @"Carta Astral";
    labelCartaAstral.textColor = [UIColor greenColor];
    labelCartaAstral.font = [UIFont fontWithName:@"Arial" size:9.0];
    [self addSubview:labelCartaAstral];
    
    UIButton *buttonHoroscopoPersonal = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonHoroscopoPersonal addTarget:self action:@selector(botonHoroscopoPersonal:)
                forControlEvents:UIControlEventTouchUpInside];
    buttonHoroscopoPersonal.frame = CGRectMake(5, midPositioning+10, self.frame.size.width-18, 30.0);
    buttonHoroscopoPersonal.backgroundColor = [UIColor greenColor];
    [buttonHoroscopoPersonal setTitle:@"Horóscopo Diario Personalizado" forState: UIControlStateNormal];
     [self addSubview:buttonHoroscopoPersonal];
        
    
        
    isFirstTime = NO;
    
    }
    else
    {
        
    }
    // Convert date object to desired output format
    /*[dateFormat setDateFormat:@"EEEE MMMM d, YYYY"];
    dateStr = [dateFormat stringFromDate:date];*/
}

-(void)botonHoroscopo:(id)sender{
    NSLog(@"Boton Horoscopo pulsado");
}

-(void)botonNumerologia:(id)sender{
    NSLog(@"Boton Numerologia pulsado");
}

-(void)botonCartaAstral:(id)sender{
    NSLog(@"Boton Carta Astral pulsado");
}

-(void)botonHoroscopoPersonal:(id)sender{
    NSLog(@"Boton Horoscopo personalizado pulsado");
}


@end

//
//  MiPerfil.m
//  TelephonePublishing
//
//  Created by Tpbeca tec on 01/10/14.
//  Copyright (c) 2014 Hearst Magazine. All rights reserved.
//

#import "MiPerfil.h"
#import "Perfiles.h"
#import <CoreData/CoreData.h>

@implementation MiPerfil

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
@synthesize fecha = _fecha;

-(instancetype)initWithPerfil:(NSString*)nombre fecha:(NSString*)fecha
{
    self = [super init];
    if (self)
    {
        [self viewConfig];
    }
    return self;
}

- (void)viewConfig{
    
    _foto = [UIImage imageNamed:@"icono-amigos.png"];
    _fotoView = [[UIImageView alloc] initWithImage:_foto];
    //Punto de creacion de la imagen y tamaño
    _fotoView.frame = CGRectMake(10, 10, 50, 50);
    //Marco para la foto
    CALayer *borderLayer = [CALayer layer];
    CGRect borderFrame = CGRectMake(0, 0, (_fotoView.frame.size.width), (_fotoView.frame.size.height));
    [borderLayer setBackgroundColor:[[UIColor clearColor] CGColor]];
    [borderLayer setFrame:borderFrame];
    [borderLayer setCornerRadius:0];
    [borderLayer setBorderWidth:2];
    [borderLayer setBorderColor:[[UIColor grayColor] CGColor]];
    [_fotoView.layer addSublayer:borderLayer];
    //Creacion de boton en la imageview
    _fotoButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_fotoButton setBackgroundColor:[UIColor clearColor]];
    [_fotoButton addTarget:self action:@selector(botonFoto:)
     forControlEvents:UIControlEventTouchUpInside];
    [_fotoButton setBackgroundImage:_foto forState:UIControlStateNormal];
    _fotoButton.frame = CGRectMake(0.0, 0.0, 50.0, 50.0);
    _fotoView.userInteractionEnabled = YES;
    [_fotoView addSubview:_fotoButton];
    //Punto de creacion de los botones nombre y fecha de nacimiento en una tableview
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(60,10,300-60,50)];
    [_tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    self.fetchedRecordsArray = [self getPerfil];
    [self addSubview:_fotoView];
    [self addSubview:_tableView];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 25;
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *cellIdentifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    NSInteger row = indexPath.row;
    NSString *text = nil;
    Perfiles * perfiles = nil;
    
    if(self.fetchedRecordsArray.count > 0){
        perfiles = (self.fetchedRecordsArray)[0];
    }
    

    
    if (nil == cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    if (row == 0) {
        if (perfiles == nil || perfiles.nombre == nil){
            text = @"Nombre";
        }
        else
        {
            text = perfiles.nombre;
            _nombre = perfiles.nombre;
        }
    }
    else if (row == 1){
        if (perfiles == nil || perfiles.fecha == nil){
            text = @"Fecha de nacimiento";
        }
        else{
            NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
            //Formateo de fecha
            [dateformate setDateFormat:@"dd"];
            NSString *dia = [dateformate stringFromDate:perfiles.fecha];
            [dateformate setDateFormat:@"MMMM"];
            NSString *mes = [dateformate stringFromDate:perfiles.fecha];
            [dateformate setDateFormat:@"YYYY"];
            NSString *anyo = [dateformate stringFromDate:perfiles.fecha];
            text = [NSString stringWithFormat:@"%@ de %@ de %@",dia ,mes, anyo];
            _fecha = perfiles.fecha;
            if(perfiles.foto != nil){
                UIImage *temp = [[UIImage alloc] initWithData:perfiles.foto];
                [_fotoButton setBackgroundImage:temp forState:UIControlStateNormal];
            }
        }
    }
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    //Las lineas comentadas es para añadir el > de la celda de color personalizado
    //UIImageView *checkmark = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"coloredCheckmark.png"]];
    //cell.accessoryView = checkmark;
    
    cell.textLabel.text = NSLocalizedString(text,nil);
    cell.textLabel.textColor = [UIColor blackColor];
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger row = indexPath.row;
    
    if (row == 0) {
        [self mostrarAlertNombre];
    }
    else if (row == 1){
        [self action];
    }
}

-(void)botonFoto:(id)sender{
    [_delegate botonFoto:sender];
}

- (void)saveData:(NSString *)nombre fecha:(NSDate *)fecha tipo:(NSString *)tipo foto:(NSData *)foto{
    
    Perfiles *newEntry = [NSEntityDescription insertNewObjectForEntityForName:@"Perfiles"
                                                      inManagedObjectContext:self.managedObjectContext];
    if(nombre != nil){
        [newEntry setValue:nombre forKey:@"nombre"];
    }
    if(fecha != nil){
        [newEntry setValue:fecha forKey:@"fecha"];
    }
    if(tipo != nil){
        [newEntry setValue:tipo forKey:@"tipo"];
    }
    if (foto != nil){
        [newEntry setValue:foto forKey:@"foto"];
    }
    
    NSError *error;
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Imposible grabar: %@", [error localizedDescription]);
    }
    [self reloadData];
}

- (NSManagedObjectContext *) managedObjectContext {
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator: coordinator];
    }
    return _managedObjectContext;
}

- (NSManagedObjectModel *)managedObjectModel {
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    _managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
    
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    NSURL *storeUrl = [NSURL fileURLWithPath: [[self applicationDocumentsDirectory]
                                               stringByAppendingPathComponent: @"Perfiles.sqlite"]];
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc]
                                   initWithManagedObjectModel:[self managedObjectModel]];
    if(![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                  configuration:nil URL:storeUrl options:nil error:&error]) {
        /*Error for store creation should be handled in here*/
    }
    
    return _persistentStoreCoordinator;
}

- (NSString *)applicationDocumentsDirectory {
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}

- (void)mostrarAlertNombre{
    dialog = [[UIAlertView alloc] initWithTitle:@"Intruduce tu nombre" message:@"" delegate:self cancelButtonTitle:@"Cancelar" otherButtonTitles:nil];
    dialog.tag = 1;
    [dialog addButtonWithTitle:@"Aceptar"];
    dialog.alertViewStyle = UIAlertViewStylePlainTextInput;
    [dialog setFrame:CGRectMake(20, 100, 300, 200)];
    [dialog show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    self.fetchedRecordsArray = [self getPerfil];
    if(buttonIndex == 1)
    {
        if(alertView.tag == 1){
            NSString *temp = [alertView textFieldAtIndex:0].text;
            if(temp != nil && ![temp  isEqual: @""])
            {
                NSLog(@"Datos a grabar en el perfil: Nombre: %@", temp);
                if (self.fetchedRecordsArray.count > 0){
                    _nombre = temp;
                    [self updateDatosPerfil];
                }
                else
                {
                    [self saveData:temp fecha:nil tipo:@"perfil" foto:nil];
                    [self reloadData];
                }
            }
        }
        if(alertView.tag == 2) {
            NSDate *datetemp = [alertView valueForKey:@"accessoryView"];
            NSLog(@"Datos a grabar en el perfil: Fecha: %@", datetemp);
        }
    }
}

-(NSArray*)getPerfil
{
    // initializing NSFetchRequest
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    //Setting Entity to be Queried
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Perfiles"
                                              inManagedObjectContext:self.managedObjectContext];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"tipo == 'perfil'"];
    
    [fetchRequest setEntity:entity];
    [fetchRequest setPredicate:predicate];
    NSError* error;
    
    // Query on managedObjectContext With Generated fetchRequest
    NSArray *fetchedRecords = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    NSLog(@"Numero de perfiles: %lu", (unsigned long)fetchedRecords.count);
    // Returning Fetched Records
    return fetchedRecords;
}

-(void)action {
    if (self.delegate && [self.delegate respondsToSelector:@selector(customViewDidPerformAction:)]) {
        [self.delegate customViewDidPerformAction:self];
    }
}

-(void)alertViewRefresh {
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(customViewDidPerformAction:)]) {
        [self.delegate alertViewDidPerformAction:self];
    }
}

-(void)reloadData{
    self.fetchedRecordsArray = [self getPerfil];
    [_tableView reloadData];
}

-(void)updateDatosPerfil{
    NSLog(@"Actualizando Perfil");
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Perfiles" inManagedObjectContext:self.managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"tipo == 'perfil'"];
    
    [request setEntity:entityDescription];
    
    [request setPredicate:predicate];
    
    NSError *error;
    NSArray *array = [_managedObjectContext executeFetchRequest:request error:&error];
    Perfiles *perfil = array[0];
    perfil.nombre = _nombre;
    perfil.fecha = _fecha;
    perfil.foto = _fotoPerfil;
    
    [self.managedObjectContext save:&error];
    
    [_tableView reloadData];
}

-(void)changeImage:(UIImage*)newImage
{
    NSLog(@"Debug: Cambiando foto %@",newImage);
    [_fotoButton setBackgroundImage:newImage forState:UIControlStateNormal];
    NSData *imageData = [NSData dataWithData:UIImagePNGRepresentation(newImage)];
    _fotoPerfil = imageData;
    if (self.fetchedRecordsArray.count > 0){
        _fotoPerfil = imageData;
        [self updateDatosPerfil];
    }
    else
    {
        [self saveData:nil fecha:nil tipo:@"perfil" foto:imageData];
        [self reloadData];
    }
    [self updateDatosPerfil];
}


@end

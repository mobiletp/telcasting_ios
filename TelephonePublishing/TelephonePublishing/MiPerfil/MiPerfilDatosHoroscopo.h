//
//  MiPerfilDatosHoroscopo.h
//  TelephonePublishing
//
//  Created by Tecnico IOS on 30/10/14.
//  Copyright (c) 2014 Hearst Magazine. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MiPerfilDatosHoroscopo;

@protocol MiPerfilDatosHoroscopoDelegate <NSObject>

@end

@interface MiPerfilDatosHoroscopo : UIView{
    UIImage* _fotoHoroscopo;
    UIImageView *_fotoHoroscopoView;
    UIImage* _fotoNumerologia;
    UIImageView *_fotoNumerologiaView;
    UIImage* _fotoCartaAstral;
    UIImageView *_fotoCartaAstralView;
}



-(void)refrescarDatos:(NSDate *)fecha;

@end

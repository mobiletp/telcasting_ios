//
//  TPDatePicker.h
//  TelephonePublishing
//
//  Created by Tecnico IOS on 29/10/14.
//  Copyright (c) 2014 Hearst Magazine. All rights reserved.
//

#import <UIKit/UIKit.h>

#define MyDateTimePickerHeight 260

@class TPDatePicker;

@protocol TPDatePickerDelegate <NSObject>
-(void)datePickerDidPerformAction:(TPDatePicker*)tpDatePicker;
@end

@interface TPDatePicker : UIView{
    __weak id<TPDatePickerDelegate> _delegate;
    NSDate* date;
}

@property (nonatomic, assign, readonly) UIDatePicker *picker;
@property (retain, nonatomic) NSDate *date;

@property(nonatomic,weak) id<TPDatePickerDelegate> delegate;

- (void) setMode: (UIDatePickerMode) mode;
- (void) setHidden: (BOOL) hidden animated: (BOOL) animated;
- (void) addTargetForDoneButton: (id) target action: (SEL) action;
//Delegate
-(void)action;

@end

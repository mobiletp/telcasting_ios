//
//  TPToolBarDown.m
//  TelephonePublishing
//
//  Created by Tpbeca tec on 17/09/14.
//  Copyright (c) 2014 Hearst Magazine. All rights reserved.
//

#import "TPTabbar.h"
#import "TPConstants.h"
#import "TPUtils.h"

@implementation TPTabbar

@synthesize tabMiPerfil=_tabMiPerfil;
@synthesize tabMisAmigos=_tabMisAmigos;

#pragma mark - Button methods

-(void)pushMiPerfil:(id)sender
{
    if(_delegate && [_delegate respondsToSelector:@selector(tabbarDelegateMiPerfil:)])
        [_delegate tabbarDelegateMiPerfil:self];
    
}

-(void)pushMisAmigos:(id)sender
{
    if(_delegate && [_delegate respondsToSelector:@selector(tabbarDelegateMisAmigos:)])
        [_delegate tabbarDelegateMisAmigos:self];
    
}

#pragma mark - Init

-(instancetype)init
{
    if (self = [super initWithFrame:CGRectMake(TABBAR_X, [[UIScreen mainScreen] applicationFrame].size.height-TABBAR_HEIGHT, TABBAR_WIDTH, TABBAR_HEIGHT)])
	{
        //color fondo
        [self setBackgroundColor:[UIColor greenColor]]; //para fondos poner clearcolor
        //background
        
        
        //Variables
        NSInteger totalButtons=2;
        NSInteger width=TABBAR_WIDTH/totalButtons;
        NSInteger x;
        
        //Mi perfil
        x = 0;
        _tabMiPerfil = [TPUtils buttonCreate:x :0 :width :TABBAR_HEIGHT :nil :nil :nil :@"YO"]; //TODO cambiar a constante
        [_tabMiPerfil addTarget:self action:@selector(pushMiPerfil:) forControlEvents:UIControlEventTouchUpInside];
        [_tabMiPerfil setBackgroundColor:[UIColor redColor]];
        [self addSubview:_tabMiPerfil];
        
        //Mis Amigos
        x += width;
        _tabMiPerfil = [TPUtils buttonCreate:x :0 :width :TABBAR_HEIGHT :nil :nil :nil :@"MIS AMIGOS"]; //TODO cambiar a constante
        [_tabMiPerfil addTarget:self action:@selector(pushMisAmigos:) forControlEvents:UIControlEventTouchUpInside];
        [_tabMiPerfil setBackgroundColor:[UIColor orangeColor]];
        [self addSubview:_tabMiPerfil];
        
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end

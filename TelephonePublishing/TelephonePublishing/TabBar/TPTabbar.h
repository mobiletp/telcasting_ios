//
//  TPToolBarDown.h
//  TelephonePublishing
//
//  Created by Tpbeca tec on 17/09/14.
//  Copyright (c) 2014 Hearst Magazine. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TPTabbar;

@protocol TPTabbarDelegate <NSObject>
-(void)tabbarDelegateMiPerfil:(TPTabbar *)tabbar;
-(void)tabbarDelegateMisAmigos:(TPTabbar *)tabbar;
@end

@interface TPTabbar : UIView
{
    //Fondo
    
    //Botones
    UIButton *_tabMisAmigos;
    UIButton *_tabMiPerfil;
}

- (instancetype) init NS_DESIGNATED_INITIALIZER;

@property (assign) id<TPTabbarDelegate> delegate;
@property (nonatomic,retain) UIButton *tabMiPerfil;
@property (nonatomic,retain) UIButton *tabMisAmigos;

@end


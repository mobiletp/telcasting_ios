//
//  Perfiles.h
//  TelephonePublishing
//
//  Created by Tecnico IOS on 28/10/14.
//  Copyright (c) 2014 Hearst Magazine. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Perfiles : NSManagedObject

@property (nonatomic, retain) NSString *nombre;
@property (nonatomic, retain) NSDate *fecha;
@property (nonatomic, retain) NSString *tipo;
@property (nonatomic, retain) NSData *foto;

@end

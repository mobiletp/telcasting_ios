//
//  Perfiles.m
//  TelephonePublishing
//
//  Created by Tecnico IOS on 28/10/14.
//  Copyright (c) 2014 Hearst Magazine. All rights reserved.
//

#import "Perfiles.h"


@implementation Perfiles

@dynamic nombre;
@dynamic fecha;
@dynamic tipo;
@dynamic foto;

@end

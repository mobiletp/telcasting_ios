//
//  CartaAstralViewController.m
//  TelephonePublishing
//
//  Created by Tecnico IOS on 27/4/15.
//  Copyright (c) 2015 Hearst Magazine. All rights reserved.
//

#import "TarotViewController.h"

@interface TarotViewController ()

@end

@implementation TarotViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor greenColor];
    
    tiradasMax = 5;
    tiradas = [[NSMutableArray alloc]initWithCapacity:0];
    
    [self createCartasButton];
    [self createCartasImage];
    
}

-(void)createCartasButton
{
    NSInteger x = 5;
    NSInteger y = 30;
    NSInteger margin = 5;
    NSInteger contadorColumnas = 0;
    
    for (int i = 1; i <= 22; i++)
    {
        //Creacion de botones para las cartas
        UIButton *carta = [[UIButton alloc] initWithFrame:CGRectMake(x + margin, y + margin, 52, 90)];
        [carta addTarget:self action:@selector(accionCarta:) forControlEvents:UIControlEventTouchUpInside];
        carta.tag = i * 100;
        [self.view addSubview:carta];
        x = carta.frame.origin.x + margin + carta.frame.size.width;
        contadorColumnas++;
        if (contadorColumnas == 5)
        {
            contadorColumnas = 0;
            x = 5;
            y = carta.frame.origin.y + margin + carta.frame.size.height;
        }
    }
    
}

-(void)createCartasImage
{
    NSInteger x = 5;
    NSInteger y = 30;
    NSInteger margin = 5;
    NSInteger contadorColumnas = 0;
    
    for (int i = 1; i <= 22; i++)
    {
        //Creaccion de las imagenes de las cartas
        UIImageView *cartaImage = [[UIImageView alloc]initWithFrame:CGRectMake(x + margin, y + margin, 52, 90)];
        cartaImage.tag = i;
        UIImage *temp = [UIImage imageNamed:@"REVERSO.png"];
        [cartaImage setImage:temp];
        [self.view addSubview:cartaImage];
        x = cartaImage.frame.origin.x + margin + cartaImage.frame.size.width;
        contadorColumnas++;
        if (contadorColumnas == 5)
        {
            contadorColumnas = 0;
            x = 5;
            y = cartaImage.frame.origin.y + margin + cartaImage.frame.size.height;
        }
    }
    
}

-(IBAction)accionCarta:(id)sender
{
    UIButton *tempButton = sender;
    NSNumber *tempTag = [NSNumber numberWithInteger:tempButton.tag]; //Para almacenar el numero del tag y poder compararlo lo transformamos a nsnumber(Que es un objeto)
    
    if (tiradasMax > 0 && ![tiradas containsObject:tempTag])
    {
        NSLog(@"Carta pulsada");
        UIImageView *cartaImage = [[UIImageView alloc]init];
        
        //Lector dinamico de cartas de tarot
        for (int i = 1; i <= 22; i++)
        {
            if((tempButton.tag/100) == i)
            {
                cartaImage = (UIImageView *)[self.view viewWithTag:i];
                NSNumber *temp = [NSNumber numberWithInteger:tempButton.tag]; //Para almacenar el numero del tag y poder compararlo lo transformamos a nsnumber(Que es un objeto)
                [tiradas addObject:temp];
            }
        }
        
        //Animacion de cambio de imagen en forma de dar la vuelta
        UIImage *flipImage = [UIImage imageNamed:@"interrogacion.png"];
        [UIView transitionWithView:cartaImage duration:0.5
                           options:UIViewAnimationOptionTransitionFlipFromRight animations:^{
                               cartaImage.image = flipImage;
                           } completion:nil];
        tiradasMax--;
    }
}

-(void)resetTiradaCartas
{
    tiradasMax = 5;
    [tiradas removeAllObjects];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

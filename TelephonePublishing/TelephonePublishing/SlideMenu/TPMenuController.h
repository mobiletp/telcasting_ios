//
//  TPMenuController.h
//  TelephonePublishing
//
//  Created by Tpbeca tec on 18/09/14.
//  Copyright (c) 2014 Hearst Magazine. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "RemoteImageView.h"

@protocol TPMenuProtocol

@end

@interface TPMenuController : UITableViewController /*<RemoteImageViewDelegate>*/

@property (nonatomic, retain) UITableView *rearTableView;
@property (nonatomic, retain) id<TPMenuProtocol> delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil delegate:(id<TPMenuProtocol>)delegate;

@end

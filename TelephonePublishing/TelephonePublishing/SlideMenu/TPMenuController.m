//
//  TPMenuController.m
//  TelephonePublishing
//
//  Created by Tecnico IOS tec on 18/09/14.
//  Copyright (c) 2014 Hearst Magazine. All rights reserved.
//

//

#import "TPMenuController.h"
#import "SWRevealViewController.h"
//#import "MainSlideController.h"
#import "TPAppDelegate.h"
#import "TPConstants.h"
#import "HMGeneral.h"
#import "RemoteImageView.h"


@interface TPMenuController()
{
    NSInteger _presentedRow; //Solo se usa si se necesita dejar seleccionado la seccion en el menu
}

@end

@implementation TPMenuController{
    //Sections
    NSMutableArray *_sections;
    RemoteImageView *_remoteImage;
}

@synthesize rearTableView = _rearTableView;
@synthesize delegate = _delegate;

#pragma mark - View lifecycle

- (id)init {
    [self loadMenu];
    if (self = [super init])
    {
        
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil delegate:(id<TPMenuProtocol>)delegateProtocole
{
    _delegate = delegateProtocole;
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

/*- (void)viewDidAppear:(BOOL)animated {
    NSIndexPath *indexPath=[NSIndexPath indexPathForRow:_presentedRow inSection:0];
    [self.tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionBottom];
}*/

#pragma marl - UITableView Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)theTableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _sections.count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *sectionName;
   
    sectionName = SLIDE_MENU_SECTION_NAME;
  
    return sectionName;
}

- (void)loadMenu
{
    //Sections
    _sections = [[NSMutableArray alloc] initWithArray:[[HMGeneral sharedInstance] sectionsArray]];
    
    //Add contact
    //[_sections addObject:[[HMGeneral sharedInstance] contactSection]];
    
    //Para quitar la primera seccion (Portada)
    //[_sections removeObject:[_sections objectAtIndex:0]];
    
    //Slide Menu
    self.clearsSelectionOnViewWillAppear = NO;
    self.tableView.backgroundColor = UIColorFromRGB(SLIDE_MENU_BACKGROUNDCOLOR);
    //self.view.backgroundColor = SLIDE_MENU_BACKGROUNDCOLOR;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero]; //Quitar lineas de decoracion sobrantes
    self.tableView.bounces = NO;
    
    
    //Imagen de fondo del menu
    UIImageView *bgImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ImagenFondo.png"]];
    bgImageView.frame = self.tableView.frame;
    [self.tableView setBackgroundView:bgImageView];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        
        cell = [[UITableViewCell alloc] initWithFrame:CGRectZero];
        cell.backgroundColor = UIColorFromRGB(SLIDE_MENU_BACKGROUNDCOLOR);
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = UIColorFromRGB(SLIDE_MENU_ROW_SELECTION_COLOR);
        [cell setSelectedBackgroundView:bgColorView];
        cell.textLabel.textColor = UIColorFromRGB(SLIDE_MENU_ROW_TEXT_COLOR);
        cell.textLabel.font = SLIDE_MENU_ROW_TEXT_FONT;
    }
    
    //Identifico los separadores para hacer que no sean seleccionables
    if([[_sections objectAtIndex:indexPath.row] isKindOfClass:[Section class]])
    {
        if ([[[[_sections objectAtIndex:indexPath.row] name] uppercaseString]  isEqual: @"BARRA"]){
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = UIColorFromRGB(SLIDE_MENU_SEPARATOR_BACKGROUNDCOLOR);
        [cell setBackgroundView:bgColorView];
        [cell setUserInteractionEnabled:NO];
        }
        else
        {
            cell.textLabel.text = [[[_sections objectAtIndex:indexPath.row] name] uppercaseString];
            NSString *textImg = [[_sections objectAtIndex:indexPath.row] logo2x];
            if (![textImg  isEqual: @""]){
                [cell setIndentationLevel:2];
                _remoteImage = [[RemoteImageView alloc] initWithFrame:CGRectMake(CELL_MARGIN, CELL_MARGIN, CELL_IMAGE_SIZE, CELL_IMAGE_SIZE)];
                [_remoteImage setFrame:CGRectMake(0, 0, MENU_CELL_ICON_WIDTH, MENU_CELL_ICON_HEIGHT)];
                [_remoteImage setDelegate:self];
                [_remoteImage stopLoadingImage];
                [_remoteImage loadImageFromURL:[NSURL URLWithString:textImg] loadingImage:[UIImage imageNamed:@"img_listado_default.png"] fixedSize:YES];
                [_remoteImage setContentMode:UIViewContentModeScaleAspectFill];
                [_remoteImage setClipsToBounds:YES];
                [cell.contentView addSubview:_remoteImage];
            }
            cell.textLabel.textAlignment = NSTextAlignmentLeft;
        }
    }
    else if([[_sections objectAtIndex:indexPath.row] isKindOfClass:[Contact class]])
    {
        cell.textLabel.text = @"CONTACTO";
        //[cell setUrlIcon:[(Contact *)[_sections objectAtIndex:indexPath.row] logo2x]];
        //[cell setCellColor:MENU_CELL_COLOR];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = self.tableView.backgroundColor;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //Si es un separador se le da otro tamaño
    if([[_sections objectAtIndex:indexPath.row] isKindOfClass:[Section class]])
    {
        if([[[[_sections objectAtIndex:indexPath.row] name] uppercaseString] isEqual: @"BARRA"]){
            return SLIDE_MENU_SEPARATORBAR;
        }
        else
        {
            return SLIDE_MENU_HEIGHT_ROW;
        }
    }
    else
    {
        return SLIDE_MENU_HEIGHT_ROW;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{

    return SLIDE_MENU_HEIGHT_HEADER;
}

//TODO Adaptar con fichero de parametros para personalizar cabecera
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
    if (sectionTitle == nil) {
        return nil;
    }
    
    UIView *view = [[UIView alloc] init];
    
    if ([sectionTitle isEqualToString:@"Image"])
    {
        view = [[UIView alloc] initWithFrame: CGRectMake(20, 8, 320, 20)];
        view.backgroundColor = [UIColor colorWithPatternImage: [UIImage imageWithContentsOfFile: [[NSBundle mainBundle] pathForResource: @"AR-MENU-iOS-162x60" ofType: @"png"]]];
    }
    else
    {
        UILabel *label = [[UILabel alloc] init];
        label.frame = CGRectMake(20, 8, 320, 20);
        label.backgroundColor = [UIColor clearColor];
        label.textColor = UIColorFromRGB(SLIDE_MENU_SECTION_TEXT_COLOR);
        label.shadowColor = [UIColor grayColor];
        label.shadowOffset = CGSizeMake(-1.0, 1.0);
        label.font = [UIFont boldSystemFontOfSize:24];
        label.text = sectionTitle;
        view = [[UIView alloc] init];
        view.backgroundColor = UIColorFromRGB(SLIDE_MENU_SECTION_BACKGROUNDCOLOR);
        [view addSubview:label];
    }
    
    
    
    return view;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger offsetBarras = 0;
    for (int i=0; i<indexPath.row; i++){
        NSString *view = [[[_sections objectAtIndex:i]viewType] uppercaseString];
        if ([view  isEqual: @"BARRA"])
            offsetBarras++;
    }
    SWRevealViewController *revealController = self.revealViewController;
    [revealController revealToggle:self];

    [((TPAppDelegate*)[UIApplication sharedApplication].delegate) goHorizontal:indexPath.row-offsetBarras];

    
}

-(BOOL)shouldAutorotate
{
    return YES;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

@end

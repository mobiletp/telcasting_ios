//
//  HMGeneral.h
//  Elle
//
//  Created by Julio Rivas on 23/08/12.
//  Copyright (c) 2012 TAPTAP Networks. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TPConstants.h"
#import "SectionsDAO.h"
#import "HMDataManager.h"
//#import "TTNMail.h"
//#import "TTNFacebook.h"
//#import "TTNTwitter.h"
//#import "TTNWebBrowserController.h"
//#import "HMRecognitionViewController.h"

@class HMGeneral;
@class ComScore;
@class Contact;
@class Images;

@protocol HMGeneralDelegate <NSObject>
-(void)general:(HMGeneral *)general setFavorite:(BOOL)favorite;
@end

@interface HMGeneral : NSObject <HMDataManagerDelegate,UIAlertViewDelegate/*,TTNTwitterDelegate,TTNFacebookDelegate,TTNMailDelegate*/,UIActionSheetDelegate/*,TTNWebBrowserControllerDelegate/*,HMRecognitionDelegate*/>
{
    //Sections
    NSArray *_sectionAuxArray;
    NSMutableArray *_sectionsArray;
    
    //Ad type
    NSDictionary *_adProperties;
    SectionAdType _adType;
    CGFloat _adGalleryTime;
    
    //NativeAds
    BOOL _nativeAds;
    
    //Nielsen
    BOOL _nielsen;
    
    //Contact
    Contact *_contactSection;
    
    //Images
    Images *_imagesRecurso;
    
    //Favorite
    BOOL _hasFavoriteSection;
    Section *_favoriteSection;
    
    //Video
    NSString *_videoToken;
    NSString *_videoCanal;
    HMDataManager *_tokenDataManager;
    NSString *_videoCategory;
    
    //Share
    UIViewController *_viewController;
    NSObject *_shareObject;
    NSString *_shareUrlPinterest;
    NSString *_shareTitle;
    NSString *_shareUrl;
    NSString *_autorCita;
    
    //CurrentSection
    Section *_currentSection;
    
    //Favorites shows
    BOOL _isFavoritesShown;
    
    //ComScore
    //ComScore *_comScore;
    
    //Legal
    BOOL _legal;
    NSString *_legalUrl;
    BOOL _legalVersionControl;
    
    //SplashAnimation
    BOOL _splashAnimation;
    NSString *_splashUrl;
    
    //Tutorial
    BOOL _showTutorial;
    
    //BrandExperience
    BOOL _brandExperience;
    NSString *_brandExperienceImageToken;
 
    
    //Delegate
    @private id<HMGeneralDelegate> _delegate;
}

@property(nonatomic, retain) NSMutableArray *sectionsArray;
@property() BOOL nielsen;
@property() BOOL nativeAds;
@property() SectionAdType adType;
@property(nonatomic, retain) NSString *notificationsServerUrl;
@property(nonatomic, retain) NSString *notificationsAppId;
@property(nonatomic, retain) NSString *sonataAdsKey;
@property(nonatomic, retain) NSString *nativeAdsKey;
@property(nonatomic, retain) NSDictionary *adProperties;
@property(nonatomic,assign) CGFloat adGalleryTime;
@property(nonatomic, retain) Contact *contactSection;
@property(nonatomic, retain) Images *imagesRecurso;
@property (assign) BOOL contactShown;
@property(nonatomic, retain) Section *favoriteSection;
@property(assign) BOOL hasFavoriteSection;
@property(nonatomic, retain) NSString *videoToken;
@property(nonatomic, retain) NSString *videoCanal;
@property(assign) id<HMGeneralDelegate> delegate;
@property(nonatomic, retain) Section *currentSection;
@property() BOOL isFavoritesShown;
@property(nonatomic, retain) NSString *videoCategory;
@property(nonatomic, retain) ComScore *comScore;
@property() BOOL legal;
@property(nonatomic, retain) NSString *legalUrl;
@property() BOOL legalVersionControl;
@property() BOOL splashAnimation;
@property(nonatomic, retain) NSString *splashUrl;
@property() BOOL showTutorial;
@property() BOOL brandExperience;
@property(nonatomic, retain) NSString *brandExperienceImageToken;


+ (HMGeneral *)sharedInstance;
- (void)showFavoriteList:(UIViewController *)viewController;
- (BOOL)isFavorite:(NSObject *)object;
- (void)setLikeObject:(NSObject *)object :(UIViewController *)viewController;
//- (void)shareObject:(NSObject *)object :(UIViewController *)viewController;
- (void)shareMail:(UIViewController *)viewController :(NSObject *)object;
- (void)shareMail:(UIViewController *)viewController :(NSObject *)object :(NSString *)title;
- (void)shareMail:(UIViewController *)viewController :(NSObject *)object :(NSString *)title :(NSString *)url;
- (void)shareFacebook:(UIViewController *)viewController :(NSObject *)object;
- (void)shareFacebook:(UIViewController *)viewController :(NSObject *)object :(NSString *)title :(NSString *)url;
- (void)shareTwitter:(UIViewController *)viewController :(NSObject *)object;
- (void)shareTwitter:(UIViewController *)viewController :(NSObject *)object :(NSString *)title :(NSString *)url;
- (void)sharePinterest:(UIViewController *)viewController :(NSObject *)object :(NSString *)shareUrl;
- (void)shareWhatsapp:(UIViewController *)viewController :(NSObject *)object :(NSString *)title :(NSString *)url;
//- (void)shareActivity:(NSObject *)object inViewController:(UIViewController *)viewController;

//Recognition
- (void)launchRecognitionOverViewController:(UIViewController *)viewController animated:(BOOL)animation;


@end

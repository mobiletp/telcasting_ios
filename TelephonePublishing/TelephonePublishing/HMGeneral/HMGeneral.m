//
//  HMGeneral.m
//  Elle
//
//  Created by Julio Rivas on 23/08/12.
//  Copyright (c) 2012 TAPTAP Networks. All rights reserved.
//

#import "HMGeneral.h"
#import "TPUtils.h"
#import "FileManager.h"
#import "Item.h"
//#import "Itemactualidad.h"
//#import "EstrenosDetail.h"
//#import "CriticaDetail.h"
//#import "Astro.h"
#import "Image.h"
//#import "Post.h"
//#import "TTNWebBrowserController.h"
//#import "CoreData.h"
//#import "HMFavouritesViewController.h"
//#import "ComScoreManager.h"
//#import "ComScore.h"
//#import "Nielsen.h"
#import <QuartzCore/QuartzCore.h>
#import "Social/Social.h"
#import "Contact.h"
//#import "CarteleraUrl.h"
//#import "HMResultViewController.h"
//#import "WhatsAppKit.h"

//static HMGeneral *sharedCLDelegate = nil;

@implementation HMGeneral

@synthesize sectionsArray=_sectionsArray;
@synthesize nielsen=_nielsen;
@synthesize adType=_adType;
@synthesize nativeAds=_nativeAds;
@synthesize contactSection=_contactSection;
@synthesize videoToken=_videoToken;
@synthesize videoCanal=_videoCanal;
@synthesize delegate=_delegate;
@synthesize currentSection=_currentSection;
@synthesize isFavoritesShown=_isFavoritesShown;
@synthesize hasFavoriteSection=_hasFavoriteSection;
@synthesize favoriteSection=_favoriteSection;
@synthesize adProperties=_adProperties;
@synthesize videoCategory=_videoCategory;
@synthesize comScore=_comScore;
@synthesize legal=_legal;
@synthesize legalUrl=_legalUrl;
@synthesize legalVersionControl=_legalVersionControl;
@synthesize splashAnimation=_splashAnimation;
@synthesize splashUrl=_splashUrl;
@synthesize showTutorial=_showTutorial;
@synthesize brandExperience=_brandExperience;
@synthesize brandExperienceImageToken=_brandExperienceImageToken;


#pragma mark - Video

/*-(void)saveVideoToken
{    
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];	
	if (standardUserDefaults) {		
		[standardUserDefaults setObject:_videoToken forKey:UD_HM_VIDEO_TOKEN];
		[standardUserDefaults synchronize];
	}
}

-(void)retrieveVideoToken
{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];	
	if (standardUserDefaults) 
	{
        _videoToken=[standardUserDefaults objectForKey:UD_HM_VIDEO_TOKEN];
        if(!_videoToken)_videoToken=@"";
	}
}*/


#pragma mark - Sections

-(void)processVideos:(Section *)section
{            
    _tokenDataManager = [[HMDataManager alloc] initHMDataManager];
    [_tokenDataManager setDelegate:self];
    [_tokenDataManager loadData:[[NSString alloc] initWithString:[[SectionsDAO videoData] tokenUrl]]];

    _videoCanal=[[NSString alloc] initWithString:[[SectionsDAO videoData] canal]];
    _videoCategory=[[NSString alloc] initWithString:[[SectionsDAO videoData] desCategory]];
}

-(void)processSections
{
    for(Section *section in _sectionAuxArray)
    {        
        //Add section
        [_sectionsArray addObject:section];
        
        //Process videos
        if([[section viewType] isEqualToString:kViewTypeVideos])
        {
            //Video section
            [self processVideos:section];
        }
    }
}

#pragma mark - Init

-(instancetype)init
{
    self = [super init];
    
    if (self) {

        //Fonts
        //[HMUtils listAllFonts];
        
        //Default
        _nielsen=NO;
        _adType=SectionAdTypeTapTap;
        _nativeAds=NO;
        _sonataAdsKey=nil;
        _nativeAdsKey=nil;
        _brandExperience=NO;
        _showTutorial=NO;
        
        //Video token
        //[self retrieveVideoToken];
        
        // Get sections
        _sectionsArray = [[NSMutableArray alloc] initWithCapacity:0];
        _sectionAuxArray = [SectionsDAO mainSections];
        _notificationsServerUrl = [SectionsDAO notificationsServerUrl];
        _notificationsAppId = [SectionsDAO notificationsAppId];
        //_sectionAuxArray = [SectionsDAO mainSections];
        _nielsen = [SectionsDAO nielsen];
        _adType = [SectionsDAO adType];
        _adGalleryTime = [SectionsDAO adGalleryTime];
        _sonataAdsKey = [[NSString alloc] initWithString:[SectionsDAO sonataAdsKey]];
        _nativeAds = [SectionsDAO nativeAds];
        _nativeAdsKey = [[NSString alloc] initWithString:[SectionsDAO nativeAdsKey]];
        _adProperties = [[NSDictionary alloc] initWithDictionary:[SectionsDAO adProperties]];
        _legal = [SectionsDAO legal];
        _legalUrl = [[NSString alloc] initWithString:[SectionsDAO legalUrl]];
        _legalVersionControl = [SectionsDAO legalVersionControl];
        
        //Tutorial
        _showTutorial=[SectionsDAO showTutorial];
        
        //BrandExperience
        _brandExperience=[SectionsDAO brandExperience];
        _brandExperienceImageToken=[[NSString alloc] initWithString:[SectionsDAO brandExperienceImageToken]];
        
              
        
        //ComScore
        /*if([SectionsDAO comScore])
        {
            _comScore = [[ComScore alloc] initWithDictionary:[SectionsDAO comScoreProperties]];
            [_comScore setShow:[SectionsDAO comScore]];
        }*/
        
        //Splash
        if([SectionsDAO hasSplashImage])
        {
            _splashAnimation=[SectionsDAO splashAnimation];
            _splashUrl=[[SectionsDAO splashUrl] retain];
        }

        //Process sections
        [self processSections];
        
        //Process contact and apptecibles
        _contactSection = [[SectionsDAO contactData] retain];
        
        //Process images
        _imagesRecurso = [[SectionsDAO imagesData] retain];
        
        //Process favorite
        _hasFavoriteSection = [SectionsDAO hasFavoriteSection];
        if(_hasFavoriteSection)
        {
            _favoriteSection = [[SectionsDAO favoriteSection] retain];
        }
        
        //Nielsen
        /*if(_nielsen)
        {
            [[Nielsen sharedInstance] initNielsenWithHTML:[SectionsDAO nielsenPixeltrack]];
        }*/
        
        //File manager
        [[FileManager sharedInstance] initFileManager];
        
        //Twitter
        //[[TTNTwitter sharedInstance] initTTNTwitter];
        //[[TTNTwitter sharedInstance] setDelegate:self];
        
        //Mail
        //[[TTNMail sharedInstance] initTTNMail];
       // [[TTNMail sharedInstance] setDelegate:self];
        
        //Facebook
        //[self initFacebook];
        //[[TTNFacebook sharedInstance] initTTNFacebook];
        //[[TTNFacebook sharedInstance] setDelegate:self];
    }
	return self;
}

#pragma mark - DataManager delegate

- (void)dataManagerDidFinish:(HMDataManager *)dataManager
{
    if(dataManager==_tokenDataManager)
    {
        DLog();
    }
}

- (void)dataManagerDidParse:(HMDataManager *)dataManager withObject:(NSObject *)object
{
    if(dataManager==_tokenDataManager && object && [object isKindOfClass:[NSString class]])
    {
        _videoToken=[[NSString alloc] initWithString:(NSString *)object];
        //[self saveVideoToken];
    }
}

- (void)dataManagerDidFail:(HMDataManager *)dataManager withError:(NSError *)error
{
    if(dataManager==_tokenDataManager)
    {
        //_videoToken=@"";
    }
}

#pragma mark - Show favorite list

- (BOOL)isFavorite:(NSObject *)object
{
    if([object isKindOfClass:[Item class]])
    {
        //return [[CoreData sharedInstance] existItem:[(Item *)object idItem]];
    }
    /*else if([object isKindOfClass:[Post class]])
    {
        return [[CoreData sharedInstance] existPost:[(Post *)object postId]];
    }*/
    
    return NO;
}

/*- (void)showFavoriteList:(UIViewController *)viewController
{
    _isFavoritesShown=YES;
    
    //Favorite
    HMFavouritesViewController *favoriteViewController = [[HMFavouritesViewController alloc] init];
    
    //Parent
    [favoriteViewController setParent:viewController];
    
    //Navigation with front view controller
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:favoriteViewController];
    
    //Hide nav bar
    [navigationController setNavigationBarHidden:YES];

    /*UINavigationController *navigationController = [HMUtils customizedNavigationController:[UIImage imageNamed:@"navbar.png"]];
    [navigationController setViewControllers:[NSArray arrayWithObject:favoriteViewController]];
    
    //Hide nav bar
    [navigationController setNavigationBarHidden:YES];
    
    //Modal
    [viewController presentViewController:navigationController animated:YES completion:nil];
    
    //Libero
    [favoriteViewController release];
}*/

#pragma mark - Like item

/*- (void)setLikeObject:(NSObject *)object :(UIViewController *)viewController
{
    //Object
    _shareObject = object;

    //Check item
    if([object isKindOfClass:[Item class]] )
    {
        //Check if is favourite
        if(![[CoreData sharedInstance] existItem:[(Item *)object idItem]])
        {
            //Alert
            UIAlertView *alerta = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"_ALERT_MY_FAVORITES", @"") message:NSLocalizedString(@"_ALERT_FAVOURITES", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"_BUT_CANCEL", @"") otherButtonTitles:NSLocalizedString(@"_BUT_OK", @""),nil];
            [alerta setTag:ALERT_FAVOURITES_TAG];
            [alerta show];
            [alerta release];
        }
        else
        {
            //Alert
            UIAlertView *alerta = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"_ALERT_MY_FAVORITES", @"") message:NSLocalizedString(@"_ALERT_FAVOURITES_REMOVE", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"_BUT_NO", @"") otherButtonTitles:NSLocalizedString(@"_BUT_YES", @""),nil];
            [alerta setTag:ALERT_FAVOURITES_DELETE_TAG];
            [alerta show];
            [alerta release];
        }
    }
    else if([object isKindOfClass:[Post class]])
    {
        //Check if is favourite
        if(![[CoreData sharedInstance] existPost:[(Post *)object postId]])
        {
            //Alert
            UIAlertView *alerta = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"_ALERT_MY_FAVORITES", @"") message:NSLocalizedString(@"_ALERT_FAVOURITES", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"_BUT_CANCEL", @"") otherButtonTitles:NSLocalizedString(@"_BUT_OK", @""),nil];
            [alerta setTag:ALERT_FAVOURITES_TAG];
            [alerta show];
            [alerta release];
        }
        else
        {
            //Alert
            UIAlertView *alerta = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"_ALERT_MY_FAVORITES", @"") message:NSLocalizedString(@"_ALERT_FAVOURITES_REMOVE", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"_BUT_NO", @"") otherButtonTitles:NSLocalizedString(@"_BUT_YES", @""),nil];
            [alerta setTag:ALERT_FAVOURITES_DELETE_TAG];
            [alerta show];
            [alerta release];
        }

    }
}*/

#pragma mark - Alert view delegate

/*- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch ([alertView tag]) {

        case ALERT_FAVOURITES_TAG:

            if(buttonIndex==1)
            {
                //Delegate
                [_delegate general:self setFavorite:YES];
                
                //Ok
                if([_shareObject isKindOfClass:[Item class]])
                {
                    [[CoreData sharedInstance] addItem:(Item *)_shareObject];
                }
                else if([_shareObject isKindOfClass:[Post class]])
                {
                    [[CoreData sharedInstance] addPost:(Post *)_shareObject];
                }
            }
            
            break;

        case ALERT_PINTEREST_TAG:
            
            if (buttonIndex == 1)
            {
                NSString *stringURL = @"http://itunes.apple.com/us/app/pinterest/id429047995?mt=8";
                NSURL *url = [NSURL URLWithString:stringURL];
                [[UIApplication sharedApplication] openURL:url];
            }
            
            break;
            
        case ALERT_FAVOURITES_DELETE_TAG:
            
            if (buttonIndex == 1)
            {
                //Delegate
                [_delegate general:self setFavorite:NO];
                
                //Elimino de favoritos
                if([_shareObject isKindOfClass:[Item class]])
                {
                    [[CoreData sharedInstance] deleteItem:[(Item *)_shareObject idItem]];
                }
                else if([_shareObject isKindOfClass:[Post class]])
                {
                    [[CoreData sharedInstance] deletePost:[(Post *)_shareObject postId]];
                }            
            }
            
        default:
            break;
    }
}*/

/*
#pragma mark - Share object

- (void)shareObject:(NSObject *)object :(UIViewController *)viewController
{
    //Object
    _shareObject = object;
    
    //ViewController
    _viewController = viewController;
    
    //Action sheet compartir
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"_SHARE_TITLE", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"_BUT_CANCEL", @"") destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"_BUT_PINTEREST",@""),NSLocalizedString(@"_BUT_TWITTER", @""), NSLocalizedString(@"_BUT_FACEBOOK", @""), NSLocalizedString(@"_BUT_MAIL", @""),nil];
    actionSheet.actionSheetStyle = UIActionSheetStyleBlackOpaque;
    [actionSheet showInView:viewController.view];
    [actionSheet release];
}*/

#pragma mark - Twitter

- (void)shareTwitter:(UIViewController *)viewController :(NSObject *)object :(NSString *)title :(NSString *)url
{
    //Title
    _shareTitle=title;
    
    //Url
    _shareUrl=url;
    
    //Twitter
    [self shareTwitter:viewController :object];
}


/*- (void)shareTwitter:(UIViewController *)viewController :(NSObject *)object
{
    //Object
    _shareObject=object;
    
    //Data
    //NSMutableString *temp = [NSMutableString string];
    NSString *tweet = @"";
    NSString *link = @"";
    
    //Item
    if([_shareObject isKindOfClass:[Item class]])
    {
            tweet = [HMUtils flattenHTML:[(Item *)_shareObject title]];
            link = [(Item *)_shareObject shareUrl];
        //Comscore
        [[ComScoreManager sharedInstance] fireComScoreShare:@"Twitter" :[_currentSection advertiseKey] :[(Item *)_shareObject title] :link];
    }
    //Image
    else if([_shareObject isKindOfClass:[Image class]])
    {
        tweet = _shareTitle; //NSLocalizedString(@"_SHARE_IMAGE", @"");//[(Image *)_shareObject title];
        link = _shareUrl; //[(Image *)_shareObject url];
        
        //Comscore
        [[ComScoreManager sharedInstance] fireComScoreShare:@"Twitter" :[_currentSection advertiseKey] :tweet :link];

    }
    //Post
    else if([_shareObject isKindOfClass:[Post class]])
    {
        tweet = [NSString stringWithFormat:NSLocalizedString(@"_SHARE_POST", @""),[(Post *)_shareObject title]];
        link = [(Post *)_shareObject link];
        
        //Comscore
        //[[ComScoreManager sharedInstance] fireComScoreShare:@"Twitter" :[_currentSection advertiseKey] :tweet :link];
    }
    else if([_shareObject isKindOfClass:[Itemactualidad class]]){
        tweet = [HMUtils flattenHTML:[(Itemactualidad *)_shareObject title]];
        link = [(Itemactualidad *)_shareObject urlPortadas];
    }
    else if([_shareObject isKindOfClass:[EstrenosDetail class]]){
        tweet = [HMUtils flattenHTML:[(EstrenosDetail *)_shareObject title]];
        link = [(EstrenosDetail *)_shareObject shareUrl];
    }
    else if([_shareObject isKindOfClass:[CriticaDetail class]]){
        tweet = [HMUtils flattenHTML:[(CriticaDetail *)_shareObject titulopelicula]];
        link = [(CriticaDetail *)_shareObject urlPortadas];
    }
    
    //Tweet
    [[TTNTwitter sharedInstance] shareOnTwitter:viewController :tweet :link];
 
}

#pragma mark - Mail


-(NSString *)createMailBody:(NSObject *)object
{
    
    //Mail
    //NSString *htmlContentFile = [[NSString alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Mail.html" ofType:nil] encoding:NSUTF8StringEncoding error:nil];
    
    //Cuerpo
    NSMutableString *body = [NSMutableString string];
    
    //Espacio superior
    [body appendString:@"<p></p><p></p>"];
    
    //Item
    if([_shareObject isKindOfClass:[Item class]])
    {
       
        //Comscore
        [[ComScoreManager sharedInstance] fireComScoreShare:@"Mail" :[_currentSection advertiseKey] :[(Item *)_shareObject title] :[(Item *)_shareObject shareUrl]];

        //Title
        [body appendFormat:@"<p><a href='%@'><font size=\"5\"><b>%@</b></font></a></p>",[(Item *)_shareObject urlPortadas],[(Item *)object title]];

        
           
        //Imagen
        if([(Item *)object imageCell] == nil){
            NSLog(@"Sin imagenes");
        }else
        {
            if([(Item *)object imageCell])
                [body appendFormat:@"<p><a href='%@'><img src='%@'/></a></p>",[(Item *)object shareUrl],[(Image *)[(Item *)object imageCell] url]];
        }
        //Texto
        if([[(Item *)object templateItem] isEqual: @"cita"]){
            [body appendFormat:@"<p>%@</p>",[(Item *)object body]];
        }
        
        //Entradilla
        [body appendFormat:@"<p>%@</p>",[(Item *)object entradilla]];
        
        
        //info final
        [body appendFormat:@"<p>%@ <a href='%@'>%@</a></p>",NSLocalizedString(@"_MAIL_MORE_NEWS", @""), [(Item *)_shareObject shareUrl], NSLocalizedString(@"_MAIL_APP_WEB", @"")];
    }    
    //Image
    else if([_shareObject isKindOfClass:[Image class]])
    {
        NSLog(@"Debug Entre");
        //Comscore
        [[ComScoreManager sharedInstance] fireComScoreShare:@"Mail" :[_currentSection advertiseKey] :[(Image *)_shareObject title] :[(Image *)_shareObject url]];
        
        //Title
        //[body appendFormat:@"<p>%@</p>",_shareTitle];
        
        //Image
        [body appendFormat:@"<p><a href='%@'><img src='%@'/></a></p>",_shareUrl,[(Image *)_shareObject url]];
        
        //Pie
        [body appendFormat:@"<p>%@</p>",[[(Image *)_shareObject title] uppercaseString]];
        [body appendFormat:@"<p>%@</p>",[(Image *)_shareObject text]];
        
        //info
        [body appendFormat:@"<p>%@ <a href='%@'>%@</a></p>",NSLocalizedString(@"_MAIL_MORE_NEWS", @""), _shareUrl, NSLocalizedString(@"_MAIL_APP_WEB", @"")];
    }
    //Post
    else if([_shareObject isKindOfClass:[Post class]])
    {
        //Comscore
        //[[ComScoreManager sharedInstance] fireComScoreShare:@"Mail" :[_currentSection advertiseKey] :[(Post *)_shareObject title] :[(Post *)_shareObject link]];
        
        //Title
        [body appendFormat:@"<p><font size=\"5\"><b>%@</b></font></p>",[(Post *)_shareObject title]];
        
        //Imagen
        if([(Post *)_shareObject image])
            [body appendFormat:@"<p><a href='%@'><img src='%@'/></a></p>",[(Post *)_shareObject image],[(Post *)_shareObject image]];
        
        //Body
        [body appendFormat:@"<p>%@</p>",[(Post *)_shareObject description]];
                
        //info
        [body appendFormat:@"<p>%@ <a href='%@'>%@</a> %@ <a href='%@'>%@</a></p>",NSLocalizedString(@"_MAIL_ASTRO_INFO", @""), [(Post *)_shareObject link], [(Post *)_shareObject blogTitle],NSLocalizedString(@"_MAIL_ASTRO_INFO_IN", @""),APP_WEBSITE,NSLocalizedString(@"_MAIL_ASTRO_INFO_WEB", @"")];
    }
    else if([_shareObject isKindOfClass:[Itemactualidad class]])
    {
        
        //Comscore
        [[ComScoreManager sharedInstance] fireComScoreShare:@"Mail" :[_currentSection advertiseKey] :[(Item *)_shareObject title] :[(Item *)_shareObject urlPortadas]];
        
        //Title
        [body appendFormat:@"<p><a href='%@'><font size=\"5\"><b>%@</b></font></a></p>",[(Itemactualidad *)_shareObject urlPortadas],[(Itemactualidad *)object title]];
        
        
        
        //Imagen
        if([(Itemactualidad *)object imageCell] == nil){
            NSLog(@"Sin imagenes");
        }else
        {
            if([(Itemactualidad *)object imageCell])
                [body appendFormat:@"<p><a href='%@'><img src='%@'/></a></p>",[(Itemactualidad *)object urlPortadas],[(Itemactualidad *)object imageCell]];
        }

        
        //Entradilla
        [body appendFormat:@"<p>%@</p>",[(Itemactualidad *)object entradilla]];
        
        //info final
        [body appendFormat:@"<p>%@ <a href='%@'>%@</a></p>",NSLocalizedString(@"_MAIL_MORE_NEWS", @""), [(Itemactualidad *)_shareObject urlPortadas], NSLocalizedString(@"_MAIL_APP_WEB", @"")];
    }
    else if([_shareObject isKindOfClass:[EstrenosDetail class]])
    {
        
        //Comscore
        [[ComScoreManager sharedInstance] fireComScoreShare:@"Mail" :[_currentSection advertiseKey] :[(Item *)_shareObject title] :[(Item *)_shareObject urlPortadas]];
        
        //Title
        [body appendFormat:@"<p><a href='%@'><font size=\"5\"><b>%@</b></font></a></p>",[(EstrenosDetail *)_shareObject shareUrl],[(EstrenosDetail *)object title]];
        
        
        
        //Imagen
        if([(EstrenosDetail *)object image] == nil){
            NSLog(@"Sin imagenes");
        }else
        {
            if([(EstrenosDetail *)object image])
                [body appendFormat:@"<p><a href='%@'><img src='%@'/></a></p>",[(EstrenosDetail *)object shareUrl],[(EstrenosDetail *)object image]];
        }
        
        
        //Entradilla
        [body appendFormat:@"<p>%@</p>",[(EstrenosDetail *)object sinopsis]];
        
        //info final
        [body appendFormat:@"<p>%@ <a href='%@'>%@</a></p>",NSLocalizedString(@"_MAIL_MORE_NEWS", @""), [(EstrenosDetail *)_shareObject shareUrl], NSLocalizedString(@"_MAIL_APP_WEB", @"")];
    }
    else if([_shareObject isKindOfClass:[CriticaDetail class]])
    {
        
        //Comscore
        [[ComScoreManager sharedInstance] fireComScoreShare:@"Mail" :[_currentSection advertiseKey] :[(Item *)_shareObject title] :[(Item *)_shareObject urlPortadas]];
        
        //Title
        [body appendFormat:@"<p><a href='%@'><font size=\"5\"><b>%@</b></font></a></p>",[(CriticaDetail *)_shareObject urlPortadas],[(CriticaDetail *)object title]];
        
        
        //Imagen
        if([(CriticaDetail*)object image] == nil){
            NSLog(@"Sin imagenes");
        }else
        {
            if([(CriticaDetail *)object image])
                [body appendFormat:@"<p><a href='%@'><img src='%@'/></a></p>",[(CriticaDetail *)object urlPortadas],[(CriticaDetail *)object image]];
        }
        
        
        //Entradilla
        [body appendFormat:@"<p>%@</p>",[(CriticaDetail *)object texto]];
        
        //info final
        [body appendFormat:@"<p>%@ <a href='%@'>%@</a></p>",NSLocalizedString(@"_MAIL_MORE_NEWS", @""), [(CriticaDetail *)_shareObject urlPortadas], NSLocalizedString(@"_MAIL_APP_WEB", @"")];
    }
    
    //Send from
    [body appendFormat:@"<p>%@ <a href='%@'>%@</a></p>",NSLocalizedString(@"_MAIL_SEND_FROM", @""),APP_ITUNES,NSLocalizedString(@"_MAIL_DOWNLOAD_NOW", @"")];
     
    return body;
}

- (void)shareMail:(UIViewController *)viewController :(NSObject *)object :(NSString *)title :(NSString *)url
{
    //Object
    _shareObject = object;
    _shareTitle = title;

    //Share
    if([_shareObject isKindOfClass:[Item class]])_shareUrl = [(Item *)_shareObject shareUrl];
    else _shareUrl=url;
    
    //Asunto
    NSMutableString *subject = [NSMutableString string];
    
    if([_shareObject isKindOfClass:[Image class]])
    {
        [subject appendString:NSLocalizedString(@"_SHARE_IMAGE", @"")];
        [subject appendString:@": "];
        [subject appendString:_shareTitle];
    }
    else if([_shareObject isKindOfClass:[Post class]])
    {
        [subject appendFormat:NSLocalizedString(@"_SHARE_MAIL_SUBJECT", @"")];
        [subject appendString:@": "];
        [subject appendString:[(Post *)_shareObject title]];
    }
    else
    {
        [subject appendString:NSLocalizedString(@"_SHARE_MAIL_SUBJECT", @"")];
        [subject appendString:@": "];
        [subject appendString:[(Item *)_shareObject title]];
    }
    
    //Comparto por mail
    [[TTNMail sharedInstance] setViewController:viewController];
    [[TTNMail sharedInstance] shareByMail:subject :[self createMailBody:_shareObject]];
}

- (void)shareMail:(UIViewController *)viewController :(NSObject *)object :(NSString *)title
{
    [self shareMail:viewController :object :title :APP_WEBSITE];
}

- (void)shareMail:(UIViewController *)viewController :(NSObject *)object
{
    [self shareMail:viewController :object :@""];
}

#pragma mark - Facebook


- (void)shareFacebook:(UIViewController *)viewController :(NSObject *)object :(NSString *)title :(NSString *)url;
{
    //Title
    _shareTitle=title;
    
    //Url
    _shareUrl=url;
    
    //Share
    [self shareFacebook:viewController :object];
}

- (void)shareFacebook:(UIViewController *)viewController :(NSObject *)object;
{
    //Object
    _shareObject=object;
    
    //Data
    NSString *title = @"";
    NSString *link = @"";
    NSString *description = @"";
    NSString *imageUrl=@"";
    UIImage *image=nil;
    
    //Item
    if([_shareObject isKindOfClass:[Item class]])
    {
        title = [HMUtils flattenHTML:[(Item *)_shareObject title]];
        link = [(Item *)_shareObject shareUrl];
        description = [HMUtils flattenHTML:[(Item *)_shareObject title]];
        
        /*if([(Item *)object imageCell] == nil){
            NSLog(@"Sin imagenes");
        }else
        {
            if([(Item *)_shareObject images]) imageUrl = [(Image *)[[(Item *)_shareObject images] objectAtIndex:0] url];
        }*/
        //Comscore
/*        [[ComScoreManager sharedInstance] fireComScoreShare:@"Facebook" :[_currentSection advertiseKey] :[(Item *)_shareObject title] :link];
    }
    //Image
    else if([_shareObject isKindOfClass:[Image class]])
    {
        title = _shareTitle;
        link = _shareUrl;
        description = [HMUtils flattenHTML:[(Image *)_shareObject text]];
        imageUrl = [(Image *)_shareObject full];
        
        //Comscore
        [[ComScoreManager sharedInstance] fireComScoreShare:@"Facebook" :[_currentSection advertiseKey] :title :link];
    }
    //Post
    else if([_shareObject isKindOfClass:[Post class]])
    {
        title = [(Post *)_shareObject title];
        link = [(Post *)_shareObject link];
        //description = [(Post *)_shareObject description];
        imageUrl = [(Post *)_shareObject image];
        //Comscore
        [[ComScoreManager sharedInstance] fireComScoreShare:@"Facebook" :[_currentSection advertiseKey] :title :link];
    }
    else if ([_shareObject isKindOfClass:[Itemactualidad class]])
    {
        title = [HMUtils flattenHTML:[(Itemactualidad *)_shareObject title]];
        description = [HMUtils flattenHTML:[(Itemactualidad *)_shareObject entradilla]];
        if([(Itemactualidad *)object imageCell] == nil){
            imageUrl = [(Itemactualidad *)_shareObject imageGalery];
        }else{
            imageUrl = [(Itemactualidad *)_shareObject imageCell];
        }
        link = [(Itemactualidad *)_shareObject urlPortadas];
        [[ComScoreManager sharedInstance] fireComScoreShare:@"Facebook" :[_currentSection advertiseKey] :title :link];
    }
    else if ([_shareObject isKindOfClass:[EstrenosDetail class]])
    {
        title = [HMUtils flattenHTML:[(EstrenosDetail *)_shareObject title]];
        link = [(EstrenosDetail *)_shareObject shareUrl];
        description = [HMUtils flattenHTML:[(EstrenosDetail *)_shareObject sinopsis]];
        imageUrl = [(EstrenosDetail *)_shareObject image];
        [[ComScoreManager sharedInstance] fireComScoreShare:@"Facebook" :[_currentSection advertiseKey] :title :link];
    }
    else if ([_shareObject isKindOfClass:[CriticaDetail class]])
    {
        title = [HMUtils flattenHTML:[(CriticaDetail *)_shareObject titulopelicula]];
        link = [(CriticaDetail *)_shareObject urlPortadas];
        description = [HMUtils flattenHTML:[(CriticaDetail *)_shareObject title]];
        imageUrl = [(EstrenosDetail *)_shareObject image];
        [[ComScoreManager sharedInstance] fireComScoreShare:@"Facebook" :[_currentSection advertiseKey] :title :link];
    }else{
        title = _shareTitle;
        link = _shareUrl;
        description = [HMUtils flattenHTML:[(Image *)_shareObject title]];
        imageUrl = [(Image *)_shareObject full];
        NSLog(@"url image: %@",imageUrl);
        [[ComScoreManager sharedInstance] fireComScoreShare:@"Facebook" :[_currentSection advertiseKey] :title :link];
    }
    
    //Native
    Class facebookClass = NSClassFromString(@"SLComposeViewController");
    if(facebookClass)
    {
        SLComposeViewController *fbController=[SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        SLComposeViewControllerCompletionHandler __block completionHandler=^(SLComposeViewControllerResult result){
            
            [fbController dismissViewControllerAnimated:YES completion:nil];
            
            switch(result){
                case SLComposeViewControllerResultCancelled:
                    
                    //Error                    
                    DLog(@"%@",[NSError errorWithDomain:@"SLComposeViewControllerResultCancelled" code:SLComposeViewControllerResultCancelled userInfo:nil]);
                    
                    break;
                    
                case SLComposeViewControllerResultDone:
                    
                    //Done
                    DLog(@"Done");
                    
                    break;
                    
                default:
                    break;
            }};
        if([imageUrl isEqual: @""]){
            
        }
        else if(imageUrl && [imageUrl length]>0)
        {
            [fbController addImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imageUrl]]]];
        }
        else if(image)
        {
            [fbController addImage:image];
        }
        
        [fbController setInitialText:[NSString stringWithFormat:@"%@\r%@",title ,description]];
        
        [fbController addURL:[NSURL URLWithString:link]];
        [fbController setCompletionHandler:completionHandler];
        [viewController presentViewController:fbController animated:YES completion:nil];
    }
    else
    {
        //Facebook
        [[TTNFacebook sharedInstance] shareFacebook:title :link :description];
    }
}

#pragma mark - Pinterest

-(void)sharePinterest
{
    NSURL *url = [NSURL URLWithString:@"pinit12://pinterest.com/pin/create/bookmarklet/?url=URL-OF-THE-PAGE-TO-PIN&media=URL-OF-THE-IMAGE-TO-PIN&description=ENTER-YOUR-DESCRIPTION-FOR-THE-PIN"];
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        [[UIApplication sharedApplication] openURL:url];
    }else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Pinterest" message:@"Would you like to download Pinterest Application to share?" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:@"Continue", nil];
        [alert show];
    }
}*/

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex 
{
    if (buttonIndex == 1){
        NSString *stringURL = @"http://itunes.apple.com/us/app/pinterest/id429047995?mt=8";
        NSURL *url = [NSURL URLWithString:stringURL];
        [[UIApplication sharedApplication] openURL:url];
    }
}

/*-(void)sharePinterest:(UIViewController *)viewController :(NSObject *)object :(NSString *)shareUrl
{    
    //Data
    NSString *urlShare = @"";
    NSString *description = @"";
    NSString *imageUrl=@"";

    //Object
    _shareObject=object;

    //url
    _shareUrl = shareUrl;
    
    //Item
    if([_shareObject isKindOfClass:[Item class]])
    {
        if([[(Item *)object templateItem] isEqual: @"cita"])
        {
            urlShare = @"";
        }
        else
        {
            urlShare = [(Item *)_shareObject shareUrl];
        }
        description = [(Item *)_shareObject title];
        if([(Item *)object imageCell] == nil){
            NSLog(@"Sin imagenes");
        }else
        {
            if([(Item *)_shareObject imageCell]) imageUrl = [(Image *)[(Item *)_shareObject imageCell] url];
        }
    }
    //Image
    
    else if([_shareObject isKindOfClass:[Image class]])
    {
        urlShare = _shareUrl;
        description = [(Image *)_shareObject title];
        imageUrl = [(Image *)_shareObject full];
    }
    //Post
    else if([_shareObject isKindOfClass:[Post class]])
    {
        urlShare = [(Post *)_shareObject link];
        description = [(Post *)_shareObject title];
        imageUrl = [(Post *)_shareObject image];
    }

    
    //Comscore
    [[ComScoreManager sharedInstance] fireComScoreShare:@"Pinterest" :[_currentSection advertiseKey] :description :urlShare];
    
    //Url
    NSMutableString *url = [NSMutableString string];
    [url appendString:@"http://pinterest.com/pin/create/button/?"];
    [url appendString:@"url="];
    [url appendString:[HMUtils escapeUrlString:urlShare]]; //url
    [url appendString:@"&media="];
    [url appendString:[HMUtils escapeUrlString:imageUrl]]; //media
    [url appendString:@"&description="];
    [url appendString:[HMUtils escapeUrlString:description]];
    _shareUrlPinterest = [[NSString alloc] initWithString:url];
    
    //Webview
    TTNWebBrowserController * webViewController = [[TTNWebBrowserController alloc] initWithNibName:@"TTNWebBrowserController" bundle:[NSBundle mainBundle]];  
    [webViewController setDelegate:self];
    [viewController presentViewController:webViewController animated:YES completion:nil];
    [webViewController loadURL:[NSURL URLWithString:url]];
    //[webViewController loadHTML:htmlString];
    [webViewController release];
}*/

#pragma mark - Whatsapp

/*- (void)shareWhatsapp:(UIViewController *)viewController :(NSObject *)object :(NSString *)title :(NSString *)url
{
    if([WhatsAppKit isWhatsAppInstalled])
    {
        //Data
        NSString *_title = title ? title : @"";
        NSString *_link = url ? url : @"";

        //Object
        _shareObject = object;

        //TItle
        _shareTitle = title;
        
        //url
        _shareUrl = url;

        //autor cita
        _autorCita = @"";
        
        //Item
        if([_shareObject isKindOfClass:[Item class]])
        {
            _title = [HMUtils flattenHTML:[(Item *)_shareObject title]];
            //Texto
            _link = [(Item *)_shareObject shareUrl];
            
            //Comscore
            [[ComScoreManager sharedInstance] fireComScoreShare:@"Whatsapp" :[_currentSection advertiseKey] :[(Item *)_shareObject title] :_link];
        }
        //Image
        else if([_shareObject isKindOfClass:[Image class]])
        {
            _title = _shareTitle; //NSLocalizedString(@"_SHARE_IMAGE", @"");//[(Image *)_shareObject title];
            _link = _shareUrl; //[(Image *)_shareObject url];
            
            //Comscore
            [[ComScoreManager sharedInstance] fireComScoreShare:@"Whatsapp" :[_currentSection advertiseKey] :_title :_link];
            
        }
        //Post
        else if([_shareObject isKindOfClass:[Post class]])
        {
            _title = [NSString stringWithFormat:NSLocalizedString(@"_SHARE_POST", @""),[(Post *)_shareObject blogTitle]];
            _link = [(Post *)_shareObject link];
            
            //Comscore
            [[ComScoreManager sharedInstance] fireComScoreShare:@"Whatsapp" :[_currentSection advertiseKey] :_title :_link];
        }
        else if([_shareObject isKindOfClass:[Itemactualidad class]])
        {
            _title = [HMUtils flattenHTML:[(Itemactualidad *)_shareObject title]];
            //Texto
            _link = [(Itemactualidad *)_shareObject urlPortadas];
            
            
            //Comscore
            [[ComScoreManager sharedInstance] fireComScoreShare:@"Whatsapp" :[_currentSection advertiseKey] :[(Item *)_shareObject title] :_link];
        }
        else if([_shareObject isKindOfClass:[EstrenosDetail class]])
        {
            _title = [HMUtils flattenHTML:[(EstrenosDetail *)_shareObject title]];
            //Texto
            _link = [(Itemactualidad *)_shareObject urlPortadas];
            
            
            //Comscore
            [[ComScoreManager sharedInstance] fireComScoreShare:@"Whatsapp" :[_currentSection advertiseKey] :[(Item *)_shareObject title] :_link];
        }
        else if([_shareObject isKindOfClass:[CriticaDetail class]])
        {
            _title = [HMUtils flattenHTML:[(CriticaDetail *)_shareObject titulopelicula]];
            //Texto
            _link = [(Itemactualidad *)_shareObject urlPortadas];
            
            //Comscore
            [[ComScoreManager sharedInstance] fireComScoreShare:@"Whatsapp" :[_currentSection advertiseKey] :[(Item *)_shareObject title] :_link];
        }

        //Compose message
        NSMutableString *message = [NSMutableString string];
        if (_title) [message appendFormat:@"%@\n",_title];
        if(_link) [message appendFormat:@"%@\n",_link];
        if(_autorCita) [message appendFormat:@"%@\n",_autorCita];
        
        DLog(@"%@",message);
        
        //Message
        [WhatsAppKit launchWhatsAppWithMessage:message];
    }
    else
    {
        //Alert
        UIAlertView *alerta = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"_ALERT_TTILE", @"") message:NSLocalizedString(@"_ALERT_WHATSAPP_NOT_INSTALLED", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"_BUT_OK", @"") otherButtonTitles:nil];
        [alerta show];
    }
}*/

//#pragma mark - Share activity
//
//- (void)shareActivity:(NSObject *)object inViewController:(UIViewController *)viewController
//{
//    DLog();
//    
//    NSString *string = @"Prueba";
//    NSURL *url = [NSURL URLWithString:@"http://www.google.com"];
//    
//    UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:@[string, url] applicationActivities:nil];
//    
//
//    
//    [activityViewController setCompletionHandler:^(NSString *activityType, BOOL completed) {
//        
//        DLog(@"%@",activityType);
//        
//        if([activityType isEqualToString: UIActivityTypeMail]){
//            DLog(@"Mail");
//        }
//        if([activityType isEqualToString: UIActivityTypePostToFacebook]){
//            DLog(@"Facebook");
//        }
//        if([activityType isEqualToString: UIActivityTypePostToTwitter]){
//            DLog(@"Facebook");
//        }
//    }];
//    
//    [viewController presentViewController:activityViewController
//                                       animated:YES
//                                     completion:^{
//                                         
//                                     }];
//}
//
//- (id)activityViewController:(UIActivityViewController *)activityViewController itemForActivityType:(NSString *)activityType
//{
//    if ([activityType isEqualToString:UIActivityTypePostToFacebook]) {
//        return NSLocalizedString(@"Like this!",@"");
//    } else if ([activityType isEqualToString:UIActivityTypePostToTwitter]) {
//        return NSLocalizedString(@"Retweet this!",@"");
//    } else {
//        return nil;
//    }
//}

#pragma mark - Web browser

/*-(void)webBrowserControllerDelegate:(TTNWebBrowserController *)webBrowserController didFinishLoad:(UIWebView *)webview
{
    if([[[[webview request] URL] absoluteString] isEqualToString:@"http://m.pinterest.com/"] && _shareUrlPinterest && [_shareUrlPinterest length]>0)
    {
        [webBrowserController loadURL:[NSURL URLWithString:_shareUrlPinterest]];
        _shareUrlPinterest=@"";
    }
}*/

#pragma mark - Recognition

/*- (void)launchRecognitionOverViewController:(UIViewController *)viewController animated:(BOOL)animation
{
    //Recognition
    HMRecognitionViewController *recognitionViewController = [[HMRecognitionViewController alloc] init];
    
    //Parent
    [recognitionViewController setParent:viewController];
    
    //Delegate
    [recognitionViewController setDelegate:self];
    
    //Modal
    [viewController presentViewController:recognitionViewController animated:animation completion:nil];
}*/

/*-(void)recognitionDidFinish:(HMRecognitionViewController *)recognitionViewController withUrl:(NSString *)url
{
    //Result
    HMResultViewController *resultViewController = [[HMResultViewController alloc] initWithUrl:url];
    
    //Transition
    [recognitionViewController.parent.navigationController pushViewController:resultViewController animated:YES];
}*/

#pragma mark - Singleton

+(HMGeneral *) sharedInstance {
    static dispatch_once_t p = 0;
    __strong static id _sharedInstance = nil;
    
    dispatch_once(&p, ^{
        _sharedInstance = [[self alloc] init];
    });
    
    return _sharedInstance;
}

@end

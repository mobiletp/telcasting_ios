//
//  DetailController.m
//  TelephonePublishing
//
//  Created by Tecnico IOS on 19/5/15.
//  Copyright (c) 2015 Hearst Magazine. All rights reserved.
//

#import "DetailController.h"
#import "HoroscopoMes.h"
#import "Recetas.h"
#import "Dreams.h"

#define kHtmlFileHoroscopoMes @"HoroscopoMes.html"
#define kHtmlFileRecetas @"Recetas.html"
#define kHtmlFileDreams @"Dreams.html"


@interface DetailController ()

@end

@implementation DetailController

@synthesize _horoscopo = horoscopo;

- (id)initWithType:(NSString *)typeData array:(NSArray *)array idx:(NSInteger)idx;
{
    if (self = [super init])
    {
        type = typeData;
        if([type  isEqual: @"horoscopomes"]){
            NSArray *temp = [[NSArray alloc]initWithArray:array];
            horoscopo = [temp objectAtIndex:idx];
        }
        else if([type  isEqual: @"recetas"]){
            NSArray *temp = [[NSArray alloc]initWithArray:array];
            recetas = [temp objectAtIndex:idx];
        }
        else if([type  isEqual: @"dreams"]){
            NSArray *temp = [[NSArray alloc]initWithArray:array];
            dreams = [temp objectAtIndex:idx];
        }
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Webview
    webView=[[UIWebView alloc] initWithFrame:CGRectMake(0, 0, 320, [[UIScreen mainScreen] bounds].size.height- 120)];
    webView.opaque = FALSE;
    webView.backgroundColor = [UIColor clearColor];
    for (UIView *subview in webView.subviews) {
        if ([subview isKindOfClass:[UIScrollView class]]) {
            ((UIScrollView*)subview).bounces = NO;
           [((UIScrollView*)subview) performSelector:@selector(setBackgroundColor:) withObject:[UIColor whiteColor]];
        }
    }
    [webView setDelegate:self];
    [self.view addSubview:webView];
    
    if([type  isEqual: @"horoscopomes"]){
        [self loadHtmlHoroscopo];
    }
    else if([type  isEqual: @"recetas"]){
        [self loadHtmlRecetas];
    }
    else if([type  isEqual: @"dreams"]){
        [self loadHtmlDreams];
    }
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - HTML

-(void)loadHtmlHoroscopo
{
    //Indicator
    //_actIndicator.frame = CGRectMake(self.frame.size.width/2-DETAIL_ACT_INDICATOR_SIZE/2, self.frame.size.height/2-DETAIL_ACT_INDICATOR_SIZE/2, DETAIL_ACT_INDICATOR_SIZE, DETAIL_ACT_INDICATOR_SIZE);
    
    //Title
    NSString *_signo = horoscopo.signo;
    if (_signo == nil)_signo = @"";
    //Texto (Descripción)
    NSString *_texto = horoscopo.texto;
    if (_texto == nil) _texto = @"";
    //Amor
    NSString *_amor = horoscopo.amor;
    if(_amor == nil) _amor = @"";
    //Suerte
    NSString *_suerte = horoscopo.suerte;
    if( _suerte == nil) _suerte = @"";
    //Trabajo y dinero
    NSString *_trabajo = horoscopo.trabajo;
    if(_trabajo == nil) _trabajo = @"";
    
    //HTML
    NSString *htmlContentFile = [[NSString alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:kHtmlFileHoroscopoMes ofType:nil] encoding:NSUTF8StringEncoding error:nil];
    NSString *htmlWebView ;
    
    
        htmlWebView = [[NSString alloc] initWithFormat: htmlContentFile,
                       //_imagePath,
                       _signo,
                       _texto,
                       _amor,
                       _suerte,
                       _trabajo
                       ];
    
    
    DLog(@"htmlWebView: %@",htmlWebView);
    
    //Load HTML
    NSString *path = [[NSBundle mainBundle] bundlePath];
    NSURL *baseURL = [NSURL fileURLWithPath:path];
    [webView loadHTMLString:htmlWebView baseURL:baseURL];
    
    //Frame default
    //webView.frame = CGRectMake(0, 0, 320, self.view.frame.size.height);
}

-(void)loadHtmlRecetas
{
    //Indicator
    //_actIndicator.frame = CGRectMake(self.frame.size.width/2-DETAIL_ACT_INDICATOR_SIZE/2, self.frame.size.height/2-DETAIL_ACT_INDICATOR_SIZE/2, DETAIL_ACT_INDICATOR_SIZE, DETAIL_ACT_INDICATOR_SIZE);
    
    //Title
    NSString *_titulo = recetas.titulo;
    if (_titulo == nil)_titulo = @"";
    //Amor
    NSString *_tipo = recetas.tipo;
    if(_tipo == nil) _tipo = @"";
    //Texto (Descripción)
    NSString *_texto = recetas.texto;
    if (_texto == nil) _texto = @"";
    
    //HTML
    NSString *htmlContentFile = [[NSString alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:kHtmlFileRecetas ofType:nil] encoding:NSUTF8StringEncoding error:nil];
    NSString *htmlWebView;
    
    
    htmlWebView = [[NSString alloc] initWithFormat: htmlContentFile,
                   //_imagePath,
                   _titulo,
                   _tipo,
                   _texto
                   ];
    
    
    DLog(@"htmlWebView: %@",htmlWebView);
    
    //Load HTML
    NSString *path = [[NSBundle mainBundle] bundlePath];
    NSURL *baseURL = [NSURL fileURLWithPath:path];
    [webView loadHTMLString:htmlWebView baseURL:baseURL];
    
    //Frame default
    //webView.frame = CGRectMake(0, 0, 320, self.view.frame.size.height);
}

-(void)loadHtmlDreams
{
    //Indicator
    //_actIndicator.frame = CGRectMake(self.frame.size.width/2-DETAIL_ACT_INDICATOR_SIZE/2, self.frame.size.height/2-DETAIL_ACT_INDICATOR_SIZE/2, DETAIL_ACT_INDICATOR_SIZE, DETAIL_ACT_INDICATOR_SIZE);
    
    //Title
    NSString *_titulo = dreams.titulo;
    if (_titulo == nil)_titulo = @"";
    //Texto (Descripción)
    NSString *_texto = dreams.texto;
    if (_texto == nil) _texto = @"";
    
    //HTML
    NSString *htmlContentFile = [[NSString alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:kHtmlFileDreams ofType:nil] encoding:NSUTF8StringEncoding error:nil];
    NSString *htmlWebView;
    
    
    htmlWebView = [[NSString alloc] initWithFormat: htmlContentFile,
                   //_imagePath,
                   _titulo,
                   _texto
                   ];
    
    
    DLog(@"htmlWebView: %@",htmlWebView);
    
    //Load HTML
    NSString *path = [[NSBundle mainBundle] bundlePath];
    NSURL *baseURL = [NSURL fileURLWithPath:path];
    [webView loadHTMLString:htmlWebView baseURL:baseURL];
    
    //Frame default
    //webView.frame = CGRectMake(0, 0, 320, self.view.frame.size.height);
}

#pragma mark - UIWebViewDelegate

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType;
{
    return YES;
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{

}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    
}

- (void)webViewDidFinishLoad:(UIWebView *)aWebView
{

}

@end

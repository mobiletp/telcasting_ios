//
//  DetailController.h
//  TelephonePublishing
//
//  Created by Tecnico IOS on 19/5/15.
//  Copyright (c) 2015 Hearst Magazine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HoroscopoMes.h"
#import "Recetas.h"
#import "Dreams.h"

@interface DetailController : UIViewController <UIWebViewDelegate>
{
    //Horoscopo
    HoroscopoMes *horoscopo;
    
    //Recetas
    Recetas *recetas;
    
    //Recetas
    Dreams *dreams;
    
    //Type
    NSString *type;
    
    //WebView
    UIWebView *webView;

}

@property (nonatomic, retain)HoroscopoMes *_horoscopo;

- (id)initWithType:(NSString *)typeData array:(NSArray *)array idx:(NSInteger)idx;

@end

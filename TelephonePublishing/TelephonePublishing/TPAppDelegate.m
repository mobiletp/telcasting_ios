//
//  TPAppDelegate.m
//  TelephonePublishing
//
//  Created by Tpbeca tec on 16/09/14.
//  Copyright (c) 2014 Hearst Magazine. All rights reserved.
//

#import "TPAppDelegate.h"
#import "HMGeneral.h"
#import "TPViewController.h"
#import "Horoscopo.h"
#import "TarotViewController.h"
#import "ListViewFilterViewController.h"
#import "NumerologiaViewController.h"
#import "TPMenuController.h"
#import "SWRevealViewController.h"
#import "ContactarViewController.h"
#import "DetailController.h"


@implementation TPAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    //Sections
    _sections = [[NSMutableArray alloc] initWithArray:[[HMGeneral sharedInstance] sectionsArray]];
    
    //Add contact
    [_sections addObject:[[HMGeneral sharedInstance] contactSection]];
    
    TPViewController *frontViewController = [[TPViewController alloc] init];
	TPMenuController *rearViewController = [[TPMenuController alloc] init];
    
    _viewController = [[SWRevealViewController alloc] initWithRearViewController:rearViewController frontViewController:nil];
    self.viewController = _viewController;
    
    [self initHorizontalView];
    
    [_viewController pushFrontViewController:_horizontalViewController animated:YES];
	
	self.window.rootViewController = self.viewController;
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)initHorizontalView
{
    _arrayVC = [[NSMutableArray alloc] init];
    _arrayNames = [[NSMutableArray alloc] init];
    
    for (Section *secciones in _sections)
    {
        if([secciones isKindOfClass:[Section class]])
        {
            NSString *view = [[secciones viewType] uppercaseString];
            if ([view  isEqual: @"PERFIL"]){
                [[HMGeneral sharedInstance] setCurrentSection:secciones];
                TPViewController *perfil = [[TPViewController alloc] init];
                //[perfil setCurrentSection:[[HMGeneral sharedInstance] currentSection]];
                [_arrayVC addObject:perfil];
                [_arrayNames addObject:[secciones name].uppercaseString];
            }
            else if  ([view  isEqual: @"HOROMENSUAL"])
            {
                Horoscopo *horoscopo = [[Horoscopo alloc] initWithType:@"normal" url:[secciones url]];
                [_arrayVC addObject:horoscopo];
                [_arrayNames addObject:[secciones name].uppercaseString];
            }
            else if ([view  isEqual: @"TAROT"]){
                [[HMGeneral sharedInstance] setCurrentSection:secciones];
                TarotViewController *tarot = [[TarotViewController alloc] init];
                //[filView setCurrentSection:[[HMGeneral sharedInstance] currentSection]];
                [_arrayVC addObject:tarot];
                [_arrayNames addObject:[secciones name].uppercaseString];
            }
            else if  ([view  isEqual: @"NUMEROLOGIA"])
            {
                [[HMGeneral sharedInstance] setCurrentSection:secciones];
                NumerologiaViewController *viewC = [[NumerologiaViewController alloc] init];
                [_arrayVC addObject:viewC];
                [_arrayNames addObject:[secciones name].uppercaseString];
            }
            else if  ([view  isEqual: @"BERTACONTESTA"])
            {
                [[HMGeneral sharedInstance] setCurrentSection:secciones];
                ContactarViewController *viewC = [[ContactarViewController alloc] init];
                [_arrayVC addObject:viewC];
                [_arrayNames addObject:[secciones name].uppercaseString];
            }
            else if ([view isEqual: @"HOROCHINO"])
            {
                Horoscopo *viewC = [[Horoscopo alloc] initWithType:@"chino" url:[secciones url]];
                [_arrayVC addObject:viewC];
                [_arrayNames addObject:[secciones name].uppercaseString];
            }
            else if ([view isEqual: @"RECMAGICAS"])
            {
            ListViewFilterViewController *viewC = [[ListViewFilterViewController alloc] initWithType:@"recetas" url:[secciones url]];
            [_arrayVC addObject:viewC];
            [_arrayNames addObject:[secciones name]];
            }
            else if ([view isEqual: @"SUEGNOS"])
            {
                ListViewFilterViewController *viewC = [[ListViewFilterViewController alloc] initWithType:@"dreams" url:[secciones url]];
                [_arrayVC addObject:viewC];
                [_arrayNames addObject:[secciones name]];
            }
          /*else if ([view isEqual: @"LISTFAVOR"])
            {
            ListViewController *viewC = [[ListViewController alloc] initFavoritos];
            viewC.title = [secciones name];
            [_arrayVC addObject:viewC];
            [_arrayNames addObject:[secciones name]];
            }
            else if ([view isEqual: @"LISTLISTREC"])
            {
            CategoryViewController *viewC = [[CategoryViewController alloc] initWithType:@"milista"];
            viewC.title = [secciones name];
            [_arrayVC addObject:viewC];
            [_arrayNames addObject:[secciones name]];
            }*/
        }
        else
        {
            /*HMContactViewController *viewC = [[HMContactViewController alloc] init];
            viewC.title = @"Contacto";
            //iewC.delegate = self;
            [_arrayVC addObject:viewC];
            [_arrayNames addObject:@"CONTACTO"];*/
        }
        _horizontalViewController = [[ContainerViewController alloc] initWithArray:_arrayVC nameArray:(NSArray *)_arrayNames index:0 usetoolbar:NO shareButton:NO isPelicula:NO backButton:NO object:nil preload:NO];
        //indexHorizontal = 0;
    }
}

-(void)goHorizontal:(NSInteger *)index
{

        [_horizontalViewController setIndex:index];
        [_viewController pushFrontViewController:_horizontalViewController animated:YES];
        //[self firesComscoreHorizontal:index controller:_horizontalViewController comScoreView:@"" postTitle:@"" isVideo:NO];

}

-(void)goDetail:(NSString *)type array:(NSArray *)array idx:(NSInteger)idx
{
    NSMutableArray *arrayVC = [[NSMutableArray alloc] init];
    NSMutableArray *arrayNames = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < array.count; i++) {
        NSArray *temp = [[NSArray alloc]initWithArray:array];
        DetailController *detailController = [[DetailController alloc] initWithType:type array:temp idx:i];
        
        [arrayVC addObject:detailController];
        [arrayNames addObject:[_arrayNames objectAtIndex:_horizontalViewController.getIndex]];
    }
    
    _detailHorizontalViewController = [[ContainerViewController alloc] initWithArray:arrayVC nameArray:(NSArray *)arrayNames index:idx usetoolbar:NO shareButton:NO isPelicula:NO backButton:YES object:nil preload:NO];
    
    [_viewController pushFrontViewController:_detailHorizontalViewController animated:YES];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end

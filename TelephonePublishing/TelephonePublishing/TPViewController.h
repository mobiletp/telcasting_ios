//
//  TPViewController.h
//  TelephonePublishing
//
//  Created by Tpbeca tec on 17/09/14.
//  Copyright (c) 2014 Hearst Magazine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPToolbarDown.h"
#import "TPToolbarLlamar.h"
#import "TPTabbar.h"
#import "MiPerfil.h"
#import "TPDatePicker.h"
#import "MiPerfilDatosHoroscopo.h"
#import "MisAmigos.h"
#import "addAmigo.h"

@interface TPViewController : UIViewController <TPTabbarDelegate, MiPerfilDelegate, TPDatePickerDelegate, MisAmigosDelegate, addAmigoDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate>{

    TPTabbar *_tabbar;
    MiPerfil *_miPerfil;
    MisAmigos *_misAmigos;
    addAmigo *_addAmigo;
    MiPerfilDatosHoroscopo *_miPerfilDatosHoroscopo;
    TPDatePicker *_picker;
    UIView *dimView;
    
    BOOL perfil;
    
    UIImageView * imageView;
}

@property (strong, nonatomic) TPTabbar* tabbar;
@property (nonatomic, retain) MiPerfil* miPerfil;
@property (nonatomic, retain) MisAmigos* misAmigos;
@property (nonatomic, retain) addAmigo* addAmigo;
@property (nonatomic, retain) MiPerfilDatosHoroscopo* miPerfilDatosHoroscopo;
@property (nonatomic, retain) TPDatePicker *picker;

@property (nonatomic, retain) UIImageView *imageView;

@property (assign) BOOL mas;

-(IBAction) botonFoto:(id)sender;
-(IBAction) botonFotoPerfil:(id)sender;

@end

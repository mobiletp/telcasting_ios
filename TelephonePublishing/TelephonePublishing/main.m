//
//  main.m
//  TelephonePublishing
//
//  Created by Tpbeca tec on 16/09/14.
//  Copyright (c) 2014 Hearst Magazine. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TPAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TPAppDelegate class]));
    }
}
